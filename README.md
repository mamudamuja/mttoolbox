# MTToolBox

[![CI Status](http://img.shields.io/travis/Milutin Tomic/MTToolBox.svg?style=flat)](https://travis-ci.org/Milutin Tomic/MTToolBox)
[![Version](https://img.shields.io/cocoapods/v/MTToolBox.svg?style=flat)](http://cocoapods.org/pods/MTToolBox)
[![License](https://img.shields.io/cocoapods/l/MTToolBox.svg?style=flat)](http://cocoapods.org/pods/MTToolBox)
[![Platform](https://img.shields.io/cocoapods/p/MTToolBox.svg?style=flat)](http://cocoapods.org/pods/MTToolBox)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MTToolBox is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MTToolBox"
```

## Author

Milutin Tomic, milutin@appbuilder.at

## License

MTToolBox is available under the MIT license. See the LICENSE file for more info.
