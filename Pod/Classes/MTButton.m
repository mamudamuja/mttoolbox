//
//  MTButton.m
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import "MTButton.h"
#import "UIView+MTToolBox.h"

@interface MTButton()

@property (nonatomic, strong) UIImageView *internalImageView;
@property (nonatomic, strong) UILabel *internalTitleLabel;

@property (nonatomic, copy, nullable) void (^actionBlock)();

@end


@implementation MTButton

#pragma mark - API

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}

- (void)addAction:(void (^)())actionBlock forControlEvents:(UIControlEvents)controlEvents {
    self.actionBlock = actionBlock;
    [self addTarget:self action:@selector(_handleControlEvent:) forControlEvents:controlEvents];
}

#pragma mark - Private

- (void)_handleControlEvent:(UIButton *)sender {
    if (self.actionBlock) self.actionBlock();
}

#pragma mark - Subviews

- (void)layoutSubviews{
    // take the image size as icon view size
    [_internalImageView sizeToFit];
    
    // clamp size to button bounds
    _internalImageView.width = MIN(_internalImageView.width, self.width);
    _internalImageView.height = MIN(_internalImageView.height, self.height);
    
    // overwrite size with user defined size properties, if provided
    if (_iconSize.width > 0) {
        _internalImageView.width = _iconSize.width;
    }
    
    if (_iconSize.height > 0) {
        _internalImageView.height = _iconSize.height;
    }

    // center icon view
    [_internalImageView centerInSuperView];
    
    // layout title label
    [_internalTitleLabel fillSuperView];
    
    // apply user defined offsets
    _internalImageView.center = CGPointMake(_internalImageView.center.x + _iconOffset.x,
                                            _internalImageView.center.y + _iconOffset.y);
    
    _internalTitleLabel.center = CGPointMake(_internalTitleLabel.center.x + _titleOffset.x,
                                             _internalTitleLabel.center.y + _titleOffset.y);
}


/**
 *  Initializes controls subviews
 */
- (void)initializeSubviews{
    if (_internalImageView) {
        return;
    }
    
    self.clipsToBounds = YES;
    
    _internalImageView = [[UIImageView alloc]init];
    _internalImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_internalImageView];
    
    _internalTitleLabel = [[UILabel alloc]init];
    _internalTitleLabel.backgroundColor = [UIColor clearColor];
    _internalTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_internalTitleLabel];
}

- (void)updateSubviews{
    // use buttons font properties
    _internalTitleLabel.textColor = self.titleLabel.textColor;
    _internalTitleLabel.font = self.titleLabel.font;
    
    // use buttons tint color as icon view tint color
    _internalImageView.tintColor = self.tintColor;
    
    // invalidate button title
    self.titleLabel.text = @"";
    
    // invalidate button image view
    self.imageView.image = nil;
}

#pragma mark - Setters

- (void)setButtonTitle:(NSString *)buttonTitle{
    _buttonTitle = buttonTitle;
    _internalTitleLabel.text = buttonTitle;
    
    [self updateSubviews];
}


- (void)setButtonIcon:(UIImage *)buttonIcon{
    _buttonIcon = buttonIcon;
    _internalImageView.image = _renderIconAsTemplate ? [buttonIcon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] : buttonIcon;
    
    [self updateSubviews];
    [self layoutSubviews];
}


- (void)setIconOffset:(CGPoint)iconOffset{
    _iconOffset = iconOffset;
    
    [self layoutSubviews];
}


- (void)setTitleOffset:(CGPoint)titleOffset{
    _titleOffset = titleOffset;
    
    [self layoutSubviews];
}


- (void)setIconSize:(CGSize)iconSize{
    _iconSize = iconSize;
    
    [self layoutSubviews];
}


- (void)setRenderIconAsTemplate:(BOOL)renderIconAsTemplate{
    _renderIconAsTemplate = renderIconAsTemplate;
    
    self.buttonIcon = _buttonIcon;
}

@end
