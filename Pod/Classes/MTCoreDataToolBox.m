//
//  MTCoreDataToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 22/12/15.
//
//

#import "MTCoreDataToolBox.h"

@interface MTCoreDataToolBox ()

@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSManagedObjectModel *objectModel;

@property (nonatomic, strong) NSString *storeName;
@property (nonatomic, strong) NSString *objectModelName;

@end


@implementation MTCoreDataToolBox


+ (instancetype)sharedInstance {
    static id sharedInstance;
    @synchronized(self) {
        if (!sharedInstance) {
            sharedInstance = [[self alloc] init];
        }
        return sharedInstance;
    }
}

#pragma mark - CA

- (NSUndoManager *)undoManager {
    if (!_undoManager) {
        _undoManager = [NSUndoManager new];
        self.managedContext.undoManager = _undoManager;
    }
    
    return _undoManager;
}

#pragma mark - Creating Stack

/**
 *  Creates core data stack
 *
 *  @param storeName       - name of store file
 *  @param objectModelName - name of the core data model file
 */
+ (void)createCoreDataStackWithStoreName:(NSString*)storeName andObjectModelName:(NSString*)objectModelName{
    [[self sharedInstance] setObjectModelName:objectModelName];
    [[self sharedInstance] setStoreName:storeName];
    
    [[self sharedInstance] managedObjectContext];
}

#pragma mark - Saving

/**
 *  Saves the changes to the managedObjectContext
 */
+ (void)saveContext{
    NSManagedObjectContext *context = [[self sharedInstance] managedContext];
    
    NSError *error;
    if (context != nil) {
        if ([context hasChanges] && ![context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

#pragma mark - Core Data stack

/**
 *  Returns the URL to the application's Documents directory.
 *
 *  @return
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


/**
 *  Returns the persistent store coordinator for the application.
 *  If the coordinator doesn't already exist, it is created and the application's store added to it.
 *
 *  @return
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.CDBStore", _storeName]];
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // If the expected store doesn't exist, copy the default store.
    if (![fileManager fileExistsAtPath:[storeURL path]]) {
        NSURL *defaultStoreURL = [[NSBundle mainBundle] URLForResource:_storeName withExtension:@"CDBStore"];
        if (defaultStoreURL) {
            [fileManager copyItemAtURL:defaultStoreURL toURL:storeURL error:NULL];
        }
    }
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @YES, NSInferMappingModelAutomaticallyOption: @YES};
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    
    NSError *error;
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        
    }
    
    return _persistentStoreCoordinator;
}


/**
 *  Returns the managed object context for the application.
 *  If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 *
 *  @return
 */
- (NSManagedObjectContext *) managedObjectContext
{
    if (_managedContext != nil) {
        return _managedContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedContext setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedContext;
}


/**
 *  Returns the managed object model for the application.
 *  If the model doesn't already exist, it is created from the application's model.
 *
 *  @return
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (_objectModel != nil) {
        return _objectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:_objectModelName withExtension:@"momd"];
    _objectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _objectModel;
}


@end
