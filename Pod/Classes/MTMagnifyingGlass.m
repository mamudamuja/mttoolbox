//
//  MTMagnifyingGlass.m
//  MTToolBox
//
//  Created by Milutin Tomic on 26/08/16.
//
//

#import "MTMagnifyingGlass.h"

static NSTimeInterval const kFadeAnimationSpeed = 0.2;
static CGFloat const kDefaultMagnifierFactor = 1.5;

@interface MTMagnifyingGlass ()

@property (nonatomic, strong) UIImageView *magnifyingImageView;

@end


@implementation MTMagnifyingGlass

#pragma mark - CA

- (void)setMagnifyingFactor:(CGFloat)magnifyingFactor {
    _magnifyingFactor = magnifyingFactor;
    
    [self _setupMagnifyingImageView];
}

- (void)setTargetView:(UIView *)targetView {
    _targetView = targetView;
    
    // set up magnifying image view    
    [self refreshInput];
}

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (self) {
        [self _setupMagnifyingView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _setupMagnifyingView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self _setupMagnifyingView];
    }
    return self;
}

#pragma mark - API

- (void)showAnimated:(BOOL)animated {
    [UIView animateWithDuration:(animated ? kFadeAnimationSpeed : 0) delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.alpha = 1.0;
        self.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:nil];
}

- (void)dismissAnimated:(BOOL)animated {
    [UIView animateWithDuration:(animated ? kFadeAnimationSpeed : 0) delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.alpha = 0.0;
        self.transform = CGAffineTransformMakeScale(.5, .5);
    } completion:nil];
}

- (void)magnify:(CGPoint)focusPoint {
    self.magnifyingImageView.center = CGPointMake((- focusPoint.x * _magnifyingFactor) + self.magnifyingImageView.frame.size.width/2.0 + self.frame.size.width/2.0,
                                                  (- focusPoint.y * _magnifyingFactor) + self.magnifyingImageView.frame.size.height/2.0 + self.frame.size.height/2.0);
}

- (void)refreshInput {
    self.magnifyingImageView.image = [self _targetViewSnapshot];
    [self _setupMagnifyingImageView];
}

#pragma mark - Private

/**
 *  Sets up the initial state of the magnifying view
 */
- (void)_setupMagnifyingView {
    _magnifyingFactor = kDefaultMagnifierFactor;
    
    // setup image view
    self.magnifyingImageView = [UIImageView new];
    [self addSubview:self.magnifyingImageView];
    
    // setup glass view
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor darkGrayColor];
}

/**
 *  Sets frame and center for the magnifying image view
 */
- (void)_setupMagnifyingImageView {
    self.magnifyingImageView.frame = CGRectMake(0.0, 0.0, self.targetView.frame.size.width * _magnifyingFactor,
                                                self.targetView.frame.size.height * _magnifyingFactor);
    self.magnifyingImageView.center = CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0);
}

/**
 *  Renders a snaphot of the target view
 */
- (UIImage *)_targetViewSnapshot {
    UIGraphicsBeginImageContextWithOptions(self.targetView.bounds.size, self.targetView.opaque, 0.0);
    [self.targetView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return snapshot;
}

@end
