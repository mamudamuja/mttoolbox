//
//  MTImageViewController.h
//  Pods
//
//  Created by Milutin Tomic on 26/02/2017.
//
//

#import <UIKit/UIKit.h>

/**
 *  In case your application is restricted to one or more device orientations
 *  but you still want the MTImageViewController to support other orientations you can
 *  place this macro in you Application Delegate class implementation scope to do the setup
 *
 *  @param OrientationMaskForImageVC  - The orientation mask for MTImageViewController instances
 *  @param OrientationMaskForOtherVCs - The orientation mask for all other view controllers
 */
#define MTImageViewControllerOrientationSetup(OrientationMaskForImageVC, OrientationMaskForOtherVCs) - (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {if ([self.window.rootViewController.presentedViewController isKindOfClass:[MTImageViewController class]]) {return (OrientationMaskForImageVC);}else{return (OrientationMaskForOtherVCs);}}


@class MTImageViewController;
@protocol MTImageViewControllerDelegate <NSObject>

@required

/**
 *  Called by the vc when the image needs to be displayed
 *
 *  @param imageVC - image view controller instance
 *  @param pageIndex - page with the index that will display the image
 *
 *  @return - image to be displayed at the passed index
 */
- (nonnull UIImage *)mtImageViewController:(nonnull MTImageViewController *)imageVC imageForPageAtIndex:(NSInteger)pageIndex;

/**
 *  Use this method to tell the image view controller how many images/pages will be displayed
 *
 *  @param imageVC - image view controller instance
 *
 *  @return - number of pages/images
 */
- (NSInteger)numberOfPagesForMTImageViewController:(nonnull MTImageViewController *)imageVC;

@end


@interface MTImageViewController : UIViewController

/**
 *  Specifies if the view controller should hide the status bar when presented
 *  Defaults to 'YES'
 */
@property (nonatomic) BOOL                                                    hidesStatusBar;

/**
 *  Specifies if the view controller should display a page indicator label
 *  Defaults to 'YES'
 */
@property (nonatomic) BOOL                                                    showPageIndicator;

/**
 *  For this property to work the 'UIViewControllerBasedStatusBarAppearance' key has to be set to 'YES'
 *  Defaults to 'UIStatusBarStyleDefault'
 */
@property (nonatomic) UIStatusBarStyle                                        statusBarStyle;

/**
 *  Specifies the space between images
 *  Defaults to '10'
 */
@property (nonatomic) NSInteger                                               spaceBetweenPages;

/**
 *  Use this property to override the default close button behaviour which just dismisses the view controller
 */
@property (nonatomic, copy, nullable) void                                    (^closeButtonAction)();

/**
 *  Set the delegate and implement the protocol if you want more control over the data source.
 *  For example if you do not want to pass all images in an array due to memory concerns you 
 *  can provide the data source via the delegate methods.
 */
@property (nonatomic, assign, nullable) id <MTImageViewControllerDelegate>    delegate;


/**
 *  Initializes image view controller with images that need to be displayed.
 *  If you set the delegate the images will be provided via delegate methods and the images array will be ignored.
 *
 *  @param images - images to display
 */
- (nonnull instancetype)initWithImages:(nullable NSArray<UIImage *> *)images;

/**
 *  Scrolls to the page with the passed index
 *
 *  @param pageIndex - page index to scroll to
 */
- (void)showPage:(NSInteger)pageIndex;

@end


#pragma mark - MTImageScrollView

@interface MTImageScrollView : UIScrollView

@property (nonatomic, strong, nullable) UIImage                               *image;
@property (nonatomic) NSInteger                                               pageIndex;

@end
