//
//  UIScrollView+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 19/02/16.
//
//

#import "UIScrollView+MTToolBox.h"

@implementation UIScrollView (MTToolBox)

#pragma mark - Vertical Scrolling

/**
 *  Scrolls the scroll view in the vertical direction to put the
 *  passed subview in the currently visible scroll view area.
 *
 *  @param subview  - subview to scroll to visible area
 *  @param position - specifies where to scroll to exactly
 *  @param offset   - applied to content offset
 *  @param animated - specifies wether to scroll animated or just jump to the position
 */
- (void)scrollSubview:(UIView*)subview toPosition:(MTScrollPosition)position offset:(CGPoint)offset animated:(BOOL)animated{
    // convert subview's frame to the scroll view
    CGRect subviewFrame = [self _frameOfSubview:subview];
    
    switch (position) {
        case MTScrollPositionTopEdge:
            [self setContentOffset:CGPointMake(offset.x, offset.y + subviewFrame.origin.y) animated:animated];
            break;
            
        case MTScrollPositionBottomEdge:
            [self setContentOffset:CGPointMake(offset.x, offset.y + subviewFrame.origin.y - self.frame.size.height + subview.frame.size.height + 5) animated:animated];
            break;

        case MTScrollPositionCenter:
            [self setContentOffset:CGPointMake(offset.x, offset.y + subviewFrame.origin.y - self.frame.size.height/2 + subview.frame.size.height/2) animated:animated];
            break;
            
        default:
            break;
    }
}

/**
 *  Scrolls the scroll view in the vertical direction to put the
 *  passed subview in the currently visible scroll view area.
 *
 *  @param subview  - subview to scroll to visible area
 *  @param position - specifies where to scroll to exactly
 *  @param animated - specifies wether to scroll animated or just jump to the position
 */
- (void)scrollSubview:(UIView *)subview toPosition:(MTScrollPosition)position animated:(BOOL)animated{
    [self scrollSubview:subview toPosition:position offset:CGPointZero animated:animated];
}

/**
 *  Scrolls the scroll view to its bottom.
 *  If the content size height is smaller than the scrollview height the call is ignored.
 *
 *  @param animated - scroll animated
 */
- (void)scrollToBottomAnimated:(BOOL)animated {
    if (self.contentSize.height > self.frame.size.height) {
        [self setContentOffset:CGPointMake(self.contentOffset.x, self.contentSize.height - self.frame.size.height) animated:animated];
    }
}

/**
 *  Scrolls the scroll view so that the content offset does not exceed the content size bounds
 *
 *  @param animated - scroll animated
 */
- (void)scrollToContentSizeAnimated:(BOOL)animated {
    if (self.contentOffset.y < 0) {// scroll view scrolled up too far -> scroll back to origin
        [self setContentOffset:CGPointZero animated:animated];
    }else if (self.frame.size.height > self.contentSize.height) {// frame bigger than content size -> scroll back to origin
        [self setContentOffset:CGPointZero animated:animated];
    }else if (self.contentOffset.y + self.frame.size.height > self.contentSize.height) {// scrolled down too far -> scroll to bottom
        [self scrollToBottomAnimated:animated];
    }
}

#pragma mark - Private

/**
 *  Maps the frame of any subview of the scrollView to its content size
 *
 *  @param subview - subview to convert the frame from
 *
 *  @return
 */
- (CGRect)_frameOfSubview:(UIView*)subview{
    if (subview.superview == self) {
        return subview.frame;
    }else{
        CGRect frameInSuperView = [self _frameOfSubview:subview.superview];
        return CGRectMake(subview.frame.origin.x + frameInSuperView.origin.x,
                          subview.frame.origin.y + frameInSuperView.origin.y,
                          subview.frame.size.width,
                          subview.frame.size.height);
    }
}

@end
