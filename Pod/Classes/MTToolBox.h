//
//  MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 26/09/15.
//
//


// CORE DATA
#import "MTCoreDataToolBox.h"

// UI
#import "MTUIToolBox.h"
#import "MTToggleButton.h"
#import "MTButton.h"
#import "MTImageView.h"
#import "MTTableViewCell.h"
#import "MTInteractiveTextView.h"
#import "MTQuickLookViewController.h"
#import "MTHUDView.h"
#import "MTMagnifyingGlass.h"
#import "MTTransitionAnimator.h"
#import "MTDropDownMenu.h"
#import "MTPieChartView.h"
#import "MTChartLegendView.h"
#import "MTImageViewController.h"
#import "MTLayoutHelperView.h"

// Foundation
#import "NSArray+MTToolBox.h"
#import "NSDictionary+MTToolBox.h"
#import "NSString+MTToolBox.h"
#import "NSDate+MTToolBox.h"
#import "NSData+MTToolBox.h"
#import "NSURL+MTToolBox.h"

// OTHER
#import "MTPasswordStrengthChecker.h"
#import "MTCache.h"
#import "MTDebugKit.h"


#pragma mark - LOG

#if DEBUG
#define MTLog(fmt, ...) NSLog(fmt, ##__VA_ARGS__);
#else
#define MTLog(fmt, ...)
#endif


#pragma mark - COLORS

/**
 *  Returns color with specified params
 *
 *  @param r - red
 *  @param g - green
 *  @param b - blue
 *  @param a - alpha
 */
#define COLOR(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

/**
 *  Returns color from the passed HEX string
 *
 *  @param RRGGBB - hex string
 */
#define COLOR_HEX(OxRRGGBB) [UIColor colorWithRed:((OxRRGGBB & 0xFF0000) >> 16) / 255.0f green:((OxRRGGBB & 0x00FF00) >>  8) / 255.0f blue:(OxRRGGBB & 0x0000FF) / 255.0f alpha:1]

/**
 *  Returns a random color
 */
#define COLOR_RANDOM [UIColor colorWithRed:RANDOM_NUMBER(0, 255)/255.0 green:RANDOM_NUMBER(0, 255)/255.0 blue:RANDOM_NUMBER(0, 255)/255.0 alpha:1]

/**
 *  Returns a random dark color
 */
#define COLOR_RANDOM_DARK [UIColor colorWithRed:RANDOM_NUMBER(0, 128)/255.0 green:RANDOM_NUMBER(0, 128)/255.0 blue:RANDOM_NUMBER(0, 128)/255.0 alpha:1]

/**
 *  Returns a random bright color
 */
#define COLOR_RANDOM_LIGHT [UIColor colorWithRed:RANDOM_NUMBER(128, 255)/255.0 green:RANDOM_NUMBER(128, 255)/255.0 blue:RANDOM_NUMBER(128, 255)/255.0 alpha:1]

/**
 *  Returns a random color with values within the passed range
 *
 *  @param low  - lower brightness bound
 *  @param high - upper brightness bound
 */
#define COLOR_RANDOM_IN_RANGE(low, high) [UIColor colorWithRed:RANDOM_NUMBER(low, high)/255.0 green:RANDOM_NUMBER(low, high)/255.0 blue:RANDOM_NUMBER(low, high)/255.0 alpha:1]


#pragma mark - UTILITIES

/**
 *  Returns random number within bounds (including the lower and upper bounds)
 */
#define RANDOM_NUMBER(MIN_BOUND, MAX_BOUND) (MIN_BOUND + arc4random_uniform(MAX_BOUND - MIN_BOUND + 1))

/**
 *  Creates a singleton accessor and initializer
 *
 *  @param accessor - name of the accessor
 */
#define SINGLETON(accessor) +(instancetype)accessor {static id accessor;@synchronized(self) {if (!accessor) {accessor = [[self alloc] init];}return accessor;}}

/**
 *  Converts radians to degrees
 */
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

/**
 *  Converts degrees to radians
 */
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

/**
 *  Opens passed url
 */
#define OPEN_URL(url) [[UIApplication sharedApplication] openURL:url];

/**
 *  Opens passed url string
 */
#define OPEN_URL_STRING(urlString) [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]]

/**
 *  Calls the passed phone number is the device supports calls
 */
#define CALL_NUMBER(string) NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", string]];if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {[[UIApplication sharedApplication] openURL:phoneURL];}

/**
 *  Sets the devices current orientation
 */
#define SET_DEVICE_ORIENTATION(UIInterfaceOrientation) [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientation] forKey:@"orientation"]

/**
 *  Boolean expression checking if the passed 'number' is within the given range
 */
#define IS_WITHIN_RANGE(number, minimum, maximum) ((number) >= (minimum) && (number) <= (maximum))


#pragma mark - PARAM ASSERTIONS

/**
 *  Asserts that the passed param is not nil. Throws exception if param was nil.
 */
#define MTAssertParam(param) if (!param) @throw [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"Parameter `%@` for method `%s` must not be nil.", [NSString stringWithFormat:@"%s", #param], __PRETTY_FUNCTION__] userInfo:nil];


#pragma mark - DEVICE INFO

#define isIPad UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define isIPhone UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone
#define isTV UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomTV

#define SYSTEM_VERSION_EQUAL_TO(version) ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(version) ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version) ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(version) ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(version) ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] != NSOrderedDescending)



