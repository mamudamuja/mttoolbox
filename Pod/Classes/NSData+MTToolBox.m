//
//  NSData+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 15/03/16.
//
//

#import "NSData+MTToolBox.h"

@implementation NSData (MTToolBox)

#pragma mark - Write to File

/**
 *  Saves data to the passed subpath of the documents directory
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return - YES if save was successfull, otherwise NO
 */
- (BOOL)saveFileInDocumentsDirectoryWithPath:(NSString*)path{
    // put together file path
    NSString *documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *finalPath = [documentsDirectoryPath stringByAppendingPathComponent:path];
    
    // create subdirectories if specified in subPath
    NSArray *subDirectories = [path componentsSeparatedByString:@"/"];
    if (subDirectories.count > 1) {
        
        // create subdirectory path
        NSString *subDirectoryPath = [[subDirectories subarrayWithRange:NSMakeRange(0, subDirectories.count-1)] componentsJoinedByString:@"/"];
        subDirectoryPath = [documentsDirectoryPath stringByAppendingPathComponent:subDirectoryPath];
        
        // create subdirectory if not exists already
        NSError *error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:subDirectoryPath]){
            [[NSFileManager defaultManager] createDirectoryAtPath:subDirectoryPath withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        if (error) {
            NSLog(@"ERROR WHILE CREATING SUBDIRECTORIES: %@", error);
        }
    }
    
    // save to path
    NSError *error;
    BOOL savedSuccessfully = [self writeToFile:finalPath options:NSDataWritingAtomic error:&error];
    
    if (!savedSuccessfully) {
        NSLog(@"ERROR WHILE SAVING FILE: %@", error);
    }
    
    return savedSuccessfully;
}

/**
 *  Saves data to the passed subpath of the Caches directory
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return - YES if save was successfull, otherwise NO
 */
- (BOOL)cacheFileInCachesDirectoryWithPath:(NSString *)path {
    // put together file path
    NSString *cacheDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *finalPath = [cacheDirectoryPath stringByAppendingPathComponent:path];
    
    // create subdirectories if specified in subPath
    NSArray *subDirectories = [path componentsSeparatedByString:@"/"];
    if (subDirectories.count > 1) {
        
        // create subdirectory path
        NSString *subDirectoryPath = [[subDirectories subarrayWithRange:NSMakeRange(0, subDirectories.count-1)] componentsJoinedByString:@"/"];
        subDirectoryPath = [cacheDirectoryPath stringByAppendingPathComponent:subDirectoryPath];
        
        // create subdirectory if not exists already
        NSError *error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:subDirectoryPath]){
            [[NSFileManager defaultManager] createDirectoryAtPath:subDirectoryPath withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        if (error) {
            NSLog(@"ERROR WHILE CREATING SUBDIRECTORIES: %@", error);
        }
    }
    
    // save to path
    NSError *error;
    BOOL savedSuccessfully = [self writeToFile:finalPath options:NSDataWritingAtomic error:&error];
    
    if (!savedSuccessfully) {
        NSLog(@"ERROR WHILE SAVING FILE: %@", error);
    }
    
    return savedSuccessfully;
}

#pragma mark - Read from File

/**
 *  Initializes NSData object with content of the file at passed path in the documents directory
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return - returns NSData object with contents of specified file at path
 */
+ (NSData*)dataFromDocumentsDirectoryWithPath:(NSString*)path{
    // put together file path
    NSString *documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *finalPath = [documentsDirectoryPath stringByAppendingPathComponent:path];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:finalPath]) {
        return [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:finalPath]];
    }else{
        return nil;
    }
}

/**
 *  Initializes NSData object with content of the file at passed path in the Caches directory
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return - returns NSData object with contents of specified file at path
 */
+ (NSData *)dataFromCachesDirectoryWithPath:(NSString *)path {
    // put together file path
    NSString *cacheDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *finalPath = [cacheDirectoryPath stringByAppendingPathComponent:path];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:finalPath]) {
        return [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:finalPath]];
    }else{
        return nil;
    }
}

@end
