//
//  UIView+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 26/09/15.
//
//

#import <UIKit/UIKit.h>

@interface UIView (MTToolBox)

#pragma mark - Popup

/**
 *  Presents the view as a popup in the key window.
 *
 *  @param setUpBlock - once called the view is added to the key window without changing any of it's properties.
 *                      this block is called after the view is added to the window. Use it to display the view 
 *                      in an animated fashion or to setup autolayout constraint.
 */
- (void)popupInWindowWithSetUpBlock:(void (^_Nullable)( UIView * _Nonnull view,  UIWindow * _Nonnull window))setUpBlock;

/**
 *  Removes the view from its superview (if called after popupInWindowWithSetUpBlock: this will be the keyWindow)
 *
 *  @param cleanUpBlock - use this block to remove the view animated or do some clean up.
 *                        NOTE: if a cleanUpBlock is provided it also needs to take care
 *                        of removing the view from it's parent view
 */
- (void)dismissFromPopupWithCleanUpBlock:(void (^_Nullable)(UIView * _Nonnull view, UIWindow * _Nonnull window))cleanUpBlock;

#pragma mark - Utils

/**
 *  Convenience method to load a view form a equally named xib file
 *
 *  @param owner - owner of the view
 */
+ (nonnull instancetype)loadViewFromXibWithOwner:(nonnull id)owner;

/**
 *  Creates a snapshot of the view and returns it as a UIImage instance
 */
- (nonnull UIImage *)snapshot;

/**
 *  Removes all subviews from view
 */
- (void)removeAllSubviews;

#pragma mark - Layout
#pragma mark Setting Center

/**
 *  Centers view horizontally in super view
 */
-(void)centerHorizontallyInSuperView;


/**
 *  Centers view vertically in super view
 */
-(void)centerVerticallyInSuperView;


/**
 *  Centers view horizontally in passed view
 *
 *  @param view
 */
-(void)centerHorizontallyInView:(nullable UIView*)view;


/**
 *  Centers view vertically in passed view
 *
 *  @param view
 */
-(void)centerVerticallyInView:(nullable UIView*)view;


/**
 *  Centers view horizontally and vertically in super view
 */
-(void)centerInSuperView;


/**
 *  Centers view horizontally in superview and vertically under passed view (with padding)
 *
 *  @param view
 *  @param padding
 */
-(void)centerUnderView:(nullable UIView*)view withPadding:(float)padding;


/**
 *  Centers view horizontally in super view and vertically at bottom of super view
 */
-(void)centerAtBottomOfSuperView;

#pragma mark Setting Frame

/**
 *  Sets views frame width
 *
 *  @param width
 */
-(void)setWidth:(float)width;


/**
 *  Sets views frame height
 *
 *  @param height
 */
-(void)setHeight:(float)height;


/**
 *  Sets views frame to fill the super view
 */
-(void)fillSuperView;

#pragma mark Calculating Frame

/**
 *  Returns views frame width
 *
 *  @return
 */
-(float)width;


/**
 *  Returns views frame height
 *
 *  @return
 */
-(float)height;

#pragma mark Calculating Edges

/**
 *  Returns the x value of the right frame edge
 *
 *  @return
 */
-(float)rightEdgeValue;


/**
 *  Returns the x value of the left frame edge
 *
 *  @return
 */
-(float)leftEdgeValue;


/**
 *  Returns the y value of the top frame edge
 *
 *  @return
 */
-(float)topEdgeValue;


/**
 *  Returns the y value of the bottom frame edge
 *
 *  @return
 */
-(float)bottomEdgeValue;

@end
