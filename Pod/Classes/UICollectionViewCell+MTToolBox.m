//
//  UICollectionViewCell+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 02/09/16.
//
//

#import "UICollectionViewCell+MTToolBox.h"

@implementation UICollectionViewCell (MTToolBox)

/**
 *  Returns a cell reuse identifier for the class
 *
 *  @return
 */
+ (NSString*)reuseIdentifier{
    return [NSString stringWithFormat:@"%@_ID", NSStringFromClass([self class])];
}

@end
