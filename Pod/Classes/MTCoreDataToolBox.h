//
//  MTCoreDataToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 22/12/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+MTCoreDataToolBox.h"

@interface MTCoreDataToolBox : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedContext;
@property (nonatomic, strong) NSUndoManager *undoManager;

+ (instancetype)sharedInstance;

#pragma mark - Creating Stack

/**
 *  Creates core data stack
 *
 *  @param storeName       - name of store file
 *  @param objectModelName - name of the core data model file
 */
+ (void)createCoreDataStackWithStoreName:(NSString*)storeName andObjectModelName:(NSString*)objectModelName;

#pragma mark - Saving

/**
 *  Saves the changes to the managedObjectContext
 */
+ (void)saveContext;

@end
