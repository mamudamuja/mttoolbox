//
//  MTTableViewCell.m
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import "MTTableViewCell.h"

@implementation MTTableViewCell

/**
 *  Sets the separator line to go egde to edge
 *
 *  @return
 */
- (UIEdgeInsets)layoutMargins{
    return UIEdgeInsetsZero;
}

@end
