//
//  MTCache.m
//  Pods
//
//  Created by Milutin Tomic on 19/02/2017.
//
//

#import "MTCache.h"
#import "MTToolBox.h"

#define kMTCacheNameSpaceCachePath @"MTCache/%@/%@"
#define kMTCacheDefaultCachePath @"MTCache/%@"


@interface MTCache () <NSCacheDelegate>

@property (nonatomic, strong) NSMutableArray *caches;
@property (nonatomic, strong) NSCache *defaultCache;

@end


@implementation MTCache

#pragma mark - Lifecycle

SINGLETON(sharedCache)

- (instancetype)init {
    self = [super init];
    if (self) {
        self.caches = [NSMutableArray array];
        self.defaultCache = [NSCache new];
        self.defaultCache.delegate = self;
    }
    return self;
}

#pragma mark - API

+ (NSCache *)cacheWithNameSpace:(NSString *)nameSpace {
    if (nameSpace) {
        NSCache *cache = [self cacheForNameSpace:nameSpace];
        return cache ?: [self createCacheWithName:nameSpace];
    }else{
        return [[self sharedCache] defaultCache];
    }
}

#pragma mark retrieve

+ (id)objectForKey:(id)key {
    MTAssertParam(key);
    return [self objectForKey:key nameSpace:nil];
}

+ (id)objectForKey:(id)key nameSpace:(NSString *)nameSpace {
    MTAssertParam(key);
    return [self objectForKey:key nameSpace:nameSpace ignoreDisk:NO];
}

+ (id)objectForKey:(id)key nameSpace:(NSString *)nameSpace ignoreDisk:(BOOL)ignoreDisk {
    MTAssertParam(key);
    id result;
    
    if (!nameSpace) {// search in default cache if 'nameSpace' is nil
        result = [[[self sharedCache] defaultCache] objectForKey:key];
    }else{// find cache for passed nameSpace and search that one
        NSCache *cache = [self cacheForNameSpace:nameSpace];
        result = [cache objectForKey:key];
    }
    
    if (ignoreDisk) {// ignore disk -> just return what is in memory, even if there was no result
        return result;
    }else{// check disk additionally
        if (!result) {// if nothing found in memory
            // retreive data from disk
            NSString *cachePath = nameSpace ? [NSString stringWithFormat:kMTCacheNameSpaceCachePath, nameSpace, key] : [NSString stringWithFormat:kMTCacheDefaultCachePath, key];
            NSData *data = [NSData dataFromCachesDirectoryWithPath:cachePath];
            result = data == nil ? nil : [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            if (result) {// store object in cache
                [self setObject:result forKey:key nameSpace:nameSpace persist:NO];
            }
        }
    }
    
    return result;
}

#pragma mark store

+ (void)setObject:(id)object forKey:(id)key {
    [self setObject:object forKey:key nameSpace:nil];
}

+ (void)setObject:(id)object forKey:(id)key nameSpace:(NSString *)nameSpace {
    [self setObject:object forKey:key nameSpace:nameSpace persist:NO];
}

+ (BOOL)setObject:(id)object forKey:(id)key nameSpace:(NSString *)nameSpace persist:(BOOL)persist {
    MTAssertParam(key);
    MTAssertParam(object);
    
    NSCache *cache;
    if (!nameSpace) {// use default cache is no namespace specified
        cache = [[self sharedCache] defaultCache];
    }else{// serach for cache with specified namespace
        cache = [self cacheForNameSpace:nameSpace];
    }
    
    // create cache for namespace if needed
    if (!cache) {
        cache = [self createCacheWithName:nameSpace];
    }
    
    // store object in cache
    [cache setObject:object forKey:key];
    
    // persist object
    if (persist) {
        NSString *cachePath = nameSpace ? [NSString stringWithFormat:kMTCacheNameSpaceCachePath, nameSpace, key] : [NSString stringWithFormat:kMTCacheDefaultCachePath, key];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:object];
        return [data cacheFileInCachesDirectoryWithPath:cachePath];
    }
    
    return NO;
}

#pragma mark remove

+ (void)removeObjectForKey:(id)key {
    [self removeObjectForKey:key nameSpace:nil];
}

+ (void)removeObjectForKey:(id)key nameSpace:(NSString *)nameSpace {
    [self removeObjectForKey:key nameSpace:nameSpace ignoreDisk:NO];
}

+ (void)removeObjectForKey:(id)key nameSpace:(NSString *)nameSpace ignoreDisk:(BOOL)ignoreDisk {
    MTAssertParam(key);
    
    // find the right cache
    NSCache *cache;
    if (nameSpace) {
        cache = [self cacheForNameSpace:nameSpace];
    }else{
        cache = [[self sharedCache] defaultCache];
    }
    
    // remove from cache
    if (cache) {
        [cache removeObjectForKey:key];
    }
    
    // remove from disk
    if (!ignoreDisk) {
        // construct item path
        NSString *cachePath = nameSpace ? [NSString stringWithFormat:kMTCacheNameSpaceCachePath, nameSpace, key] : [NSString stringWithFormat:kMTCacheDefaultCachePath, key];
        NSString *cacheDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *finalPath = [cacheDirectoryPath stringByAppendingPathComponent:cachePath];
        
        // remove object from disk if it exists
        if ([[NSFileManager defaultManager] fileExistsAtPath:finalPath]){
            [[NSFileManager defaultManager] removeItemAtPath:finalPath error:nil];
        }
    }
}

+ (void)removeAllObjects {
    [self removeAllObjectsInNameSpace:nil];
}

+ (void)removeAllObjectsInNameSpace:(NSString *)nameSpace {
    [self removeAllObjectsInNameSpace:nameSpace ignoreDisk:NO];
}

+ (void)removeAllObjectsInNameSpace:(NSString *)nameSpace ignoreDisk:(BOOL)ignoreDisk {
    // find the right cache
    NSCache *cache;
    if (nameSpace) {
        cache = [self cacheForNameSpace:nameSpace];
    }else{
        cache = [[self sharedCache] defaultCache];
    }
    
    // empty cache
    if (cache) {
        [cache removeAllObjects];
    }
    
    // empty disk cache
    if (!ignoreDisk) {
        // construct item path
        NSString *cachePath = nameSpace ? [NSString stringWithFormat:@"MTCache/%@", nameSpace] : @"MTCache";
        NSString *cacheDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *finalPath = [cacheDirectoryPath stringByAppendingPathComponent:cachePath];
        
        // remove object from disk if it exists
        if ([[NSFileManager defaultManager] fileExistsAtPath:finalPath]){
            [[NSFileManager defaultManager] removeItemAtPath:finalPath error:nil];
        }
    }
}

#pragma mark - Private

/**
 *  Creates a new cache instance and sets the shared instance as the delegate
 *
 *  @param name - name of the cache
 *
 *  @return - new NSCache instance
 */
+ (NSCache *)createCacheWithName:(NSString *)name {
    NSCache *cache = [NSCache new];
    cache.delegate = [self sharedCache];
    cache.name = name;
    [[[self sharedCache] caches] addObject:cache];
    
    return cache;
}

/**
 *  Returns the cache with the name equal to the passed nameSpace
 *
 *  @param nameSpace - name of the cache
 *
 *  @return - cache with the passed name or nil
 */
+ (NSCache *)cacheForNameSpace:(NSString *)nameSpace {
    for (NSCache *cache in [[self sharedCache] caches]) {
        if ([cache.name isEqualToString:nameSpace]) {
            return cache;
        }
    }
    
    return nil;
}

#pragma mark - NSCacheDelegate

- (void)cache:(NSCache *)cache willEvictObject:(id)obj {
    MTLog(@"evicting object %@", obj);
}

@end
