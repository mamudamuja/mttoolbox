//
//  UIScrollView+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 19/02/16.
//
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    MTScrollPositionTopEdge,
    MTScrollPositionBottomEdge,
    MTScrollPositionCenter
} MTScrollPosition;


@interface UIScrollView (MTToolBox)

#pragma mark - Vertical Scrolling

/**
 *  Scrolls the scroll view in the vertical direction to put the
 *  passed subview in the currently visible scroll view area.
 *
 *  @param subview  - subview to scroll to visible area
 *  @param position - specifies where to scroll to exactly
 *  @param offset   - applied to content offset
 *  @param animated - specifies wether to scroll animated or just jump to the position
 */
- (void)scrollSubview:(UIView*)subview toPosition:(MTScrollPosition)position offset:(CGPoint)offset animated:(BOOL)animated;

/**
 *  Scrolls the scroll view in the vertical direction to put the
 *  passed subview in the currently visible scroll view area.
 *
 *  @param subview  - subview to scroll to visible area
 *  @param position - specifies where to scroll to exactly
 *  @param animated - specifies wether to scroll animated or just jump to the position
 */
- (void)scrollSubview:(UIView*)subview toPosition:(MTScrollPosition)position animated:(BOOL)animated;

/**
 *  Scrolls the scroll view to its bottom.
 *  If the content size height is smaller than the scrollview height the call is ignored.
 *
 *  @param animated - scroll animated
 */
- (void)scrollToBottomAnimated:(BOOL)animated;

/**
 *  Scrolls the scroll view so that the content offset does not exceed the content size bounds
 *
 *  @param animated - scroll animated
 */
- (void)scrollToContentSizeAnimated:(BOOL)animated;

@end
