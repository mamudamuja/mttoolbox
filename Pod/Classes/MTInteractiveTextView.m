//
//  MTInteractiveTextView.m
//  Pods
//
//  Created by Milutin Tomic on 25/01/16.
//
//

#import "MTInteractiveTextView.h"



#pragma mark - ACTION CLASS

@interface MTInteractiveTextViewAction : NSObject

@property (nonatomic, copy) void (^action)(void);
@property (nonatomic) NSRange actionRange;

@end


@implementation MTInteractiveTextViewAction

@end



#pragma mark - TEXT VIEW

@interface MTInteractiveTextView()

@property (nonatomic, strong) NSMutableArray *actions;
@property (nonatomic, strong) NSAttributedString *originalAttributedText;

@end


@implementation MTInteractiveTextView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupTextView];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupTextView];
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setupTextView];
    }
    return self;
}


/**
 *  Setup the text view
 */
- (void)setupTextView{
    self.editable = NO;
    self.selectable = NO;
}

#pragma mark - Handling Touches

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    
    CGPoint touchPoint = [touch locationInView:self];
    
    // iterate backwards through actions
    for (int index = (int)self.actions.count - 1; index >= 0; index --) {
        MTInteractiveTextViewAction *action = self.actions[index];
        
        // if touch is within action frame highlight action and return
        if (CGRectContainsPoint([self boundingRectForCharacterRange:action.actionRange], touchPoint)) {

            if (_highlightedColor) {
                _originalAttributedText = self.attributedText;
                self.attributedText = [self highlightedStringWithRange:action.actionRange];
            }

            return;
        }
    }
}


- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    
    CGPoint touchPoint = [touch locationInView:self];
    
    // iterate backwards through actions
    for (int index = (int)self.actions.count - 1; index >= 0; index --) {
        MTInteractiveTextViewAction *action = self.actions[index];
        
        // if touch is within action frame execute action and return
        if (CGRectContainsPoint([self boundingRectForCharacterRange:action.actionRange], touchPoint)) {
            action.action();
            
            // reset highlight but after the action has been executed
            // if color is reset before the view does not get a layout
            // update -> the bounding box is wrong and the action won't
            // be executed
            if (_originalAttributedText) {
                self.attributedText = _originalAttributedText;
            }
            
            return;
        }
    }
    
    // if no action executed reset the color
    if (_originalAttributedText) {
        self.attributedText = _originalAttributedText;
    }
}

#pragma mark - Adding Actions

/**
 *  Lazy instantiation for actions array
 *
 *  @return
 */
- (NSMutableArray*)actions{
    if (!_actions) {
        _actions = [NSMutableArray array];
    }
    
    return _actions;
}


/**
 *  Adds the passed action for the first occurrence of the passed substring
 *
 *  @param action    - action to be executed when the substring is tapped
 *  @param substring - substring to attach the action to
 */
- (void)addAction:(void (^)(void))action forSubstring:(NSString*)substring{
    MTInteractiveTextViewAction *textViewAction = [[MTInteractiveTextViewAction alloc]init];
    textViewAction.action = action;
    textViewAction.actionRange = [self rangeForSubstring:substring occurrence:0];
    
    [self.actions addObject:textViewAction];
}


/**
 *  Adds the passed action for the passed occurrence of the passed substring
 *
 *  @param action     - action to be executed when the substring is tapped
 *  @param substring  - substring to attach the action to
 *  @param occurrence - desired occurrence of the substring
 */
- (void)addAction:(void (^)(void))action forSubstring:(NSString*)substring occurrence:(NSInteger)occurrence{
    MTInteractiveTextViewAction *textViewAction = [[MTInteractiveTextViewAction alloc]init];
    textViewAction.action = action;
    textViewAction.actionRange = [self rangeForSubstring:substring occurrence:occurrence];
    
    [self.actions addObject:textViewAction];
}


/**
 *  Adds the passed action for all occurrences of the passed substring
 *
 *  @param action     - action to be executed when the substring is tapped
 *  @param substring  - substring to attach the action to
 */
- (void)addAction:(void (^)(void))action forAllOccurrencesOfSubstring:(NSString*)substring{
    // get all occurrences
    NSArray *ranges = [self rangesForSubstring:substring];
    
    // attach action to all occurrences
    for (int index = 0; index < ranges.count; index++) {
        NSRange range = [[ranges objectAtIndex:index] rangeValue];
        
        MTInteractiveTextViewAction *textViewAction = [[MTInteractiveTextViewAction alloc]init];
        textViewAction.action = action;
        textViewAction.actionRange = range;
        
        [self.actions addObject:textViewAction];
    }
}


/**
 *  Adds the passed action for the passed range
 *
 *  @param action - action to be executed when the substring is tapped
 *  @param range  - range to attach the action to
 */
- (void)addAction:(void (^)(void))action forRange:(NSRange)range{
    MTInteractiveTextViewAction *textViewAction = [[MTInteractiveTextViewAction alloc]init];
    textViewAction.action = action;
    textViewAction.actionRange = range;
    
    [self.actions addObject:textViewAction];
}

#pragma mark - Calculating Bounding Rect

/**
 *  Returns the range of the nth occurrence of the passed substring
 *
 *  @param substring  - substring to look for
 *  @param occurrence - the desired occurrence
 *
 *  @return
 */
- (NSRange)rangeForSubstring:(NSString*)substring occurrence:(NSInteger)occurrence{
    
    NSRange range;
    NSRange remainingRange = NSMakeRange(0, self.attributedText.string.length);
    int occurrenceCount = 0;
    
    do {
        range = [[self.attributedText string] rangeOfString:substring options:0 range:remainingRange];
        
        if (range.location != NSNotFound) {
            occurrenceCount++;
        }
        
        remainingRange = NSMakeRange(range.location + range.length, self.attributedText.string.length - (range.location + range.length));
    } while (range.location != NSNotFound && occurrenceCount <= occurrence);
    
    return range;
}


/**
 *  Returns ranges of all occurrences of the passed substring
 *
 *  @param substring  - substring to look for
 *
 *  @return
 */
- (NSArray*)rangesForSubstring:(NSString*)substring{
    NSRange range;
    NSRange remainingRange = NSMakeRange(0, self.attributedText.string.length);
    NSMutableArray *ranges = [NSMutableArray array];
    
    do {
        range = [[self.attributedText string] rangeOfString:substring options:0 range:remainingRange];
        
        if (range.location != NSNotFound) {
            [ranges addObject:[NSValue valueWithRange:range]];
        }
        
        remainingRange = NSMakeRange(range.location + range.length, self.attributedText.string.length - (range.location + range.length));
    } while (range.location != NSNotFound);
    
    return [NSArray arrayWithArray:ranges];
}


/**
 *  Returns a bounding box for a character range within the text view
 *
 *  @param range - character range
 *
 *  @return
 */
- (CGRect)boundingRectForCharacterRange:(NSRange)range{
    // get glyph range
    NSRange glyphRange = [self.layoutManager glyphRangeForCharacterRange:range actualCharacterRange:nil];
    
    // get text container
    NSTextContainer *glyphContainer = [self.layoutManager textContainerForGlyphAtIndex:glyphRange.location effectiveRange:nil];
    
    // get bounding box for glyph range
    CGRect glyphRect = [self.layoutManager boundingRectForGlyphRange:glyphRange inTextContainer:glyphContainer];
    
    // apply text views top inset
    glyphRect.origin.y += self.textContainerInset.top;
    
    return glyphRect;
}

#pragma mark - Highlight Selection

- (void)setHighlightedColor:(UIColor *)highlightedColor{
    _highlightedColor = highlightedColor;
}

- (NSAttributedString*)highlightedStringWithRange:(NSRange)range{
    NSMutableAttributedString *highlightedString = [[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
    [highlightedString addAttribute:NSForegroundColorAttributeName value:_highlightedColor range:range];
    
    return highlightedString;
}

#pragma mark - Debug

/**
 *  Draws overlays for all actions
 */
- (void)highlightActions{
    // There is a difference between layouting a text view on iOS8 and iOS9
    // on iOS8 the boundingRectForCharacterRange method returns a wrong frame
    // but after the text view is finished laying out it returns the correct frame.
    // So the bounding boxes for the actions are correct because they are calculated
    // when the touch occurs (by then the text view layout is done) but the overlay
    // frames for debugging are not.
    // With this workaround it works.
    float_t delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // draw overlay for every action
        for (MTInteractiveTextViewAction *action in self.actions) {
            UIView *view = [[UIView alloc]initWithFrame:[self boundingRectForCharacterRange:action.actionRange]];
            view.backgroundColor = [UIColor colorWithRed:1.000 green:0.000 blue:0.000 alpha:0.500];
            [self addSubview:view];
        }
    });
}

@end
