//
//  UICollectionViewCell+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 02/09/16.
//
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (MTToolBox)

/**
 *  Returns a cell reuse identifier for the class
 *
 *  @return
 */
+ (NSString*)reuseIdentifier;

@end
