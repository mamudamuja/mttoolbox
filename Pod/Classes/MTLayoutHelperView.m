//
//  MTLayoutHelperView.m
//  Pods
//
//  Created by Milutin Tomic on 05/05/2017.
//
//

#import "MTLayoutHelperView.h"

@implementation MTLayoutHelperView

- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    
#if TARGET_INTERFACE_BUILDER
    self.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.3];    
#endif
}

@end
