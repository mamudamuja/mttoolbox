//
//  UITableView+MTUIToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import <UIKit/UIKit.h>

@interface UITableView (MTUIToolBox)

/**
 *  Registers Nib with name for table view
 *
 *  @param cellName - Class/Nib name
 */
- (void)registerCellWithName:(NSString*)cellName;

@end
