//
//  NSArray+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 17/01/16.
//
//

#import "NSArray+MTToolBox.h"

@implementation NSArray (MTToolBox)

#pragma mark - JSON

- (NSString *)mtJsonStringWithPrettyPrint:(BOOL)prettyPrint {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions) (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (error) {
        NSLog(@"ERROR while converting array to json:\n%@", error);
        return @"[]";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

#pragma mark - Enumeration

/**
 *  Returns an array where the mapFunction is applied 
 *  to every object of the original array.
 *
 *  @param mapFunction - function to apply to array objects
 */
- (NSArray *)map:(id (^)(id, NSInteger))mapFunction {
    return [self map:mapFunction ingnoreNil:NO];
}

/**
 *  Returns an array where the mapFunction is applied 
 *  to every object of the original array
 *
 *  @param mapFunction - function to apply to array objects
 *  @param ignore      - if this is set to YES, nil pointers can be returned from 
 *                       the mapping function and the object will be ignored (not added to the result array)
 */
- (NSArray *)map:(id (^)(id, NSInteger))mapFunction ingnoreNil:(BOOL)ignore {
    // result array
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:self.count];
    
    // apply mapping function
    NSInteger sourceIndex = 0;
    NSInteger targetIndex = 0;
    for (id obj in self) {
        id mappedObj = mapFunction(self[sourceIndex], sourceIndex);
        
        if (ignore) {
            // add object if not nil
            if (mappedObj) {
                result[targetIndex] = mappedObj;
                targetIndex++;
            }
        }else{
            result[targetIndex] = mappedObj;
            targetIndex++;
        }
        
        sourceIndex++;
    }
    
    return [result copy];
}

#pragma mark - Sorting

/**
 *  Returns copy of array sorted by key
 *
 *  @param key       - key to sort with
 *  @param ascending - ascending/descending
 *
 *  @return
 */
- (NSArray*)sortedByKey:(NSString*)key ascending:(BOOL)ascending{
    return [self sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:key ascending:ascending]]];
}

#pragma mark - Description

/**
 *  Returns a list of all object as a string
 *
 *  @return 
 */
- (NSString*)list{
    NSMutableString *result = [NSMutableString string];
    
    for (int index = 0; index < self.count; index++) {
        [result appendFormat:@"%i: %@\n", index, self[index]];
    }
    
    return [NSString stringWithString:result];
}

@end
