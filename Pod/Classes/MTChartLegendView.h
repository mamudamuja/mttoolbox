//
//  MTChartLegendView.h
//  Pods
//
//  Created by Milutin Tomic on 30/09/2016.
//
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    MTChartLegendViewLayoutVertical,
    MTChartLegendViewLayoutHorizontal
} MTChartLegendViewLayout;


@class MTChartLegendView, MTChartLegendItem;
@protocol MTChartLegendViewSupport <NSObject>

@required

/**
 *  Associates the legend view with the implementing chart view.
 *  The implementing chart view becomes the data source for this legend view
 *  and becomes responsible to update/refresh the legend when necessary.
 *
 *  @param legendView - legend view for the implementing chart to register
 */
- (void)registerLegendView:(nonnull MTChartLegendView *)legendView;

/**
 *  Provides the legend view with the necessary data to render the legend
 */
- (nullable NSArray <MTChartLegendItem *> *)legendItems;

@end


@interface MTChartLegendItem : NSObject

@property (nonatomic, strong, nonnull) UIColor *color;
@property (nonatomic, strong, nonnull) NSAttributedString *title;

@end


@interface MTChartLegendView : UIView

@property (nonatomic) MTChartLegendViewLayout legendLayout;

/**
 *  By default the legend view will use the data provided by the chart to render the legend items.
 *  It will use the color and title provided by the chart.
 *  To override this behaviour implement a custom block and style the item UI.
 */
@property (nonatomic, copy, nullable) void (^legendItemConfigurator)(UIView * _Nonnull colorIndicator, UILabel * _Nonnull titleLabel, NSInteger itemIndex);

/**
 *  Fetches data from datasource and updates UI
 */
- (void)reloadData;

/**
 *  Associates the legend view with the passed chart view.
 *  The chart then provides the legend with data and handles data reload.
 *  NOTE: the legend should only be attached to one chart only
 */
- (void)attachToChart:(nonnull id <MTChartLegendViewSupport>)chart;

@end
