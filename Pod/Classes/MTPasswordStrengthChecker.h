//
//  MTPasswordStrengthChecker.h
//  Pods
//
//  Created by Milutin Tomic on 14/03/16.
//
//

#import <Foundation/Foundation.h>

typedef enum{
    MTPasswordRequirementOneLetter      = 1 << 0,
    MTPasswordRequirementOneUpperCase   = 1 << 1,
    MTPasswordRequirementOneLowerCase   = 1 << 2,
    MTPasswordRequirementOneNumber      = 1 << 3,
    MTPasswordRequirementOneSymbol      = 1 << 4
} MTPasswordRequirement;


typedef enum {
    MTPasswordStrengthWeak,
    MTPasswordStrengthModerate,
    MTPasswordStrengthStrong
} MTPasswordStrength;


@interface MTPasswordStrengthChecker : NSObject

/**
 *  Checks if the password satisfies all of the specified requirements
 *
 *  @param password     - password
 *  @param requirements - requirements expected of the password
 *
 *  @return
 */
+ (BOOL)doesPassword:(NSString*)password satisfyRequirements:(MTPasswordRequirement)requirements;


/**
 *  Returns the passsword strength using all defined requirements and length for measurement
 *
 *  @param password     - password to check the strength of
 *
 *  @return
 */
+ (MTPasswordStrength)checkPassword:(NSString*)password;

@end
