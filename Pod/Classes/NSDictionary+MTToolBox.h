//
//  NSDictionary+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 23/09/2016.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary (MTToolBox)

#pragma mark - JSON

/**
 *  Returns a JSON String representation of the NSDictionary
 *
 *  @param prettyPrint - indicates wheter to use NSJSONWritingPrettyPrinted writing option
 */
- (NSString *)mtJsonStringWithPrettyPrint:(BOOL)prettyPrint;

@end
