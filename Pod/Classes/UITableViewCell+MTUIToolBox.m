//
//  UITableViewCell+MTUIToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import "UITableViewCell+MTUIToolBox.h"

@implementation UITableViewCell (MTUIToolBox)

/**
 *  Returns a cell reuse identifier for the class
 *
 *  @return
 */
+ (NSString*)reuseIdentifier{
    return [NSString stringWithFormat:@"%@_ID", NSStringFromClass([self class])];
}

@end
