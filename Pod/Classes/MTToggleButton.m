//
//  MTToggleButton.m
//  Pods
//
//  Created by Milutin Tomic on 13/01/16.
//
//

#import "MTToggleButton.h"
#import "MTUIToolBox.h"

@interface MTToggleButton()

// the button that triggers the action
@property (nonatomic, strong) UIButton *toggleButton;

@end


@implementation MTToggleButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initializeSubviews];
    }
    return self;
}


- (void)layoutSubviews{
    [_imageView fillSuperView];
    [_titleLabel fillSuperView];
    [_toggleButton fillSuperView];
    
    [self updateSubviews];
}


/**
 *  Toggles the control
 */
- (void)toggleAction{
    self.isOn = !self.isOn;
    [self updateSubviews];
    
    // set tint color if specified
    if (_isOn) {
        if (_tintColorWhenOn) {
            self.imageView.tintColor = _tintColorWhenOn;
            self.titleLabel.tintColor = _tintColorWhenOn;
        }
    }else{
        if (_tintColorWhenOff) {
            self.imageView.tintColor = _tintColorWhenOff;
            self.titleLabel.tintColor = _tintColorWhenOff;
        }
    }
    
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}


#pragma mark - Subviews

/**
 *  Initializes controls subviews
 */
- (void)initializeSubviews{
    if (_imageView) {
        return;
    }
    
    self.clipsToBounds = YES;
    
    // initialize image view
    _imageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    _imageView.translatesAutoresizingMaskIntoConstraints = YES;
    [self addSubview:_imageView];
    
    // initialize title label
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.translatesAutoresizingMaskIntoConstraints = YES;
    [self addSubview:_titleLabel];
    
    // initialize toggle button
    _toggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _toggleButton.backgroundColor = [UIColor clearColor];
    [_toggleButton addTarget:self action:@selector(toggleAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_toggleButton];
}


/**
 *  Displays the image specified for current toggle state
 */
- (void)updateImageView{
    if (_isOn) {
        if (_tintColorWhenOn) {
            self.imageView.image = [self.imageWhenOn imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        }else{
            self.imageView.image = self.imageWhenOn;
        }
    }else{
        if (_tintColorWhenOff) {
            self.imageView.image = [self.imageWhenOff imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        }else{
            self.imageView.image = self.imageWhenOff;
        }
    }
}


/**
 *  Displays the title specified for the current toggle state
 */
- (void)updateTitleLabel{
    // set text color
    if (_isOn) {
        if (_tintColorWhenOn) {
            self.titleLabel.textColor = _tintColorWhenOn;
        }else{
            self.titleLabel.textColor = [UIColor blackColor];
        }
    }else{
        if (_tintColorWhenOff) {
            self.titleLabel.textColor = _tintColorWhenOff;
        }else{
            self.titleLabel.textColor = [UIColor blackColor];
        }
    }
    
    // set text
    self.titleLabel.text = self.isOn ? self.titleWhenOn : self.titleWhenOff;
}


/**
 *  Updates subviews to show right content
 */
- (void)updateSubviews{
    [self updateImageView];
    [self updateTitleLabel];
}

#pragma mark - Setters

- (void)setIsOn:(BOOL)isOn{
    _isOn = isOn;
    
    [self updateSubviews];
}


- (void)setImageWhenOn:(UIImage *)imageWhenOn{
    _imageWhenOn = imageWhenOn;
    
    [self updateSubviews];
}


- (void)setImageWhenOff:(UIImage *)imageWhenOff{
    _imageWhenOff = imageWhenOff;
    
    [self updateSubviews];
}


- (void)setTitleWhenOn:(NSString *)titleWhenOn{
    _titleWhenOn = titleWhenOn;
    
    [self updateSubviews];
}


- (void)setTitleWhenOff:(NSString *)titleWhenOff{
    _titleWhenOff = titleWhenOff;
    
    [self updateSubviews];
}


- (void)setTintColorWhenOn:(UIColor *)tintColorWhenOn{
    _tintColorWhenOn = tintColorWhenOn;
    
    self.imageView.tintColor = tintColorWhenOn;
}


- (void)setTintColorWhenOff:(UIColor *)tintColorWhenOff{
    _tintColorWhenOff = tintColorWhenOff;
    
    self.imageView.tintColor = tintColorWhenOff;
}

@end
