//
//  MTMagnifyingGlass.h
//  Pods
//
//  Created by Milutin Tomic on 26/08/16.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MTMagnifyingGlass : UIView

/**
 *  View beeing magnified
 */
@property (nonatomic, strong) UIView *targetView;

/**
 *  The factor the target view needs to be scaled to.
 *
 *  Defaults to 1.5
 */
@property (nonatomic) IBInspectable CGFloat magnifyingFactor;


/**
 *  Displays magnifying view
 *  Manipulates alha and layer scale of the view
 *
 *  @param animated - enables/disables a fade animation
 */
- (void)showAnimated:(BOOL)animated;

/**
 *  Hides magnifying view
 *  Manipulates alha and layer scale of the view
 *
 *  @param animated - enables/disables a fade animation
 */
- (void)dismissAnimated:(BOOL)animated;

/**
 *  Focuses the magnifying view to a point on the target view
 *
 *  @param focusPoint - point on target view to magnify
 */
- (void)magnify:(CGPoint)focusPoint;

/**
 *  Rerenders the input of the target image view
 */
- (void)refreshInput;

@end
