//
//  MTChartLegendView.m
//  Pods
//
//  Created by Milutin Tomic on 30/09/2016.
//
//

#import "MTChartLegendView.h"
#import "MTToolBox.h"

@interface MTChartLegendView ()

@property (nonatomic, strong) NSArray<MTChartLegendItem *> *legendItems;
@property (nonatomic, strong) id <MTChartLegendViewSupport> chart;

@property (nonatomic, strong) UIView *legendContainer;

@end


@implementation MTChartLegendView

#pragma mark - API

- (void)attachToChart:(id<MTChartLegendViewSupport>)chart {
    MTAssertParam(chart);
    
    self.chart = chart;
    [chart registerLegendView:self];
    
    // fetch initial data
    [self reloadData];
}

- (void)reloadData {
    self.legendItems = [self.chart legendItems];
    [self _drawLegend];
}

#pragma mark - Overrides

- (CGSize)intrinsicContentSize {
    if (self.legendItems) {
        return self.legendContainer.frame.size;
    }else{
        return CGSizeMake(100, 100);
    }
}

#pragma mark - CA

- (void)setLegendLayout:(MTChartLegendViewLayout)legendLayout {
    _legendLayout = legendLayout;
    
    [self reloadData];
}

- (UIView *)legendContainer {
    if (!_legendContainer) {
        _legendContainer = [UIView new];
        _legendContainer.backgroundColor = [UIColor clearColor];
        [self addSubview:_legendContainer];
    }
    
    return _legendContainer;
}

#pragma mark - Private

/**
 *  Draws legend UI
 */
- (void)_drawLegend {
    // clean up container
    [self.legendContainer removeAllSubviews];
    
    // create items
    CGFloat maxSize = 0;
    UIView *previousItemContainer = nil;
    NSInteger itemIndex = 0;
    for (MTChartLegendItem *item in self.legendItems) {
        // setup label
        UILabel *titleLabel = [UILabel new];
        titleLabel.numberOfLines = 0;
        titleLabel.attributedText = item.title;
        
        // setup color indicator
        UIView *colorIndicator = [UIView new];
        colorIndicator.frame = CGRectMake(0, 0, 20, 20);
        colorIndicator.backgroundColor = item.color;
        
        // pass views for custom configuration
        if (self.legendItemConfigurator) {
            self.legendItemConfigurator(colorIndicator, titleLabel, itemIndex);
        }
        
        // size to fit after custom configuration
        [titleLabel sizeToFit];
        
        // wrap item UI into container
        UIView *itemContainer = [UIView new];
        [itemContainer addSubview:titleLabel];
        [itemContainer addSubview:colorIndicator];
        itemContainer.frame = CGRectMake(0, 0, colorIndicator.width + titleLabel.width + 5, MAX(colorIndicator.height, titleLabel.height));
        [self.legendContainer addSubview:itemContainer];
        
        // store max width/height
        switch (self.legendLayout) {
            case MTChartLegendViewLayoutVertical:
                maxSize = MAX(maxSize, itemContainer.width);
                break;
                
            case MTChartLegendViewLayoutHorizontal:
                maxSize = MAX(maxSize, itemContainer.height);
                break;
        }
        
        // position item UI in container
        colorIndicator.center = CGPointMake(colorIndicator.width/2.0, itemContainer.height/2.0);
        titleLabel.center = CGPointMake(itemContainer.width - titleLabel.width/2.0, itemContainer.height/2.0);
        
        // position item container in superview
        switch (self.legendLayout) {
            case MTChartLegendViewLayoutHorizontal:{
                itemContainer.center = CGPointMake(itemContainer.width/2.0 + (previousItemContainer ? previousItemContainer.rightEdgeValue + 5 : 0),
                                                   itemContainer.height/2.0);
            } break;
                
            case MTChartLegendViewLayoutVertical:{
                itemContainer.center = CGPointMake(itemContainer.width/2.0,
                                                   itemContainer.height/2.0 + (previousItemContainer ? previousItemContainer.bottomEdgeValue + 5 : 0));
            } break;
        }
        
        // set previous item container
        previousItemContainer = itemContainer;
        itemIndex++;
    }
    
    // update legend container
    switch (self.legendLayout) {
        case MTChartLegendViewLayoutHorizontal:{
            self.legendContainer.frame = CGRectMake(0, 0, previousItemContainer.rightEdgeValue, maxSize);
        } break;
            
        case MTChartLegendViewLayoutVertical:{
            self.legendContainer.frame = CGRectMake(0, 0, maxSize, previousItemContainer.bottomEdgeValue);
        } break;
    }
    
    // update intrinsic content size
    [self invalidateIntrinsicContentSize];
}

@end

#pragma mark - MTChartLegendItem

@implementation MTChartLegendItem

@end
