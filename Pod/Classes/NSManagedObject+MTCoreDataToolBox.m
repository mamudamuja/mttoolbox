//
//  NSManagedObject+MTCoreDataToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 22/12/15.
//
//

#import "NSManagedObject+MTCoreDataToolBox.h"
#import "MTCoreDataToolBox.h"

@implementation NSManagedObject (MTCoreDataToolBox)

+ (NSManagedObjectContext*)managedObjectContext{
    return [[MTCoreDataToolBox sharedInstance]managedContext];
}

#pragma mark - Inserting

/**
 *  Creates a new instance of the Core Data Managed Object
 *
 *  @return - new Entity
 */
+ (instancetype)newEntity{
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[self managedObjectContext]];
}

#pragma mark - Fetching

/**
 *  Returns all entities fetched by the passed fetcheReqeust
 *
 *  @param fetchRequest - the fetch reqeust to use for the query
 */
+ (NSArray *)entitiesWithFetchRequest:(NSFetchRequest *)fetchRequest {
    NSError *error;
    NSArray *results = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"MTCoreDataToolBox ERROR: %@", error);
        return nil;
    }
    
    return results;
}

/**
 *  Returns all entities of the callers class from the Core Data Store
 *
 *  @return - all entities
 */
+ (NSArray*)allEntities{
    NSFetchRequest *request = [self fetchRequest];
    
    NSError *error;
    NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"MTCoreDataToolBox ERROR: %@", error);
        return nil;
    }
    
    return results;
}


/**
 *  Returns all entities of the callers class from the Core Data Store matching the passed predicate
 *
 *  @param predicate - predicate for filtering entities
 *
 *  @return - all entities matching the predicate
 */
+ (NSArray*)entitiesMatchingPredicate:(NSPredicate*)predicate{
    NSFetchRequest *request = [self fetchRequest];
    
    // specify select criteria
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"couldn't fetch date. ERROR: %@", error);
        return nil;
    }
    
    return results;
}

#pragma mark - Deleting

/**
 *  Removes entity from Core Data Store
 */
- (void)deleteEntity{
    [[self managedObjectContext]deleteObject:self];
}


/**
 *  Removes all entities of the callers class from the Core Data Store
 */
+ (void)deleteAllEntities{
    // fetch all entities of callers class
    for (id entity in [self allEntities]) {
        [[self managedObjectContext]deleteObject:entity];
    }
}

@end
