//
//  NSString+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 25/01/16.
//
//

#import "NSString+MTToolBox.h"

@implementation NSString (MTToolBox)

#pragma mark - Conversion

/**
 *  Returns string from passed date formatted with the passed format
 *
 *  @param date   - date to convert to string
 *  @param format - date format to use for conversion
 */
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:format];
    return [formatter stringFromDate:date];
}

#pragma mark - Regex

/**
 *  Checks if the stinrg validates with the passed regexPattern
 *
 *  @param regexPattern - regexPatter to validate against
 *
 *  @return
 */
- (BOOL)validatesWithRegex:(NSString *)regexPattern {
    return [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", regexPattern] evaluateWithObject:self];
}

#pragma mark - Input Validation

/**
 *  Checks if the string is a valid international phone number
 *
 *  @param strictFilter - specifies if the filter also checks for a country code
 *
 *  @return
 */
- (BOOL)isValidPhoneNumberWithStrictFilter:(BOOL)strictFilter{
    NSString *phoneRegex;
    if (strictFilter) {
        phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    }else{
        phoneRegex = @"^((\\+)|(00))?[0-9]{6,14}$";
    }
    
    NSPredicate *phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phonePredicate evaluateWithObject:self];
}


/**
 *  Checks if the string is a valid email address
 *
 *  @param strictFilter - specifies if the filter should validate strictly
 *
 *  @return
 */
- (BOOL)isValidEmailAddressWithStrictFilter:(BOOL)strictFilter{
    NSString *strictFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = strictFilter ? strictFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

#pragma mark - Fractions

/**
 *  Returns passed number in superscript
 */
+ (NSString *)superscript:(NSInteger)number {
    NSDictionary *superscripts = @{@0: @"\u2070", @1: @"\u00B9", @2: @"\u00B2", @3: @"\u00B3", @4: @"\u2074", @5: @"\u2075", @6: @"\u2076", @7: @"\u2077", @8: @"\u2078", @9: @"\u2079"};
    return superscripts[@(number)];
}

/**
 *  Returns passed number in subscript
 */
+ (NSString *)subscript:(NSInteger)number {
    NSDictionary *subscripts = @{@0: @"\u2080", @1: @"\u2081", @2: @"\u2082", @3: @"\u2083", @4: @"\u2084", @5: @"\u2085", @6: @"\u2086", @7: @"\u2087", @8: @"\u2088", @9: @"\u2089"};
    return subscripts[@(number)];
}

/**
 *  Returs a fraction string with sub and superscripted numbers
 *
 *  @param numerator   - numerator
 *  @param denominator - denominator
 *
 *  @return
 */
+ (NSString *)fractionWithNumerator:(NSInteger)numerator denominator:(NSInteger)denominator {
    NSMutableString *result = [NSMutableString string];
    
    NSString *numeratorString = [NSString stringWithFormat:@"%li", (long)numerator];
    for (int i = 0; i < numeratorString.length; i++) {
        [result appendString:[self superscript:[[numeratorString substringWithRange:NSMakeRange(i, 1)] intValue]]];
    }
    [result appendString:@"/"];
    
    NSString *denominatorString = [NSString stringWithFormat:@"%li", (long)denominator];
    for (int i = 0; i < denominatorString.length; i++) {
        [result appendString:[self subscript:[[denominatorString substringWithRange:NSMakeRange(i, 1)] intValue]]];
    }
    return result;
}

@end
