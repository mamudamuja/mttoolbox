//
//  MTQuickLookViewController.h
//  Pods
//
//  Created by Milutin Tomic on 15/03/16.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuickLook/QuickLook.h>

@interface MTQuickLookViewControllerItem : NSObject <QLPreviewItem>

@property(readwrite, nonatomic, strong) NSURL *previewItemURL;
@property(readwrite, nonatomic, strong) NSString *previewItemTitle;

+ (MTQuickLookViewControllerItem*)quickLookItemWithItemURL:(NSURL*)itemUrl andItemTitle:(NSString*)itemTitle;
- (instancetype)initWithItemURL:(NSURL*)itemUrl andItemTitle:(NSString*)itemTitle;

@end


@interface MTQuickLookViewController : QLPreviewController

@property (nonatomic, strong) UIColor *barColor;

/**
 *  Initializes Presenter with a single preview item
 *
 *  @param item
 *
 *  @return
 */
- (instancetype)initWithItem:(MTQuickLookViewControllerItem*)item;


/**
 *  Initializes Presenter with a list of preview items
 *
 *  @param previewItems
 *
 *  @return
 */
- (instancetype)initWithPreviewItems:(NSArray <QLPreviewItem> *)previewItems;

@end
