//
//  MTPasswordStrengthChecker.m
//  Pods
//
//  Created by Milutin Tomic on 14/03/16.
//
//

#import "MTPasswordStrengthChecker.h"

#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"     //one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"     //one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"        //one or more numbers
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"    //one or more symbols


@implementation MTPasswordStrengthChecker


/**
 *  Checks if the password satisfies all of the specified requirements
 *
 *  @param password     - password
 *  @param requirements - requirements expected of the password
 *
 *  @return
 */
+ (BOOL)doesPassword:(NSString*)password satisfyRequirements:(MTPasswordRequirement)requirements{
    BOOL oneLetter = YES;
    BOOL oneLowerCase = YES;
    BOOL oneUpperCase = YES;
    BOOL oneSymbol = YES;
    BOOL oneNumber = YES;
    
    if (requirements & MTPasswordRequirementOneLetter) {
        oneLetter = [self password:password matchesPattern:REGEX_PASSWORD_ONE_LOWERCASE] || [self password:password matchesPattern:REGEX_PASSWORD_ONE_UPPERCASE];
    }
    
    if (requirements & MTPasswordRequirementOneLowerCase) {
        oneLowerCase = [self password:password matchesPattern:REGEX_PASSWORD_ONE_LOWERCASE];
    }
    
    if (requirements & MTPasswordRequirementOneUpperCase) {
        oneUpperCase = [self password:password matchesPattern:REGEX_PASSWORD_ONE_UPPERCASE];
    }
    
    if (requirements & MTPasswordRequirementOneNumber) {
        oneNumber = [self password:password matchesPattern:REGEX_PASSWORD_ONE_NUMBER];
    }
    
    if (requirements & MTPasswordRequirementOneSymbol) {
        oneSymbol = [self password:password matchesPattern:REGEX_PASSWORD_ONE_SYMBOL];
    }
    
    return oneLetter && oneLowerCase && oneUpperCase && oneSymbol && oneNumber;
}


/**
 *  Returns the passsword strength using all defined requirements and length for measurement
 *
 *  @param password     - password to check the strength of
 *
 *  @return
 */
+ (MTPasswordStrength)checkPassword:(NSString*)password{
    int len = (int)password.length;
    
    int strength = 0;
    
    if (len == 0) {
        return MTPasswordStrengthWeak;
    }else if (len <= 5){
        strength++;
    }else if (len <= 10){
        strength += 2;
    }else{
        strength += 3;
    }
    
    strength += [self password:password matchesPattern:REGEX_PASSWORD_ONE_LOWERCASE];
    strength += [self password:password matchesPattern:REGEX_PASSWORD_ONE_UPPERCASE];
    strength += [self password:password matchesPattern:REGEX_PASSWORD_ONE_NUMBER];
    strength += [self password:password matchesPattern:REGEX_PASSWORD_ONE_SYMBOL];
    
    if(strength <= 3){
        return MTPasswordStrengthWeak;
    }else if (strength < 6){
        return MTPasswordStrengthModerate;
    }else{
        return MTPasswordStrengthStrong;
    }
}


/**
 *  Checks if the passed password matches the passed regex pattern
 *
 *  @param password - password to check against the pattern
 *  @param pattern  - regex pattern
 *
 *  @return
 */
+ (BOOL)password:(NSString*)password matchesPattern:(NSString*)pattern{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, password.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:password options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = NO;
    
    if (matchRange.location != NSNotFound){
        didValidate = YES;
    }
    
    return didValidate;
}

@end
