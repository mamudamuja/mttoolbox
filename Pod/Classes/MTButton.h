//
//  MTButton.h
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MTButton : UIButton

/**
 *  Adds action block for the button to execute when the passed control event takes place.
 *  Unfortunately only one block can be added and only for one set of UIControlEvents
 *
 *  @param actionBlock  - action block
 *  @param controlEvent - events that triggers the action
 */
- (void)addAction:(void (^_Nonnull)())actionBlock forControlEvents:(UIControlEvents)controlEvent;

- (void)updateSubviews;

- (void)layoutSubviews;

// title to be displayed in button
// !!!NOTE!!!
// to assign a custom color to the title just set the textColor property of the button
// in addition to that you also need to set buttons title property to any string you want
// otherwise the textColor will not be set by iOS
@property (nonatomic, strong, nullable) IBInspectable NSString *buttonTitle;

// icon to be displayed in button
@property (nonatomic, strong, nullable) IBInspectable UIImage *buttonIcon;

// specifies if the image view should render the icon in template mode
// DEFAULTS to NO
@property (nonatomic) IBInspectable BOOL renderIconAsTemplate;

// specifies the offset for the title label from center
@property (nonatomic) IBInspectable CGPoint titleOffset;

// specifies the offset for the icon view from center
@property (nonatomic) IBInspectable CGPoint iconOffset;

// specifies the size of the icon view
// DEFAULTS to the size of the image, but is scaled down
// if it doesn't fit button bounds
@property (nonatomic) IBInspectable CGSize iconSize;

@end
