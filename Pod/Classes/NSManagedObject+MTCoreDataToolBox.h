//
//  NSManagedObject+MTCoreDataToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 22/12/15.
//
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (MTCoreDataToolBox)

#pragma mark - Inserting

/**
 *  Creates a new instance of the Core Data Managed Object
 *
 *  @return - new Entity
 */
+ (instancetype)newEntity;

#pragma mark - Fetching

/**
 *  Returns all entities fetched by the passed fetcheReqeust
 *
 *  @param fetchRequest - the fetch reqeust to use for the query
 */
+ (NSArray *)entitiesWithFetchRequest:(NSFetchRequest *)fetchRequest;

/**
 *  Returns all entities of the callers class from the Core Data Store
 *
 *  @return - all entities
 */
+ (NSArray*)allEntities;

/**
 *  Returns all entities of the callers class from the Core Data Store matching the passed predicate
 *
 *  @param predicate - predicate for filtering entities
 *
 *  @return - all entities matching the predicate
 */
+ (NSArray*)entitiesMatchingPredicate:(NSPredicate*)predicate;

#pragma mark - Deleting

/**
 *  Removes entity from Core Data Store
 */
- (void)deleteEntity;


/**
 *  Removes all entities of the callers class from the Core Data Store
 */
+ (void)deleteAllEntities;

@end
