//
//  MTPieChartView.h
//  Pods
//
//  Created by Milutin Tomic on 27/09/2016.
//
//

#import <UIKit/UIKit.h>

@class MTPieChartView;
@protocol MTChartLegendViewSupport;

@protocol MTPieChartViewDelegate <NSObject>

@required

/**
 *  Returns the number of segments for the passed pie chart instance
 *
 *  @param pieChartView - pie chart instance
 */
- (NSInteger)numberOfSegmentsInPiechartView:(nonnull MTPieChartView *)pieChartView;

/**
 *  Returns the value of the segment at index
 *
 *  @param pieChartView - pie chart instance
 *  @param index        - segment index
 */
- (CGFloat)piechartView:(nonnull MTPieChartView *)pieChartView valueForSegmentAtIndex:(NSInteger)index;

/**
 *  Returns the color of the segment at index
 *
 *  @param pieChartView - pie chart instance
 *  @param index        - segment index
 */
- (nonnull UIColor *)piechartView:(nonnull MTPieChartView *)pieChartView colorForSegmentAtIndex:(NSInteger)index;

@optional

/**
 *  Returns the title of the segment at index.
 *  The title is the text that is displayed inside of a segment
 *
 *  @param pieChartView - pie chart instance
 *  @param index        - segment index
 */
- (nullable NSAttributedString *)piechartView:(nonnull MTPieChartView *)pieChartView titleForSegmentAtIndex:(NSInteger)index;

/**
 *  Returns the color of the segment title at index
 *
 *  @param pieChartView - pie chart instance
 *  @param index        - segment index
 */
- (nonnull UIColor *)piechartView:(nonnull MTPieChartView *)pieChartView colorForTitleAtIndex:(NSInteger)index;

/**
 *  Returns the description of the segment at index.
 *  The description is displayed outside the piechart and connected to the segment with a line
 *
 *  @param pieChartView - pie chart instance
 *  @param index        - segment index
 */
- (nullable NSAttributedString *)piechartView:(nonnull MTPieChartView *)pieChartView descriptionForSegmentAtIndex:(NSInteger)index;

@end


@interface MTPieChartView : UIView <MTChartLegendViewSupport>

@property (nonatomic) CGFloat innerRadius;
@property (nonatomic, assign, nullable) id <MTPieChartViewDelegate> delegate;

/**
 *  Refetches data from delegate and updates UI
 */
- (void)reloadData;

@end
