//
//  MTCache.h
//  Pods
//
//  Created by Milutin Tomic on 19/02/2017.
//
//

#import <Foundation/Foundation.h>

@interface MTCache : NSObject

/**
 *  Returns the cache with the given nameSpace or the default cache if the nameSpace was nil.
 *  If no cache for the passed nameSpace was found one will be created.
 *
 *  @param nameSpace - name of the cache
 *
 *  @return - cache or nil
 */
+ (nullable NSCache *)cacheWithNameSpace:(nullable NSString *)nameSpace;

/**
 *  Returns the object from the default cache associated with the given key.
 *
 *  @param key - key associated with the desired object
 *
 *  @return - associated object or nil
 */
+ (nullable id)objectForKey:(nonnull id)key;

/**
 *  Returns the object from the cache with the given namespace associated with the given key.
 *  If the namespace is nil the default cache will be searched.
 *
 *  @param key       - key associated with the desired object
 *  @param nameSpace - name of the cache
 *
 *  @return - associated object or nil
 */
+ (nullable id)objectForKey:(nonnull id)key nameSpace:(nullable NSString *)nameSpace;

/**
 *  Returns the object from the cache with the given namespace associated with the given key.
 *  If the namespace is nil the default cache will be searched.
 *
 *  @param key        - key associated with the desired object
 *  @param nameSpace  - name of the cache
 *  @param ignoreDisk - if this is set to yes only the in-memory-cache will be checked
 *                      otherwise also the disk will be checked
 *
 *  @return - associated object or nil
 */
+ (nullable id)objectForKey:(nonnull id)key nameSpace:(nullable NSString *)nameSpace ignoreDisk:(BOOL)ignoreDisk;

/**
 *  Sets the value of the specified key in the cache.
 *  This caches the value in memory but the value will not be persisted.
 *
 *  @param object - value
 *  @param key    - key
 */
+ (void)setObject:(nonnull id)object forKey:(nonnull id)key;

/**
 *  Sets the value of the specified key in the cache.
 *  If the cache with the passed nameSpace does not exist, it will be created.
 *  If the nameSpace is nil the default cache will be used.
 *
 *  @param object    - value
 *  @param key       - key
 *  @param nameSpace - name of the cache
 */
+ (void)setObject:(nonnull id)object forKey:(nonnull id)key nameSpace:(nullable NSString *)nameSpace;

/**
 *  Sets the value of the specified key in the cache.
 *  If the cache with the passed nameSpace does not exist, it will be created.
 *  If the nameSpace is nil the default cache will be used.
 *
 *  @param object    - value
 *  @param key       - key
 *  @param nameSpace - name of the cache
 *  @param persist   - the object will additionaly be saved to disk (/Library/Caches/nameSpace/)
 *
 *  @return - whether the object was persisted successfully
 */
+ (BOOL)setObject:(nonnull id)object forKey:(nonnull id)key nameSpace:(nullable NSString *)nameSpace persist:(BOOL)persist;

/**
 *  Removes object for the passed key from the default cache (in-memory and from disk).
 *
 *  @param key - key of the object
 */
+ (void)removeObjectForKey:(nonnull id)key;

/**
 *  Removes object for the passed key from the cache with the passed nameSpace (in-memory and from disk).
 *  If the nameSpace is nil the object will be removed from the default cache.
 *
 *  @param key       - key of the object
 *  @param nameSpace - name of the cache
 */
+ (void)removeObjectForKey:(nonnull id)key nameSpace:(nullable NSString *)nameSpace;

/**
 *  Removes object for the passed key from the cache with the passed nameSpace (in-memory and from disk).
 *  If the nameSpace is nil the object will be removed from the default cache.
 *
 *  @param key        - key of the object
 *  @param nameSpace  - name of the cache
 *  @param ignoreDisk - if this is set to yes, the object will only be removed from the in-memory-cache
 *                      otherwise also from the disk cache
 */
+ (void)removeObjectForKey:(nonnull id)key nameSpace:(nullable NSString *)nameSpace ignoreDisk:(BOOL)ignoreDisk;

/**
 *  Removes all objects from the default cache
 */
+ (void)removeAllObjects;

/**
 *  Removes all objects from the cache with the passed nameSpace.
 *  If nameSpace is nil the default cache will be emptied.
 *
 *  @param nameSpace - name of the cache
 */
+ (void)removeAllObjectsInNameSpace:(nullable NSString *)nameSpace;

/**
 *  Removes all objects from the cache with the passed nameSpace.
 *  If nameSpace is nil the default cache will be emptied.
 *
 *  @param nameSpace  - name of the cache
 *  @param ignoreDisk - if this is set to yes only the in-memory cache will be emptied
 *                      otherwise also the disk cache
 */
+ (void)removeAllObjectsInNameSpace:(nullable NSString *)nameSpace ignoreDisk:(BOOL)ignoreDisk;


// MTTODO: IMPLEMEMT AND TEST
+ (BOOL)persistObjectForKey:(nonnull id)key;
+ (BOOL)persistObjectForKey:(nonnull id)key nameSpace:(nullable NSString *)nameSpace;
+ (void)persistCache;
+ (void)persistCacheWithNameSpace:(nullable NSString *)nameSpace;

@end
