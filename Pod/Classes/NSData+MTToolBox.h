//
//  NSData+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 15/03/16.
//
//

#import <Foundation/Foundation.h>

@interface NSData (MTToolBox)

#pragma mark - Writing To File

/**
 *  Saves data to the passed subpath of the documents directory
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return - YES if save was successfull, otherwise NO
 */
- (BOOL)saveFileInDocumentsDirectoryWithPath:(NSString*)path;

/**
 *  Saves data to the passed subpath of the Caches directory
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return - YES if save was successfull, otherwise NO
 */
- (BOOL)cacheFileInCachesDirectoryWithPath:(NSString *)path;

#pragma mark - Reading from File

/**
 *  Initializes NSData object with content of the file at passed path in the documents directory
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return - returns NSData object with contents of specified file at path
 */
+ (NSData*)dataFromDocumentsDirectoryWithPath:(NSString*)path;

/**
 *  Initializes NSData object with content of the file at passed path in the Caches directory
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return - returns NSData object with contents of specified file at path
 */
+ (NSData *)dataFromCachesDirectoryWithPath:(NSString *)path;

@end
