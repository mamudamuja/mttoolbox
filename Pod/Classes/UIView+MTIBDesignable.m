//
//  UIView+MTIBDesignable.m
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import "UIView+MTIBDesignable.h"

@implementation UIView (MTIBDesignable)

- (void)setCornerRadius:(CGFloat)cornerRadius{
    self.layer.cornerRadius = cornerRadius;
}

- (CGFloat)cornerRadius{
    return self.layer.cornerRadius;
}


- (void)setBorderColor:(UIColor *)borderColor{
    self.layer.borderColor = [borderColor CGColor];
}

- (UIColor*)borderColor{
    return [UIColor colorWithCGColor:self.layer.borderColor];
}


- (void)setBorderWidth:(CGFloat)borderWidth{
    self.layer.borderWidth = borderWidth;
}

- (CGFloat)borderWidth{
    return self.layer.borderWidth;
}

@end
