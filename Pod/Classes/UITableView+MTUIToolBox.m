//
//  UITableView+MTUIToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import "UITableView+MTUIToolBox.h"

@implementation UITableView (MTUIToolBox)

/**
 *  Registers Nib with name for table view
 *
 *  @param cellName - Class/Nib name
 */
- (void)registerCellWithName:(NSString*)cellName{
    [self registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:[NSString stringWithFormat:@"%@_ID", cellName]];
}

@end
