//
//  UITextField+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 23/01/16.
//
//

#import "UITextField+MTToolBox.h"

/* === APPEARANCE ATTRIBUTE KEYS === */
NSString * const MTTitleColorKey = @"MTTitleColorKey";
NSString * const MTTitleFontKey = @"MTTitleFontKey";
NSString * const MTToolBarStyleKey = @"MTToolBarStyleKey";


@implementation UITextField (MTToolBox)

/**
 *  Adds a tool bar with a done button which dismisses the caller (UITextField)
 *
 *  @param buttonTitle - title of the done button
 */
- (void)addDoneButtonWithTitle:(NSString*)buttonTitle{
    [self addDoneButtonWithTitle:buttonTitle andAppearanceAttributes:@{}];
}


/**
 *  Adds a tool bar with a done button which dismisses the caller (UITextField)
 *
 *  @param buttonTitle          - title of the done button
 *  @param appearanceAttributes - appearance attributes (doesn't need to contains all keys)
 */
- (void)addDoneButtonWithTitle:(NSString*)buttonTitle andAppearanceAttributes:(NSDictionary*)appearanceAttributes{
    self.inputAccessoryView = [self accessoryViewWithDoneButtonTitle:buttonTitle andAppearanceAttributes:appearanceAttributes];;
}


/**
 *  Creates tool bar with a done button which dismisses the caller (UITextField)
 *
 *  @param buttonTitle - done button title
 *
 *  @return
 */
- (UIToolbar*)accessoryViewWithDoneButtonTitle:(NSString*)buttonTitle{
    return [self accessoryViewWithDoneButtonTitle:buttonTitle andAppearanceAttributes:@{}];
}


/**
 *  Creates tool bar with a done button which dismisses the caller (UITextField)
 *
 *  @param buttonTitle          - done button title
 *  @param appearanceAttributes - appearance attributes
 *
 *  @return
 */
- (UIToolbar*)accessoryViewWithDoneButtonTitle:(NSString*)buttonTitle andAppearanceAttributes:(NSDictionary*)appearanceAttributes{
    // get title attributes
    UIColor *titleColor = [appearanceAttributes objectForKey:MTTitleColorKey];
    UIFont *titleFont = [appearanceAttributes objectForKey:MTTitleFontKey];
    UIBarStyle barStyle = UIBarStyleDefault;
    if ([appearanceAttributes objectForKey:MTToolBarStyleKey]) {
        barStyle = [[appearanceAttributes objectForKey:MTToolBarStyleKey] integerValue];
    }
    
    // done button
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:buttonTitle forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:titleFont == nil ? [UIFont systemFontOfSize:16] : titleFont];
    [doneButton setTitleColor:titleColor == nil ? doneButton.tintColor : titleColor forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(resignFirstResponder) forControlEvents:UIControlEventTouchUpInside];
    [doneButton sizeToFit];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    toolBar.barStyle = barStyle;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithCustomView:doneButton]];
    [toolBar sizeToFit];

    return toolBar;
}

@end
