//
//  MTTransitionAnimator.h
//  Pods
//
//  Created by Milutin Tomic on 10/09/16.
//
//

#import <Foundation/Foundation.h>

@class MTTransitionAnimator;

typedef enum : NSUInteger {
    MTTransitionAnimatorOperationNone,
    MTTransitionAnimatorOperationPush,
    MTTransitionAnimatorOperationPop,
    MTTransitionAnimatorOperationPresent,
    MTTransitionAnimatorOperationDismiss
} MTTransitionAnimatorOperation;

#pragma mark - MTTransitionAnimatorViewController

/**
 *  Protocol can be implemented by presenting and/or presented view controller in order
 *  to expose a transitionContext (like UIViews etc.) for a more complex transition animations
 */
@protocol MTTransitionAnimatorViewController <NSObject>

@optional

/**
 *  Returns a object relevant to the transition animation
 */
- (nullable id)transitionContext;

/**
 *  Returns a object relevant to the transition animation but gives more information about the current transition
 *
 *  @param fromViewController - the view controller present before the transition
 *  @param toViewController   - the view controller present after the transition
 *  @param operation          - the operation beeing performed
 */
- (nullable id)transitionContextForTransitionFromViewController:(nonnull UIViewController<MTTransitionAnimatorViewController> *)fromViewController
                                               toViewController:(nonnull UIViewController<MTTransitionAnimatorViewController> *)toViewController
                                                  withOperation:(MTTransitionAnimatorOperation)operation;

@end

#pragma mark - MTTransitionAnimatorDelegate

/**
 *  Must be implemented by the transition animator delegate
 */
@protocol MTTransitionAnimatorDelegate <NSObject>

@required

/**
 *  Called while setting up the transition.
 *  Also used during noninteractive transition to determine when to call completeTransition.
 *
 *  @param animator  - transition animator
 *  @param operation - operation beeing performed
 *
 *  @return - animation duration
 */
- (NSTimeInterval)transitionAnimator:(nonnull MTTransitionAnimator *)animator
      transitionDurationForOperation:(MTTransitionAnimatorOperation)operation;

/**
 *  Called when a transition operation is performed.
 *  Implement this to provide the transition animator with the transition animation.
 *  The delegate does not need to complete the transition. The transition animator does that internally.
 *  For this to work properly the delegate needs to implement the transitionAnimator:transitionDurationForOperation: method
 *  and provide the transition animator with the transition length (the total length of the animations beeing performed)
 *
 *  @param animator           - transition animator instance
 *  @param fromViewController - the view controller present before the transition
 *  @param toViewController   - the view controller present after the transition
 *  @param animationContainer - the view displayed during transition (perform all your animations on this view)
 *  @param transitionContext  - the transition context (should be used to get the proposed frames of the view controller view's)
 *  @param operation          - the operation beeing performed
 */
- (void (^ _Nonnull)(void))transitionAnimator:(nonnull MTTransitionAnimator *)animator
animationBlockForTransitionFromViewController:(nonnull UIViewController<MTTransitionAnimatorViewController> *)fromViewController
                        toViewController:(nonnull UIViewController<MTTransitionAnimatorViewController> *)toViewController
                      animationContainer:(nonnull UIView *)animationContainer
                        transitionContext:(nonnull id <UIViewControllerContextTransitioning>)transitionContext
                           withOperation:(MTTransitionAnimatorOperation)operation;

@optional

/**
 *  Called when a transition operation is performed.
 *  If this method returns YES, the delegate also needs to update the transition by calling the
 *  updateInteractiveTransition: method and finish the transition by calling finishInteractiveTransition 
 *  or cancel the transition by calling cancelInteractiveTransition. 
 *  If the transition is cancelled ([transitionContext transitionWasCancelled]) the delegate has to undo all changes the transition made to the view hierarchy.
 *  If the method returns NO the transition performed will noninteractive.
 *
 *  @param animator           - transition animator instance
 *  @param fromViewController - the view controller present before the transition
 *  @param toViewController   - the view controller present after the transition (in case of a modal dismissal transition this vc nil)
 *  @param animationContainer - the view displayed during transition
 *  @param transitionContext  - the transition context (can be used to get the proposed frames of the view controller view's)
 *  @param operation          - the operation beeing performed
 */
- (BOOL)transitionAnimator:(nonnull MTTransitionAnimator *)animator
shouldStartInteractiveTransitionFromViewController:(nonnull UIViewController *)fromViewController
          toViewController:(nullable UIViewController *)toViewController
             withOperation:(MTTransitionAnimatorOperation)operation;

@end

#pragma mark - MTTransitionAnimatorDelegate

@interface MTTransitionAnimator : NSObject

/**
 *  Initializes transition animator with a delegate who will receive all the
 *  neceserry calls to be able to perform the animation
 *
 *  @param delegate - transition animator delegate
 *
 *  @return
 */
- (nonnull instancetype)initWithDelegate:(nonnull id <MTTransitionAnimatorDelegate>)delegate;

/**
 *  Initializes transition animator with a delegate who will receive all the
 *  neceserry calls to be able to perform the animation and an navigation controller.
 *  The animator will assign self as navigationController delegate and animate all its transitions.
 *
 *  @param delegate             - transition animator delegate
 *  @param navigationController - navigationController whos transitions need animating
 *
 *  @return
 */
- (nonnull instancetype)initWithDelegate:(nonnull id <MTTransitionAnimatorDelegate>)delegate navigationController:(nonnull UINavigationController *)navigationController;

/**
 *  If animator is initialised with a navigation controller it becomes it's delegate.
 *  This method decouples the animator from the navigation controller.
 */
- (void)decoupleFromNavigationController;

/**
 *  Presents view controller modally
 *
 *  @param viewController     - view controller to present
 *  @parem fromViewController - view controller that is presenting
 *  @param completion         - completion handler executed after the transition is finished
 */
- (void)presentViewController:(nonnull UIViewController *)viewController
           fromViewController:(nonnull UIViewController *)fromViewController
                   completion:(void (^ _Nullable)(void))completion;

/**
 *  Dismisses view controller
 *
 *  @param viewController     - view controller to dismiss
 *  @param completion         - completion handler executed after the transition is finished
 */
- (void)dismissViewController:(nonnull UIViewController *)viewController
                   completion:(void (^ _Nullable)(void))completion;

/**
 *  Pushes view controller onto passed navigation controller
 *
 *  @param viewController       - view controller to be pushed
 *  @param navigationController - navigation controller to push the passed view controller on
 */
- (void)pushViewController:(nonnull UIViewController *)viewController
  ontoNavigationController:(nonnull UINavigationController *)navigationController;

/**
 *  Pops the top view controller from the passed navigation controller
 *
 *  @param navigationController - navigation controller to perform the pop
 */
- (void)popViewControllerFromNavigationController:(nonnull UINavigationController *)navigationController;

/**
 *  Updates the interactiveTransition beeing performed currently
 *
 *  @param percentComplete - transition completion in percent (0..1)
 */
- (void)updateInteractiveTransition:(CGFloat)percentComplete;

/**
 *  Cancels the interactiveTransition beeing performed currently
 */
- (void)cancelInteractiveTransition;

/**
 *  Finishes the interactiveTransition beeing performed currently
 */
- (void)finishInteractiveTransition;

@end
