//
//  MTImagePickerViewController.m
//  Pods
//
//  Created by Milutin Tomic on 14/12/2016.
//
//

#import "MTImagePickerViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface MTImagePickerViewController ()

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCapturePhotoOutput *photoOutput;
@property (nonatomic, strong) AVCaptureStillImageOutput *stillImageOutput;

@end


@implementation MTImagePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}

@end
