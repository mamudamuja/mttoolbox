//
//  MTDropDownMenu.m
//  Pods
//
//  Created by Milutin Tomic on 12/09/16.
//
//

#import "MTDropDownMenu.h"
#import "MTToolBox.h"

static const NSTimeInterval kAnimationDuration = 0.3;
static const CGFloat kDefaultMenuRelativeWidth = 0.7; // 70% of the target view
static const CGFloat kMenuPaddingToWindow = 5;


@interface MTDropDownMenu () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) id<MTDropDownMenuDelegate> delegate;

@property (nonatomic, strong) UIButton *menuContainerView;
@property (nonatomic, strong) UIView *targetView;

@end


@implementation MTDropDownMenu

#pragma mark - API

- (instancetype)initWithDelegate:(id<MTDropDownMenuDelegate>)delegate anchorView:(UIView *)anchorView {
    self = [super init];
    if (self) {
        MTAssertParam(delegate);
        
        self.delegate = delegate;
        self.anchorView = anchorView;
        
        // setup table view
        self.tableView = [UITableView new];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 8, 0, 16);
        self.tableView.separatorColor = [UIColor colorWithRed:0.3333 green:0.3333 blue:0.3333 alpha:0.5];
        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:[UITableViewCell reuseIdentifier]];
        [self addSubview:self.tableView];
        
        // initialize default property values
        self.dropDirection = MTDropDownMenuDropDirectionAuto;
        self.dismissMode = MTDropDownMenuDismissModeAuto;
        self.dimmerColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];
    }
    return self;
}

- (void)showAnimated:(BOOL)animated inView:(UIView *)targetView {
    // reload table view
    [self.tableView reloadData];
    
    // display cell separator if no custom views are used
    if ([self.delegate respondsToSelector:@selector(dropDownMenu:viewForItemAtIndex:)]) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }else{
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    
    // add menu to window
    self.targetView = targetView ?: [UIApplication sharedApplication].keyWindow;
    [self.targetView addSubview:self.menuContainerView];
    self.menuContainerView.alpha = 0;
    
    // layout views
    [self _layoutMenu];
    
    [UIView animateWithDuration:animated ? kAnimationDuration : 0 animations:^{
        self.menuContainerView.alpha = 1;
    } completion:^(BOOL finished) {
        [self.tableView flashScrollIndicators];
    }];
}

- (void)dismiss:(BOOL)animated {
    [UIView animateWithDuration:animated ? kAnimationDuration : 0 animations:^{
        self.menuContainerView.alpha = 0;
    } completion:^(BOOL finished) {
        // clean up
        [self.menuContainerView removeFromSuperview];
        self.tableView.contentOffset = CGPointMake(0, 0);
    }];
}

#pragma mark - Lifecycle

- (void)layoutSubviews {
    float_t delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
       [self _layoutMenu];
    });
}

#pragma mark - CA

- (UIButton *)menuContainerView {
    if (!_menuContainerView) {
        _menuContainerView = [UIButton buttonWithType:UIButtonTypeCustom];
        _menuContainerView.backgroundColor = self.dimmerColor;
        [_menuContainerView addTarget:self action:@selector(_didTouchDimmer) forControlEvents:UIControlEventTouchUpInside];
        
        // add menu to container
        [_menuContainerView addSubview:self];
    }
    
    return _menuContainerView;
}

- (void)setDimmerColor:(UIColor *)dimmerColor {
    _dimmerColor = dimmerColor;
    _menuContainerView.backgroundColor = dimmerColor;
}

#pragma mark - Private

/**
 *  Lays out menu with respect to the setup properties
 */
- (void)_layoutMenu {
    // get size preferred size from delegate
    CGSize preferredSize = CGSizeZero;
    CGSize finalSize = CGSizeZero;
    if ([self.delegate respondsToSelector:@selector(preferredSizeForDropDownMenu:)]) {
        preferredSize = [self.delegate preferredSizeForDropDownMenu:self];
        finalSize = preferredSize;
    }
    
    // if delegate hasn't provided width
    if (preferredSize.width == 0) {
        finalSize.width = self.anchorView ? self.anchorView.width : self.targetView.width * kDefaultMenuRelativeWidth;
    }
    
    // get height of all items together
    CGFloat heightThatFitsAllItems = 0;
    for (NSInteger item = 0; item < [self.delegate numberOfItemsInDropDownMenu:self]; item++) {
        heightThatFitsAllItems += [self.delegate dropDownMenu:self heightForItemAtIndex:item];
    }
    
    // if delegate hasn't provided height
    if (preferredSize.height == 0) {
        finalSize.height = heightThatFitsAllItems;
    }
    
    
    // get preferred offset from delegate
    CGPoint preferredOffset = CGPointZero;
    if ([self.delegate respondsToSelector:@selector(preferredOffsetForDropDownMenu:)]) {
        preferredOffset = [self.delegate preferredOffsetForDropDownMenu:self];
    }
    
    // menu position
    if (!self.anchorView) {// no anchor, center in screen
        CGPoint menuCenter = CGPointMake(self.targetView.width/2.0 + preferredOffset.x, self.targetView.height/2.0 + preferredOffset.y);
        
        // create temporary frame
        CGRect menuFrame = CGRectMake(menuCenter.x - finalSize.width/2.0, menuCenter.y - finalSize.height/2.0, finalSize.width, finalSize.height);
        
        // check if it exceeds bounds
        if (!CGRectContainsRect(CGRectInset(self.targetView.bounds, kMenuPaddingToWindow, kMenuPaddingToWindow), menuFrame)) {// if so clamp it
            menuFrame = CGRectIntersection(CGRectInset(self.targetView.bounds, kMenuPaddingToWindow, kMenuPaddingToWindow), menuFrame);
        }
        
        // set menu frame
        self.frame = menuFrame;
    }else{// calculate position relative to anchorView
        CGRect anchorFrame = [self.targetView convertRect:self.anchorView.frame fromView:self.anchorView.superview];
        
        if (self.dropDirection == MTDropDownMenuDropDirectionDown ||
            self.dropDirection == MTDropDownMenuDropDirectionUp) {// if drop direction is fixed
            
            // create temporary frame
            CGRect menuFrame = CGRectMake(
                anchorFrame.origin.x + preferredOffset.x,
                self.dropDirection == MTDropDownMenuDropDirectionDown ? anchorFrame.size.height + anchorFrame.origin.y + preferredOffset.y : anchorFrame.origin.y - finalSize.height - preferredOffset.y,
                finalSize.width,
                finalSize.height
            );
            
            // check if it exceeds bounds
            if (!CGRectContainsRect(CGRectInset(self.targetView.bounds, kMenuPaddingToWindow, kMenuPaddingToWindow), menuFrame)) {// if so clamp it
                menuFrame = CGRectIntersection(CGRectInset(self.targetView.bounds, kMenuPaddingToWindow, kMenuPaddingToWindow), menuFrame);
            }
            
            // set menuFrame
            self.frame = menuFrame;
        }else{// auto drop direction
            // this is how to decide if what drop direction is fitting better for the drop menu
            // if the whole menu fits when dropped down
            // leave it down. If it intersects with the allowed menu bounds
            // pick the drop direction where the menu has more space
            
            // try down
            CGRect menuFrame = CGRectMake(anchorFrame.origin.x + preferredOffset.x, anchorFrame.origin.y + anchorFrame.size.height + preferredOffset.y, finalSize.width, finalSize.height);
            
            // check if it exceeds bounds
            if (!CGRectContainsRect(CGRectInset(self.targetView.bounds, kMenuPaddingToWindow, kMenuPaddingToWindow), menuFrame)) {
                CGRect upFrame = CGRectMake(anchorFrame.origin.x + preferredOffset.x, anchorFrame.origin.y - finalSize.height - preferredOffset.y, finalSize.width, finalSize.height);
                
                // calculate areas of the two possibilities
                CGRect downAreaFrame = CGRectIntersection(CGRectInset(self.targetView.bounds, kMenuPaddingToWindow, kMenuPaddingToWindow), menuFrame);
                CGRect upAreaFrame = CGRectIntersection(CGRectInset(self.targetView.bounds, kMenuPaddingToWindow, kMenuPaddingToWindow), upFrame);
                CGFloat areaOfDownFrame = downAreaFrame.size.width * downAreaFrame.size.height;
                CGFloat areaOfUpFrame = upAreaFrame.size.width * upAreaFrame.size.height;
                
                // pick bigger area
                menuFrame = areaOfDownFrame >= areaOfUpFrame ? downAreaFrame : upAreaFrame;
            }
            
            // set menu frame
            self.frame = menuFrame;
        }
    }
    
    // everything is layed out, check if all items fit and enable scrolling if needed
    [self.tableView setScrollEnabled:self.height < heightThatFitsAllItems];
    
    // layout table view
    [self.tableView fillSuperView];
    
    // layout dimmer
    [self.menuContainerView fillSuperView];
}

/**
 *  Called when the user touches the dimmer view
 */
- (void)_didTouchDimmer {
    if (self.dismissMode == MTDropDownMenuDismissModeAuto) {
        [self dismiss:YES];
    }
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.delegate respondsToSelector:@selector(numberOfItemsInDropDownMenu:)]) {
        return [self.delegate numberOfItemsInDropDownMenu:self];
    }else{
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"The delegate must implement the numberOfItemsInDropDownMenu: method" userInfo:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(dropDownMenu:heightForItemAtIndex:)]) {
        return [self.delegate dropDownMenu:self heightForItemAtIndex:indexPath.row];
    }else{
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"The delegate must implement the dropDownMenu:heightForItemAtIndex: method" userInfo:nil];
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[UITableViewCell reuseIdentifier]];
    
    // clean up reused cell
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    if ([self.delegate respondsToSelector:@selector(dropDownMenu:viewForItemAtIndex:)]) {// delegate provides custom view
        [cell.contentView addSubview:[self.delegate dropDownMenu:self viewForItemAtIndex:indexPath.row]];
    }else if ([self.delegate respondsToSelector:@selector(dropDownMenu:titleForItemAtIndex:)]){// set title provided by delegate
        cell.textLabel.text = [self.delegate dropDownMenu:self titleForItemAtIndex:indexPath.row];
    }else{
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"The delegate must implement one of the following methods:\n- dropDownMenu:titleForItemAtIndex:\n- dropDownMenu:viewForItemAtIndex:" userInfo:nil];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(dropDownMenu:didSelectItemAtIndex:)]) {
        [self.delegate dropDownMenu:self didSelectItemAtIndex:indexPath.row];
    }
    
    if (self.dismissMode == MTDropDownMenuDismissModeAuto ||
        self.dismissMode == MTDropDownMenuDismissModeOnSelect) {// dismiss
        [self dismiss:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
