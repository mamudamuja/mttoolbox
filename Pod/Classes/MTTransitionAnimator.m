//
//  MTTransitionAnimator.m
//  Pods
//
//  Created by Milutin Tomic on 10/09/16.
//
//

#import "MTTransitionAnimator.h"
#import "MTToolBox.h"

@interface MTTransitionAnimator () <UIViewControllerAnimatedTransitioning, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate>

@property (nonatomic, assign) id <MTTransitionAnimatorDelegate> delegate;
@property MTTransitionAnimatorOperation currentOperation;
@property (nonatomic, strong) UIViewController *fromVC;
@property (nonatomic, strong) UIViewController *toVC;
@property (nonatomic, strong) id<UIViewControllerContextTransitioning> transitionContext;

@property (nonatomic, strong) UINavigationController *targetNavigationController;
@property (nonatomic, strong) UINavigationController *temporaryNavigationController;
@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactiveTransition;

@end


@implementation CATransformLayer (Setters)
// Avoid warning "changing property ... in transform-only layer, will have no effect"
- (void)setOpaque:(BOOL)opaque{return;}
- (void)setMasksToBounds:(BOOL)masksToBounds{return;}
- (void)setBackgroundColor:(CGColorRef)backgroundColor{return;}
@end


@implementation MTTransitionAnimator

#pragma mark - API

- (instancetype)initWithDelegate:(id <MTTransitionAnimatorDelegate>)delegate {
    self = [super init];
    if (self) {
        MTAssertParam(delegate);
        self.delegate = delegate;
    }
    return self;
}

- (instancetype)initWithDelegate:(id<MTTransitionAnimatorDelegate>)delegate navigationController:(UINavigationController *)navigationController {
    self = [super init];
    if (self) {
        MTAssertParam(delegate);
        MTAssertParam(navigationController);
        self.delegate = delegate;
        self.targetNavigationController = navigationController;
        navigationController.delegate = self;
    }
    return self;
}

- (void)decoupleFromNavigationController {
    self.targetNavigationController.delegate = nil;
}

#pragma mark Presenting/Dismissing

- (void)presentViewController:(UIViewController *)viewController
           fromViewController:(UIViewController *)fromViewController
                   completion:(void (^)(void))completion {
    // set self as transitioning delegate
    viewController.transitioningDelegate = self;
    
    // present view controller
    [fromViewController presentViewController:viewController animated:YES completion:^{
        // remove animator as transitioning delegate
        viewController.transitioningDelegate = nil;
        
        // execute completion block if one passed
        if (completion) completion();
    }];
}

- (void)dismissViewController:(UIViewController *)viewController
                   completion:(void (^)(void))completion {
    // set self as transitioning delegate
    viewController.transitioningDelegate = self;

    // present view controller
    [viewController dismissViewControllerAnimated:YES completion:^{
        // remove animator as transitioning delegate
        viewController.transitioningDelegate = nil;
        
        // execute completion block if one passed
        if (completion) completion();
    }];
}

- (void)pushViewController:(UIViewController *)viewController ontoNavigationController:(UINavigationController *)navigationController {
    // set self as navigation controller delegate
    // to be able to provide an animator for the transition
    navigationController.delegate = self;
    
    // save navigation controller to remove self as delegate after transition
    self.temporaryNavigationController = navigationController;
    
    // push view controller
    [navigationController pushViewController:viewController animated:YES];
}

- (void)popViewControllerFromNavigationController:(UINavigationController *)navigationController {
    // set self as navigation controller delegate
    // to be able to provide an animator for the transition
    navigationController.delegate = self;
    
    // save navigation controller to remove self as delegate after transition
    self.temporaryNavigationController = navigationController;
    
    // pop view controller
    [navigationController popViewControllerAnimated:YES];
}

#pragma mark Interactive Transitions

- (void)updateInteractiveTransition:(CGFloat)percentComplete {
    if (self.interactiveTransition) {
        [self.interactiveTransition updateInteractiveTransition:percentComplete];
    }
}

- (void)finishInteractiveTransition {
    if (self.interactiveTransition) {
        [self.interactiveTransition finishInteractiveTransition];
        
        NSTimeInterval duration = [self.delegate transitionAnimator:self transitionDurationForOperation:self.currentOperation];
        NSTimeInterval leftDuration = duration * (1.0 - self.interactiveTransition.percentComplete);
        [self _completeTransitionWithContext:self.transitionContext completed:YES withDelay:leftDuration];
    }
}

- (void)cancelInteractiveTransition {
    if (self.interactiveTransition) {
        [self.interactiveTransition cancelInteractiveTransition];
        
        NSTimeInterval duration = [self.delegate transitionAnimator:self transitionDurationForOperation:self.currentOperation];
        NSTimeInterval leftDuration = duration * self.interactiveTransition.percentComplete;
        [self _completeTransitionWithContext:self.transitionContext completed:NO withDelay:leftDuration];
    }
}

#pragma mark - UINavigationControllerDelegate (Needed for NavigationController transitions)

/**
 *  Listens for any push/pop events and returns animator if the delegate returns transition animation
 */
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    // save the operation beeing performed
    switch (operation) {
        case UINavigationControllerOperationPop:
            self.currentOperation = MTTransitionAnimatorOperationPop;
            break;
        
        case UINavigationControllerOperationPush:
            self.currentOperation = MTTransitionAnimatorOperationPush;
            break;
            
        default:
            // don't know how this can happen
            self.currentOperation = MTTransitionAnimatorOperationNone;
            break;
    }
    
    // save from and to vc
    // need them to ask the delegate if the transition should be interactive
    self.fromVC = fromVC;
    self.toVC = toVC;
    
    // return animator object
    return self;
}

- (id <UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                          interactionControllerForAnimationController:(id <UIViewControllerAnimatedTransitioning>)animationController {
    // check if transition should be interactive
    if ([self.delegate respondsToSelector:@selector(transitionAnimator:shouldStartInteractiveTransitionFromViewController:toViewController:withOperation:)] &&
        [self.delegate transitionAnimator:self shouldStartInteractiveTransitionFromViewController:self.fromVC toViewController:self.toVC withOperation:self.currentOperation]) {
        self.interactiveTransition = [UIPercentDrivenInteractiveTransition new];
        return self.interactiveTransition;
    }
    
    return nil;
}

#pragma mark - UIViewControllerTransitioningDelegate (Needed for modal transitions)

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    self.currentOperation = MTTransitionAnimatorOperationPresent;
    
    // save from and to vc
    // need them to ask the delegate if the transition should be interactive
    self.fromVC = presenting;
    self.toVC = presented;
    
    return self;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.currentOperation = MTTransitionAnimatorOperationDismiss;
    
    // save from vc
    // need it to ask the delegate if the transition should be interactive
    self.fromVC = dismissed;
    
    return self;
}

- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id <UIViewControllerAnimatedTransitioning>)animator {
    // check if transition should be interactive
    if ([self.delegate respondsToSelector:@selector(transitionAnimator:shouldStartInteractiveTransitionFromViewController:toViewController:withOperation:)] &&
        [self.delegate transitionAnimator:self shouldStartInteractiveTransitionFromViewController:self.fromVC toViewController:self.toVC withOperation:self.currentOperation]) {
        self.interactiveTransition = [UIPercentDrivenInteractiveTransition new];
        return self.interactiveTransition;
    }
    return nil;
}

- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id <UIViewControllerAnimatedTransitioning>)animator {
    // check if transition should be interactive
    if ([self.delegate respondsToSelector:@selector(transitionAnimator:shouldStartInteractiveTransitionFromViewController:toViewController:withOperation:)] &&
        [self.delegate transitionAnimator:self shouldStartInteractiveTransitionFromViewController:self.fromVC toViewController:self.toVC withOperation:self.currentOperation]) {
        self.interactiveTransition = [UIPercentDrivenInteractiveTransition new];
        return self.interactiveTransition;
    }
    return nil;
}

#pragma mark - UIViewControllerAnimatedTransitioning

// This is used for percent driven interactive transitions, as well as for container controllers that have companion animations that might need to
// synchronize with the main animation.
- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    if ([self.delegate respondsToSelector:@selector(transitionAnimator:transitionDurationForOperation:)]) {
        return [self.delegate transitionAnimator:self transitionDurationForOperation:self.currentOperation];
    }else{
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"You must implement the transitionAnimator:transitionDurationForOperation: delegate method" userInfo:nil];
    }
}

// This method can only be a nop if the transition is interactive and not a percentDriven interactive transition.
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIViewController<MTTransitionAnimatorViewController> *toViewController = (UIViewController<MTTransitionAnimatorViewController> *) [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController<MTTransitionAnimatorViewController> *fromViewController = (UIViewController<MTTransitionAnimatorViewController> *) [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    // save transition context to be able to complete transition
    // once a interactive transition is finished
    self.transitionContext = transitionContext;
    
    if ([self.delegate respondsToSelector:@selector(transitionAnimator:animationBlockForTransitionFromViewController:toViewController:animationContainer:transitionContext:withOperation:)]) {
        // get animation block from delegate
        void (^animation)() = [self.delegate transitionAnimator:self animationBlockForTransitionFromViewController:fromViewController toViewController:toViewController animationContainer:[transitionContext containerView] transitionContext:transitionContext withOperation:self.currentOperation];
        
        // execute animation bloc
        if (animation) {
            animation();
        }else{
            @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"The animationBlock returned from the transitionAnimator:animationBlockForTransitionFromViewController:toViewController:animationContainer:transitionContext:withOperation: delegate method must not be nil" userInfo:nil];
        }
        
        // check if transition is interactive
        // and complete transition if it is not
        if ([self.delegate respondsToSelector:@selector(transitionAnimator:shouldStartInteractiveTransitionFromViewController:toViewController:withOperation:)]) {
            if (![self.delegate transitionAnimator:self shouldStartInteractiveTransitionFromViewController:self.fromVC toViewController:self.toVC withOperation:self.currentOperation]) {
                [self _completeTransitionWithContext:transitionContext completed:![transitionContext transitionWasCancelled] withDelay:0];
            }
        }else{
            [self _completeTransitionWithContext:transitionContext completed:![transitionContext transitionWasCancelled] withDelay:0];
        }
    }else{
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"You must implement the transitionAnimator:animationBlockForTransitionFromViewController:toViewController:animationContainer:withOperation: delegate method" userInfo:nil];
    }
}

// This is a convenience and if implemented will be invoked by the system when the transition context's completeTransition: method is invoked.
- (void)animationEnded:(BOOL) transitionCompleted {
    // clean up
    self.fromVC = nil;
    self.toVC = nil;
    self.interactiveTransition = nil;
    self.transitionContext = nil;
    
    // at this point the transition is complete
    // remove self as delegate
    if (self.temporaryNavigationController) {
        self.temporaryNavigationController.delegate = nil;
        self.temporaryNavigationController = nil;
    }
}

#pragma mark - Private

/**
 *  Complete the transition after the transition duration specified by the delegate
 *
 *  @param transitionContext - the transition context to complete
 *  @param completed         - YES if transition ended properly, NO if was cancelled
 *  @param delay             - time interval to wait until completing (if 0 passed the whole transition duration will be used as delay)
 */
- (void)_completeTransitionWithContext:(id <UIViewControllerContextTransitioning>)transitionContext completed:(BOOL)completed withDelay:(NSTimeInterval)delay {
    // no need to check if the delegate implemented this method since this is check already in the transitionDuration: method
    // and that method is called before this one
    NSTimeInterval transitionDuration = [self.delegate transitionAnimator:self transitionDurationForOperation:self.currentOperation];
    
    // complete transition
    float_t delayInSeconds = delay == 0 ? transitionDuration : delay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [transitionContext completeTransition:completed];
    });
}

@end
