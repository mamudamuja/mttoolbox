//
//  UICollectionView+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 02/09/16.
//
//

#import <UIKit/UIKit.h>

@interface UICollectionView (MTToolBox)

/**
 *  Registers Nib with name for table view
 *
 *  @param cellName - Class/Nib name
 */
- (void)registerCellWithName:(NSString*)cellName;

@end
