//
//  UIView+MTIBDesignable.h
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UIView (MTIBDesignable)

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic, strong) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;

@end
