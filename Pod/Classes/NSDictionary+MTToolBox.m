//
//  NSDictionary+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 23/09/2016.
//
//

#import "NSDictionary+MTToolBox.h"

@implementation NSDictionary (MTToolBox)

#pragma mark - JSON

- (NSString *)mtJsonStringWithPrettyPrint:(BOOL)prettyPrint {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions) (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (error) {
        NSLog(@"ERROR while converting dictionary to json:\n%@", error);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

@end
