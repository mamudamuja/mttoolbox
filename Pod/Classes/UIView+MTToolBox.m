//
//  UIView+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 26/09/15.
//
//

#import "UIView+MTToolBox.h"

@implementation UIView (MTToolBox)

#pragma mark - Popup

/**
 *  Presents the view as a popup in the key window.
 *
 *  @param setUpBlock - once called the view is added to the key window without changing any of it's properties.
 *                      this block is called after the view is added to the window. Use it to display the view
 *                      in an animated fashion or to setup autolayout constraint.
 */
- (void)popupInWindowWithSetUpBlock:(void (^_Nullable)(UIView *view, UIWindow *window))setUpBlock {
    // add to window
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self];
    
    // run setup block
    if (setUpBlock) {
        setUpBlock(self, keyWindow);
    }
}

/**
 *  Removes the view from its superview (if called after popupInWindowWithSetUpBlock: this will be the keyWindow)
 *
 *  @param cleanUpBlock - use this block to remove the view animated or do some clean up.
 *                        NOTE: if a cleanUpBlock is provided it also needs to take care
 *                        of removing the view from it's parent view
 */
- (void)dismissFromPopupWithCleanUpBlock:(void (^_Nullable)(UIView *view, UIWindow *window))cleanUpBlock {
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    if (cleanUpBlock) {
        cleanUpBlock(self, keyWindow);
    }else{
        [self removeFromSuperview];
    }
}

#pragma mark - Utils

/**
 *  Convenience method to load a view form a equally named xib file
 *
 *  @param owner - owner of the view
 */
+ (instancetype)loadViewFromXibWithOwner:(id)owner {
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:owner options:nil] firstObject];
}

/**
 *  Creates a snapshot of the view and returns it as a UIImage instance
 */
- (UIImage *)snapshot {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return snapshot;
}

/**
 *  Removes all subviews from view
 */
- (void)removeAllSubviews {
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
}

#pragma mark - Layout
#pragma mark Setting Center

/**
 *  Centers view horizontally in super view
 */
-(void)centerHorizontallyInSuperView{
    [self centerHorizontallyInView:self.superview];
}


/**
 *  Centers view vertically in super view
 */
-(void)centerVerticallyInSuperView{
    [self centerVerticallyInView:self.superview];
}


/**
 *  Centers view horizontally in passed view
 *
 *  @param view
 */
-(void)centerHorizontallyInView:(UIView*)view{
    self.center = CGPointMake(view.frame.size.width/2, self.center.y);
}


/**
 *  Centers view vertically in passed view
 *
 *  @param view
 */
-(void)centerVerticallyInView:(UIView*)view{
    self.center = CGPointMake(self.center.x, view.frame.size.height/2);
}


/**
 *  Centers view horizontally and vertically in super view
 */
-(void)centerInSuperView{
    self.center = CGPointMake(self.superview.frame.size.width/2, self.superview.frame.size.height/2);
}


/**
 *  Centers view horizontally in superview and vertically under passed view (with padding)
 *
 *  @param view
 *  @param padding
 */
-(void)centerUnderView:(UIView*)view withPadding:(float)padding{
    if (view) {
        self.center = CGPointMake(self.superview.frame.size.width/2, view.frame.origin.y + view.frame.size.height + self.frame.size.height/2 + padding);
    }else{
        self.center = CGPointMake(self.superview.frame.size.width/2, self.frame.size.height/2 + padding);
    }
    
}


/**
 *  Centers view horizontally in super view and vertically at bottom of super view
 */
-(void)centerAtBottomOfSuperView{
    self.center = CGPointMake(self.superview.frame.size.width/2, self.superview.frame.size.height - self.frame.size.height/2);
}

#pragma mark Setting Frame

/**
 *  Sets views frame width
 *
 *  @param width
 */
-(void)setWidth:(float)width{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, width, self.frame.size.height);
}


/**
 *  Sets views frame height
 *
 *  @param height
 */
-(void)setHeight:(float)height{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height);
}


/**
 *  Sets views frame to fill the super view
 */
-(void)fillSuperView{
    self.frame = CGRectMake(0, 0, self.superview.frame.size.width, self.superview.frame.size.height);
}

#pragma mark Calculating Frame

/**
 *  Returns views frame width
 *
 *  @return
 */
-(float)width{
    return self.frame.size.width;
}


/**
 *  Returns views frame height
 *
 *  @return
 */
-(float)height{
    return self.frame.size.height;
}

#pragma mark Calculating Edges

/**
 *  Returns the x value of the right frame edge
 *
 *  @return
 */
-(float)rightEdgeValue{
    return self.frame.origin.x + self.frame.size.width;
}


/**
 *  Returns the x value of the left frame edge
 *
 *  @return
 */
-(float)leftEdgeValue{
    return self.frame.origin.x;
}


/**
 *  Returns the y value of the top frame edge
 *
 *  @return
 */
-(float)topEdgeValue{
    return self.frame.origin.y;
}


/**
 *  Returns the y value of the bottom frame edge
 *
 *  @return
 */
-(float)bottomEdgeValue{
    return self.frame.origin.y + self.frame.size.height;
}

@end
