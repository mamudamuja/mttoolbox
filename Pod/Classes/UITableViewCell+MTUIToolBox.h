//
//  UITableViewCell+MTUIToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (MTUIToolBox)

/**
 *  Returns a cell reuse identifier for the class
 *
 *  @return
 */
+ (NSString*)reuseIdentifier;

@end
