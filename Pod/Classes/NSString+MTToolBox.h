//
//  NSString+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 25/01/16.
//
//

#import <Foundation/Foundation.h>

@interface NSString (MTToolBox)

#pragma mark - Conversion

/**
 *  Returns string from passed date formatted with the passed format
 *
 *  @param date   - date to convert to string
 *  @param format - date format to use for conversion
 */
+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format;

#pragma mark - Regex

/**
 *  Checks if the stinrg validates with the passed regexPattern
 *
 *  @param regexPattern - regexPatter to validate against
 *
 *  @return
 */
- (BOOL)validatesWithRegex:(NSString *)regexPattern;

#pragma mark - Input Validation

/**
 *  Checks if the string is a valid international phone number
 *
 *  @param strictFilter - specifies if the filter also checks for a country code
 *
 *  @return
 */
- (BOOL)isValidPhoneNumberWithStrictFilter:(BOOL)strictFilter;


/**
 *  Checks if the string is a valid email address
 *
 *  @param strictFilter - specifies if the filter should validate strictly
 *
 *  @return
 */
- (BOOL)isValidEmailAddressWithStrictFilter:(BOOL)strictFilter;

#pragma mark - Fractions

/**
 *  Returns passed number in superscript
 */
+ (NSString *)superscript:(NSInteger)number;


/**
 *  Returns passed number in subscript
 */
+ (NSString *)subscript:(NSInteger)numbe;


/**
 *  Returs a fraction string with sub and superscripted numbers
 *
 *  @param numerator   - numerator
 *  @param denominator - denominator
 *
 *  @return
 */
+ (NSString *)fractionWithNumerator:(NSInteger)numerator denominator:(NSInteger)denominator;

@end
