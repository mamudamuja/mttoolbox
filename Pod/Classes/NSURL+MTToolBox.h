//
//  NSURL+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 15/03/16.
//
//

#import <Foundation/Foundation.h>

@interface NSURL (MTToolBox)

#pragma mark - File URLs

/**
 *  Returns fileURL for Resource with passed name
 *
 *  @param fileName - name should be of format xxxx.yyy (e.g. test.pdf)
 *
 *  @return
 */
+ (NSURL*)mainBundleFileURLWithFileName:(NSString*)fileName;


/**
 *  Returns fileURL for subpath in documents folder
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return
 */
+ (NSURL*)documentsFolderFileURLWithPath:(NSString*)path;

@end
