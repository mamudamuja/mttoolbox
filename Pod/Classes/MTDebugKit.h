//
//  MTDebugKit.h
//  Pods
//
//  Created by Milutin Tomic on 24/02/2017.
//
//

#import <Foundation/Foundation.h>

@interface MTDebugKit : NSObject

/**
 *  Returns the switch with the passed identifier
 *
 *  @param identifier - switch identifier
 *
 *  @return - switch or nil
 */
+ (nullable UISwitch *)switchWithID:(nonnull NSString *)identifier;

/**
 *  Sets up a switch for debugging purposes and adds it to the key window.
 *
 *  @param identifier   - switch identifier
 *  @param center       - center coordinates of the switch
 *  @param eventHandler - the handler if passed will be executed every time the switch value is changed
 */
+ (void)debugSwithWithID:(nonnull NSString *)identifier center:(CGPoint)center eventHandler:(void (^_Nullable)(UISwitch * _Nonnull))eventHandler;

/**
 *  Sets up a button for debugging purposes and adds it to the key window.
 *
 *  @param title        - button title
 *  @param center       - center coordinates of the button
 *  @param eventHandler - the handler if passed will be executed every time the the button is pressed
 */
+ (void)debugButtonWithTitle:(nonnull NSString *)title center:(CGPoint)center eventHandler:(void (^_Nullable)())eventHandler;

@end
