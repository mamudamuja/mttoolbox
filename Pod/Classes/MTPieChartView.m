//
//  MTPieChartView.m
//  Pods
//
//  Created by Milutin Tomic on 27/09/2016.
//
//

#import "MTPieChartView.h"
#import "MTToolBox.h"

#pragma mark - PIE CHART SEGMENT

@interface MTPieChartSegment : NSObject

@property (nonatomic, strong) UIColor *color;
@property (nonatomic) CGFloat value;
@property (nonatomic, strong) NSAttributedString *title;
@property (nonatomic, strong) NSAttributedString *segmentDescription;

@end

@implementation MTPieChartSegment

@end

#pragma mark - PIE CHART VIEW

@interface MTPieChartView ()

@property (nonatomic, strong) NSMutableArray<MTPieChartSegment *> *segments;
@property (nonatomic, strong) NSMutableArray<MTChartLegendView*> *legendViews;

@end


@implementation MTPieChartView

#pragma mark - API

- (void)reloadData {
    // get data
    [self _fetchSegmentData];
    
    // update ui
    [self setNeedsDisplay];
    
    // update registered legend views
    for (MTChartLegendView *legendView in self.legendViews) {
        [legendView reloadData];
    }
}

#pragma mark - MTChartLegendViewSupport

- (void)registerLegendView:(MTChartLegendView *)legendView {
    [self.legendViews addObject:legendView];
}

- (NSArray<MTChartLegendItem *> *)legendItems {
    return [self.segments map:^id(MTPieChartSegment *object, NSInteger index) {
        MTChartLegendItem *item = [MTChartLegendItem new];
        item.color = [object.color copy];
        item.title = [object.title copy];
        return item;
    }];
}

#pragma mark - Private

/**
 *  Fetches segment data from delegate
 */
- (void)_fetchSegmentData {
    if (!self.delegate) {
        [self.segments removeAllObjects];
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(piechartView:valueForSegmentAtIndex:)] &&
        [self.delegate respondsToSelector:@selector(piechartView:colorForSegmentAtIndex:)] &&
        [self.delegate respondsToSelector:@selector(numberOfSegmentsInPiechartView:)]) {
        
        for (NSInteger segmentIndex = 0; segmentIndex < [self.delegate numberOfSegmentsInPiechartView:self]; segmentIndex++) {
            MTPieChartSegment *segment = [MTPieChartSegment new];
            segment.color = [self.delegate piechartView:self colorForSegmentAtIndex:segmentIndex];
            segment.value = [self.delegate piechartView:self valueForSegmentAtIndex:segmentIndex];
            
            if ([self.delegate respondsToSelector:@selector(piechartView:titleForSegmentAtIndex:)]) {
                segment.title = [self.delegate piechartView:self titleForSegmentAtIndex:segmentIndex];
            }
            
            if ([self.delegate respondsToSelector:@selector(piechartView:descriptionForSegmentAtIndex:)]) {
                segment.title = [self.delegate piechartView:self descriptionForSegmentAtIndex:segmentIndex];
            }
            
            [self.segments addObject:segment];
        }
        
    }else{
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"The required MTPieChartViewDelegate protocol methods must be implemented" userInfo:nil];
    }
}

/**
 *  Returns a percentage value for the passed value
 */
- (CGFloat)_valueInPercent:(CGFloat)value {
    CGFloat totalValue = 0;
    for (MTPieChartSegment *s in self.segments) {
        totalValue += s.value;
    }
    
    CGFloat resultValue = value/totalValue;
    
    return isnan(resultValue) ? 0 : resultValue;
}

/**
 *  Calculates start angle for segment at index
 */
- (CGFloat)_startAngleForSegmentAtIndex:(NSInteger)index {
    MTPieChartSegment *segment = self.segments[index];
    
    CGFloat precedingSegmentsValue = 0;
    for (MTPieChartSegment *s in self.segments) {
        if (s == segment) {
            break;
        }
        
        precedingSegmentsValue += s.value;
    }
    
    CGFloat precedingPercentage = [self _valueInPercent:precedingSegmentsValue];
    
    return 360 * precedingPercentage;
}

/**
 *  Calculates end angle for segment at index
 */
- (CGFloat)_endAngleForSegmentAtIndex:(NSInteger)index {
    MTPieChartSegment *segment = self.segments[index];
    
    CGFloat precedingSegmentsValue = 0;
    for (MTPieChartSegment *s in self.segments) {
        if (s == segment) {
            break;
        }
        
        precedingSegmentsValue += s.value;
    }
    
    CGFloat percentageIncludingCurrentSegment = [self _valueInPercent:precedingSegmentsValue + segment.value];
    
    return 360 * percentageIncludingCurrentSegment;
}

/**
 *  Creates labels for the passed segment titles and lays them out
 */
- (void)_drawSegmentTitles {
    [self removeAllSubviews];
    
    NSInteger segmentIndex = 0;
    for (MTPieChartSegment *segment in self.segments) {
        if (segment.title) {
            // create label
            UILabel *titleLabel = [UILabel new];
            titleLabel.attributedText = segment.title;
            titleLabel.numberOfLines = 0;
            
            if ([self.delegate respondsToSelector:@selector(piechartView:colorForTitleAtIndex:)]) {
                titleLabel.textColor = [self.delegate piechartView:self colorForTitleAtIndex:segmentIndex];
            } else {
                titleLabel.textColor = [UIColor whiteColor];
            }
            
            [titleLabel sizeToFit];
            [self addSubview:titleLabel];
            
            // position label
            CGFloat radius = MIN(self.width, self.height) / 2.0;
            CGFloat direction = ([self _startAngleForSegmentAtIndex:segmentIndex] + [self _endAngleForSegmentAtIndex:segmentIndex]) / 2.0;
            CGFloat distance = (radius + self.innerRadius) / 2.0;
            
            CGPoint labelCenter = CGPointMake(self.width/2.0 + distance * cos(DEGREES_TO_RADIANS(direction)),
                                              self.height/2.0 + distance * sin(DEGREES_TO_RADIANS(direction)));
            titleLabel.center = labelCenter;
            
            segmentIndex++;
        }
    }
}

#pragma mark - CA

- (void)setDelegate:(id<MTPieChartViewDelegate>)delegate {
    _delegate = delegate;
    
    [self reloadData];
}

- (NSMutableArray<MTPieChartSegment *> *)segments {
    if (!_segments) {
        _segments = [NSMutableArray new];
    }
    return _segments;
}

- (NSMutableArray<MTChartLegendView *> *)legendViews {
    if (!_legendViews) {
        _legendViews = [NSMutableArray new];
    }
    return _legendViews;
}

#pragma mark - Overrides

- (void)drawRect:(CGRect)rect {
    self.backgroundColor = [UIColor clearColor];
        
    // draw segments
    NSInteger segmentIndex = 0;
    for (MTPieChartSegment *segment in self.segments) {
        CGRect segmentRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
        UIBezierPath *segmentPath = [UIBezierPath bezierPath];
        [segmentPath addArcWithCenter:CGPointMake(segmentRect.size.width/2.0, segmentRect.size.height/2.0)
                               radius:MIN(segmentRect.size.width/2.0, segmentRect.size.height/2.0)
                           startAngle:DEGREES_TO_RADIANS([self _startAngleForSegmentAtIndex:segmentIndex])
                             endAngle:DEGREES_TO_RADIANS([self _endAngleForSegmentAtIndex:segmentIndex])
                            clockwise:YES];
        [segmentPath addLineToPoint:CGPointMake(rect.size.width/2.0, rect.size.height/2.0)];
        [segmentPath closePath];
        
        [segment.color setFill];
        [segmentPath fill];
        
        segmentIndex++;
    }
    
    // cut out inner radius
    if (self.innerRadius > 0) {
        // begin context
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSetBlendMode(ctx, kCGBlendModeClear);
        CGRect innerCircleRect = CGRectMake(rect.size.width/2.0 - self.innerRadius,
                                            rect.size.height/2.0 - self.innerRadius,
                                            self.innerRadius * 2.0,
                                            self.innerRadius * 2.0);
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:innerCircleRect];
        [[UIColor clearColor] set];
        [path fill];
    }
    
    // draw segment titles
    [self _drawSegmentTitles];
}

@end
