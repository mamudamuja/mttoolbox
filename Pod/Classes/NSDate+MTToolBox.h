//
//  NSDate+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 25/01/16.
//
//

#import <Foundation/Foundation.h>

@interface NSDate (MTToolBox)

#pragma mark - String Representation

/**
 *  Returns the string representation of the date using the passed date format
 *
 *  @param dateFormat - date format to use
 *
 *  @return - string representation
 */
- (NSString *)stringUsingDateFormat:(NSString *)dateFormat;

#pragma mark - Comparing Dates

/**
 *  Checks if the date is the todays day
 */
- (BOOL)isToday;

/**
 *  Returns the difference to the passed date in the specified unit
 *
 *  @param anotherDate - date to compare to
 *  @param unit        - unit
 */
- (NSDateComponents *)differenceToDate:(NSDate *)anotherDate unit:(NSCalendarUnit)unit;

#pragma mark - Date Components

/**
 *  Returns dates hour component
 *
 *  @return
 */
- (NSInteger)hour;


/**
 *  Returns dates hour component
 *
 *  @return
 */
- (NSInteger)minute;


/**
 *  Returns dates hour component
 *
 *  @return
 */
- (NSInteger)second;

    
/**
 *  Returns dates year component
 *
 *  @return
 */
- (NSInteger)year;


/**
 *  Returns dates month component
 *
 *  @return
 */
- (NSInteger)month;


/**
 *  Returns dates day component
 *
 *  @return
 */
- (NSInteger)day;

#pragma mark - Creating Dates

/**
 *  Creates new date from components
 *
 *  @param day
 *  @param month
 *  @param year
 *  @param hour
 *  @param minut
 *  @param second
 *
 *  @return
 */
+ (NSDate*)dateWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second;

/**
 *  Creates new date from components
 *
 *  @param day
 *  @param month
 *  @param year
 *
 *  @return
 */
+ (NSDate*)dateWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;

@end
