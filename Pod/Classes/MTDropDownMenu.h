//
//  MTDropDownMenu.h
//  Pods
//
//  Created by Milutin Tomic on 12/09/16.
//
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    MTDropDownMenuDismissModeAuto, // dismiss when an item gets selected or the background gets touched
    MTDropDownMenuDismissModeOnSelect, // dismiss when an item gets selected
    MTDropDownMenuDismissModeManual // does not dismiss automatically
} MTDropDownMenuDismissMode;

typedef enum : NSUInteger {
    MTDropDownMenuDropDirectionDown,
    MTDropDownMenuDropDirectionUp,
    MTDropDownMenuDropDirectionAuto // drops down if enough space, if not drops up
                                    // if not enough space on both sides drops down
} MTDropDownMenuDropDirection;


@class MTDropDownMenu;
@protocol MTDropDownMenuDelegate <NSObject>

@required

/**
 *  Called when the drop down menu gets presented
 *
 *  @param dropDownMenu - drop down menu instance
 *
 *  @return
 */
- (NSInteger)numberOfItemsInDropDownMenu:(nonnull MTDropDownMenu *)dropDownMenu;

/**
 *  Provides the drop down menu with a title for the item at index.
 *  To customize the appearance of the menu items, implement the dropDownMenu:viewForItemAtIndex: method.
 *
 *  @param dropDownMenu - drop down menu instance
 *  @param index        - item index
 *
 *  @return
 */
- (nonnull NSString *)dropDownMenu:(nonnull MTDropDownMenu *)dropDownMenu titleForItemAtIndex:(NSInteger)index;

/**
 *  Privides the menu with the height for the item at the passed index
 *
 *  @param dropDownMenu - drop down menu instance
 *  @param index        - item index
 *
 *  @return
 */
- (CGFloat)dropDownMenu:(nonnull MTDropDownMenu *)dropDownMenu heightForItemAtIndex:(NSInteger)index;

@optional

/**
 *  Provides the drop down menu with a view that should be presented for an item.
 *  If implemented the dropDownMenu:titleForItemAtIndex: method will be ignored.
 *
 *  @param dropDownMenu - drop down menu instance
 *  @param index        - item index
 *
 *  @return
 */
- (nonnull UIView *)dropDownMenu:(nonnull MTDropDownMenu *)dropDownMenu viewForItemAtIndex:(NSInteger)index;

/**
 *  Called when an item gets selected.
 *
 *  @param dropDownMenu - drop down menu instance
 *  @param index        - index of the selected item
 */
- (void)dropDownMenu:(nonnull MTDropDownMenu *)dropDownMenu didSelectItemAtIndex:(NSInteger)index;

/**
 *  Provides the drop down menu with a custom height.
 *  If this method is not implemented the drop down menu will use the size
 *  that is the best fitting for the set properties.
 *  If you preffer to set just one of the params you can do so.
 *  If you provide just a width the height will be calculated automatically (and vice versa).
 *
 *  @param dropDownMenu - drop down menu instance
 *
 *  @return
 */
- (CGSize)preferredSizeForDropDownMenu:(nonnull MTDropDownMenu *)dropDownMenu;

/**
 *  Provides the drop down menu with a custom offset.
 *  If this method is not implemented the drop down menu will be positioned in the center of the screen
 *  if no anchor view is set. Otherwise the menu will stick to the anchorView
 *  If you wish to change that offset implement this method.
 *
 *  @param dropDownMenu - drop down menu instance
 *
 *  @return
 */
- (CGPoint)preferredOffsetForDropDownMenu:(nonnull MTDropDownMenu *)dropDownMenu;

@end


@interface MTDropDownMenu : UIView

/**
 *  Initializes drop down menu with a delegate and a anchor view.
 *
 *  @param delegate   - drop down menu delegate
 *  @param anchorView - the view the drop down menu should stick to (is centered in screen if no anchorView provided)
 *
 *  @return
 */
- (nonnull instancetype)initWithDelegate:(nonnull id<MTDropDownMenuDelegate>)delegate anchorView:(nullable UIView *)anchorView;

/**
 *  Shows drop down menu instance.
 *  If anchorView is nil the drop down menu will be centered on the screen.
 *
 *  @param animated   - determines wheter the view should show animated
 *  @param targetView - the view that will contain the target view (defaults to the keyWindow)
 */
- (void)showAnimated:(BOOL)animated inView:(nullable UIView *)targetView;

/**
 *  Dismisses drop down menu
 *
 *  @param animated - determines wheter the view should dismiss animated
 */
- (void)dismiss:(BOOL)animated;


/**
 *  The view that the drop down menu should stick to
 */
@property (nonatomic, strong, nullable) UIView *anchorView;

/**
 *  The color of the drop down menu dimmer
 *  Defaults to 30% black.
 */
@property (nonatomic, strong, nullable) UIColor *dimmerColor;

/**
 *  Indicates how the drop down menu should be dismissed.
 *  Defaults to MTDropDownMenuDismissModeAuto.
 */
@property (nonatomic) MTDropDownMenuDismissMode dismissMode;

/**
 *  Indicates where the drop down menu should be displayed relative to the anchorView.
 *  If anchorView is nil this property has no effect.
 *  Defaults to MTDropDownMenuDropDirectionAuto.
 */
@property (nonatomic) MTDropDownMenuDropDirection dropDirection;

@end
