//
//  NSArray+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 17/01/16.
//
//

#import <Foundation/Foundation.h>

@interface NSArray (MTToolBox)

#pragma mark - JSON

/**
 *  Returns a JSON String representation of the NSArray
 *
 *  @param prettyPrint - indicates wheter to use NSJSONWritingPrettyPrinted writing option
 */
- (NSString *)mtJsonStringWithPrettyPrint:(BOOL)prettyPrint;

#pragma mark - Enumeration

/**
 *  Returns an array where the mapFunction is applied
 *  to every object of the original array.
 *
 *  @param mapFunction - function to apply to array objects
 *
 *  @return
 */
- (NSArray*)map:(id(^)(id object, NSInteger index))mapFunction;

/**
 *  Returns an array where the mapFunction is applied
 *  to every object of the original array
 *
 *  @param mapFunction - function to apply to array objects
 *  @param ignore      - if this is set to YES, nil pointers can be returned from
 *                       the mapping function and the object will be ignored (not added to the result array)
 */
- (NSArray *)map:(id (^)(id, NSInteger))mapFunction ingnoreNil:(BOOL)ignore;

#pragma mark - Sorting

/**
 *  Returns copy of array sorted by key
 *
 *  @param key       - key to sort with
 *  @param ascending - ascending/descending
 *
 *  @return
 */
- (NSArray*)sortedByKey:(NSString*)key ascending:(BOOL)ascending;

#pragma mark - Description

/**
 *  Returns a list of all object as a string
 *
 *  @return
 */
- (NSString*)list;

@end
