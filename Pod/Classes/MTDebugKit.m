//
//  MTDebugKit.m
//  Pods
//
//  Created by Milutin Tomic on 24/02/2017.
//
//

#import "MTDebugKit.h"
#import "MTToolBox.h"

@interface MTDebugKit ()

@property (nonatomic, strong) NSMutableDictionary *debugSwitches;
@property (nonatomic, strong) NSMutableDictionary *debugButtons;

@end


@implementation MTDebugKit

SINGLETON(sharedKit)

- (instancetype)init {
    self = [super init];
    if (self) {
        self.debugSwitches = [NSMutableDictionary new];
        self.debugButtons = [NSMutableDictionary new];
    }
    return self;
}

#pragma mark - API

+ (UISwitch *)switchWithID:(NSString *)identifier {
#if DEBUG
    MTAssertParam(identifier);
    NSDictionary *dictionary = [[[self sharedKit] debugSwitches] objectForKey:identifier];
    return [dictionary objectForKey:@"switch"];
#else
    return nil;
#endif
}

+ (void)debugSwithWithID:(NSString *)identifier center:(CGPoint)center eventHandler:(void (^)(UISwitch *))eventHandler {
#if DEBUG
    MTAssertParam(identifier);
    
    // if switch with identifier exists ignore call
    if ([[[self sharedKit] debugSwitches] objectForKey:identifier]) {
        return;
    }
    
    // setup switch
    UISwitch *dSwitch = [UISwitch new];
    dSwitch.center = center;
    [dSwitch addTarget:[self sharedKit] action:@selector(handleSwitchEvent:) forControlEvents:UIControlEventValueChanged];
    
    // store handler block
    if (eventHandler) {
        [[[self sharedKit] debugSwitches] setObject:@{@"switch" : dSwitch, @"handler" : eventHandler} forKey:identifier];
    }else{
        [[[self sharedKit] debugSwitches] setObject:@{@"switch" : dSwitch} forKey:identifier];
    }
    
    // add to window
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:dSwitch];
    
    // add info label
    UILabel *infoLabel = [self infoLabelWithText:identifier];
    infoLabel.center = CGPointMake(dSwitch.center.x, dSwitch.bottomEdgeValue + infoLabel.height/2.0);
    [keyWindow addSubview:infoLabel];
#endif
}

+ (void)debugButtonWithTitle:(NSString *)title center:(CGPoint)center eventHandler:(void (^)())eventHandler {
#if DEBUG
    MTAssertParam(title);
    
    // if button with title exists ignore call
    if ([[[self sharedKit] debugButtons] objectForKey:title]) {
        return;
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:[self sharedKit] action:@selector(handleButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    button.center = center;
    
    if (eventHandler) {
        [[[self sharedKit] debugButtons] setObject:eventHandler forKey:title];
    }
    
    // add to window
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:button];
#endif
}

#pragma mark - Private

+ (UILabel *)infoLabelWithText:(NSString *)text {
    UILabel *label = [UILabel new];
    label.textColor = [UIColor redColor];
    label.text = text;
    label.font = [UIFont systemFontOfSize:8];
    [label sizeToFit];
    label.frame = CGRectMake(0, 0, MIN(label.width, 100), label.height);
    
    return label;
}

- (void)handleButtonEvent:(UIButton *)button {
    void (^eventHandler)() = [self.debugButtons objectForKey:button.titleLabel.text];
    
    if (eventHandler) eventHandler();
}

- (void)handleSwitchEvent:(id)sender {
    // find handler for switch and execute
    for (NSDictionary *dictionary in [self.debugSwitches allValues]) {
        if ([dictionary objectForKey:@"switch"] == sender && [dictionary objectForKey:@"handler"]) {
            void (^eventHandler)(UISwitch *) = [dictionary objectForKey:@"handler"];
            if (eventHandler) {
                eventHandler((UISwitch *)sender);
            }
            
            return;
        }
    }
}

@end
