//
//  MTQuickLookViewController.m
//  Pods
//
//  Created by Milutin Tomic on 15/03/16.
//
//

#import "MTQuickLookViewController.h"
#import "MTToolBox.h"

@interface MTQuickLookViewController () <QLPreviewControllerDataSource>

/* === DATA === */
@property (nonatomic, strong) NSArray <QLPreviewItem> *previewItems;

@end


@implementation MTQuickLookViewController

/**
 *  Initializes Presenter with a single preview item
 *
 *  @param item
 *
 *  @return
 */
- (instancetype)initWithItem:(MTQuickLookViewControllerItem*)item
{
    self = [super init];
    if (self) {
        _previewItems = (NSArray <QLPreviewItem> *)@[item];
        
        [self preparePreviewController];
    }
    return self;
}


/**
 *  Initializes Presenter with a list of preview items
 *
 *  @param previewItems
 *
 *  @return
 */
- (instancetype)initWithPreviewItems:(NSArray <QLPreviewItem> *)previewItems
{
    self = [super init];
    if (self) {
        _previewItems = [previewItems copy];
        
        [self preparePreviewController];
    }
    return self;
}

#pragma mark - Private

/**
 *  Initializes preview VC and the enclosing navigation VC
 */
- (void)preparePreviewController {
    self.dataSource = self;
}

- (void)styleNavigationbar {
    id firstChild = self.childViewControllers.firstObject;
    if ([firstChild isKindOfClass:[UINavigationController class]]) {
        UINavigationController *nav = (UINavigationController *) firstChild;
        [nav.navigationBar setTranslucent:NO];
        [nav.navigationBar setBarTintColor:[UIColor whiteColor]];
        [nav.navigationBar setTintColor:self.barColor];
    }
}

#pragma mark - Overrides

- (void)viewDidLoad {
    [super viewDidLoad];
    [self styleNavigationbar];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self styleNavigationbar];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - CA

- (void)setBarColor:(UIColor *)barColor {
    _barColor = barColor;
    [self styleNavigationbar];
}

#pragma mark - Quick Look Preview Delegate

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller{
    return _previewItems.count;
}


- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index{
    return _previewItems[index];
}

@end



@implementation MTQuickLookViewControllerItem

+ (MTQuickLookViewControllerItem*)quickLookItemWithItemURL:(NSURL*)itemUrl andItemTitle:(NSString*)itemTitle{
    return [[MTQuickLookViewControllerItem alloc]initWithItemURL:itemUrl andItemTitle:itemTitle];
}

- (instancetype)initWithItemURL:(NSURL*)itemUrl andItemTitle:(NSString*)itemTitle
{
    self = [super init];
    if (self) {
        self.previewItemTitle = itemTitle;
        self.previewItemURL = itemUrl;
    }
    return self;
}

@end
