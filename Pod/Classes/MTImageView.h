//
//  MTImageView.h
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MTImageView : UIImageView

// if provided will be set as image and will
// use the render as template rendering mode
@property (nonatomic, strong) IBInspectable UIImage *templateImage;

#pragma mark - Asynchronous Loading

/**
 *  Loads image from disk/main bundle in background.
 *  The placeholder, if provided, will be shown until the image is loaded.
 *
 *  @param imageURL    - url of the image
 *  @param placeHolfer - placeholder to show while image is loading
 */
- (void)loadImageFromDisk:(NSURL *)imageURL placeHolder:(UIImage *)placeHolder;

/**
 *  Loads image from disk/main bundle in background.
 *
 *  @param imageURL    - url of the image
 */
- (void)loadImageFromDisk:(NSURL *)imageURL;

/**
 *  Cancels all image loading operations
 */
- (void)cancelImageLoading;

@end
