//
//  MTToggleButton.h
//  Pods
//
//  Created by Milutin Tomic on 13/01/16.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MTToggleButton : UIControl

// current state of the toggle
// DEFAULT IS NO
@property (nonatomic) IBInspectable BOOL isOn;

// displayed when toggle is on
@property (nonatomic, strong) IBInspectable NSString *titleWhenOn;
@property (nonatomic, strong) IBInspectable UIImage *imageWhenOn;

// displayed when toggle is off
@property (nonatomic, strong) IBInspectable NSString *titleWhenOff;
@property (nonatomic, strong) IBInspectable UIImage *imageWhenOff;

// if the tint color is specified the image will be used as template
// and rendered in the given tint color
// the title label will use the tint color as text color
// DEFAULT IS NIL
@property (nonatomic, strong) IBInspectable UIColor *tintColorWhenOn;
@property (nonatomic, strong) IBInspectable UIColor *tintColorWhenOff;


// displays the toggle title
@property (nonatomic, strong, readonly) UILabel *titleLabel;

// displays the toggle icon
@property (nonatomic, strong, readonly) UIImageView *imageView;

@end
