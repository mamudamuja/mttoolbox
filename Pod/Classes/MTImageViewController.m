//
//  MTImageViewController.m
//  Pods
//
//  Created by Milutin Tomic on 26/02/2017.
//
//

#import "MTImageViewController.h"
#import "MTToolBox.h"

@interface MTImageViewController () <UIScrollViewDelegate>

/* === UI === */
@property (nonatomic, strong) UIScrollView                         *pagingScrollView;
@property (nonatomic, strong) UIButton                             *closeButton;
@property (nonatomic, strong) UILabel                              *pageIndicatorLabel;

/* === DATA === */
@property (nonatomic, strong) NSArray<UIImage *>                   *images;
@property (nonatomic, strong) NSMutableSet<MTImageScrollView *>    *visiblePages;
@property (nonatomic, strong) NSMutableSet<MTImageScrollView *>    *reusablePages;
@property (nonatomic) NSInteger                                    currentPage;

@end


@implementation MTImageViewController

#pragma mark - API

- (instancetype)initWithImages:(NSArray<UIImage *> *)images {
    self = [super init];
    if (self) {
        self.images = images;
        self.hidesStatusBar = YES;
        self.spaceBetweenPages = 10;
        self.showPageIndicator = YES;
        
        self.visiblePages = [NSMutableSet new];
        self.reusablePages = [NSMutableSet new];
        
        [self _setupViews];
    }
    return self;
}

- (void)showPage:(NSInteger)pageIndex {
    if (IS_WITHIN_RANGE(pageIndex, 0, [self _numberOfPages] - 1)) {
        // set current page
        self.currentPage = pageIndex;
        
        // scroll to current page
        [self.pagingScrollView setContentOffset:CGPointMake(self.pagingScrollView.width * self.currentPage, 0)];
        
        // setup tiles
        [self _setupTiles];
        
        // layout tiles
        // this forces the tiles to update layout
        [self _layoutTiles];
    }
}

#pragma mark - CA

- (void)setShowPageIndicator:(BOOL)showPageIndicator {
    _showPageIndicator = showPageIndicator;
    
    self.pageIndicatorLabel.hidden = !showPageIndicator;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.hidesStatusBar) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.hidesStatusBar) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self _layoutViews];
}

#pragma mark - Private

/**
 *  Sets up the internal UI hierarchy
 */
- (void)_setupViews {
    // paging scroll view
    self.pagingScrollView = [UIScrollView new];
    self.pagingScrollView.showsVerticalScrollIndicator = NO;
    self.pagingScrollView.showsHorizontalScrollIndicator = NO;
    self.pagingScrollView.pagingEnabled = YES;
    self.pagingScrollView.delegate = self;
    [self.view addSubview:self.pagingScrollView];
    
    // close button
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton setImage:[self _closeButtonIcon] forState:UIControlStateNormal];
    self.closeButton.frame = CGRectMake(0, 0, 30, 30);
    [self.closeButton addTarget:self action:@selector(_dismissImageViewController) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
    
    // page indicator label
    self.pageIndicatorLabel = [UILabel new];
    self.pageIndicatorLabel.textColor = [UIColor whiteColor];
    self.pageIndicatorLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.pageIndicatorLabel];
}

/**
 *  Lays out the internal UI
 */
- (void)_layoutViews {
    // close button
    CGFloat closeButtonPadding = 10;
    CGFloat statusBarPadding = [[UIApplication sharedApplication] isStatusBarHidden] ? 0 : 20;
    self.closeButton.center = CGPointMake(self.view.width - closeButtonPadding - self.closeButton.width/2.0, statusBarPadding + closeButtonPadding + self.closeButton.width/2.0);
    
    // page indicator label
    self.pageIndicatorLabel.frame = CGRectMake(0, 0, self.view.width, 40);
    self.pageIndicatorLabel.center = CGPointMake(self.view.width/2.0, self.closeButton.center.y);
    
    // paging scroll view
    self.pagingScrollView.frame = CGRectMake(-self.spaceBetweenPages/2.0, 0, self.view.width + self.spaceBetweenPages, self.view.height);
    self.pagingScrollView.contentSize = CGSizeMake(self.pagingScrollView.width * [self _numberOfPages], self.pagingScrollView.height);
    
    // scroll to current page
    [self showPage:self.currentPage];
    [self _updatePageIndicator];
}

/**
 *  This method forces the tiles to update their layout.
 *  It can be used to relayout the tiles after a device rotation for example.
 */
- (void)_layoutTiles {
    for (MTImageScrollView *scrollView in self.visiblePages) {
        // update scroll view
        scrollView.frame = self.view.bounds;
        scrollView.image = [self _imageForPageAtIndex:scrollView.pageIndex];
        scrollView.center = [self _centerForPageAtIndex:scrollView.pageIndex];
    }
}

/**
 *  Sets up the visible pages and cleans up the invisible ones
 */
- (void)_setupTiles {
    // find range of visible pages
    NSInteger minPageIndex = (self.pagingScrollView.contentOffset.x) / self.pagingScrollView.width;
    NSInteger maxPageIndex = (self.pagingScrollView.contentOffset.x + self.pagingScrollView.width - 1) / self.pagingScrollView.width;
    
    // remove pages that are not visible anymore
    for (MTImageScrollView *scrollView in self.visiblePages) {
        // if the page is not in the range of visible pages -> remove it
        if (!IS_WITHIN_RANGE(scrollView.pageIndex, minPageIndex, maxPageIndex)) {
            [self.reusablePages addObject:scrollView];
            [scrollView removeFromSuperview];
        }
    }
    // remove reused pages from the visible pages set
    [self.visiblePages minusSet:self.reusablePages];
    
    // add pages that are within the visible range
    for (NSInteger pageIndex = minPageIndex; pageIndex <= maxPageIndex; pageIndex++) {
        if (IS_WITHIN_RANGE(pageIndex, 0, [self _numberOfPages] - 1) && [self _isPageMissing:pageIndex]) {
            [self _dequeueImageScrollViewForPageAtIndex:pageIndex];
        }
    }
}

/**
 *  Updates the page indicator label
 */
- (void)_updatePageIndicator {
    NSInteger currentPage = (self.pagingScrollView.contentOffset.x + self.pagingScrollView.width/2.0) / self.pagingScrollView.width;
    self.pageIndicatorLabel.text = [NSString stringWithFormat:@"%i/%i", currentPage + 1, [self _numberOfPages]];
}

/**
 *  Checks whether the page with the passed index is already visible
 *
 *  @param index - page index
 */
- (BOOL)_isPageMissing:(NSInteger)index {
    for (MTImageScrollView *scrollView in self.visiblePages) {
        if (scrollView.pageIndex == index) {
            return NO;
        }
    }
    
    return YES;
}


/**
 *  Dequeues a zoomable image page and sets up the position, the image and adds it to the slider.
 *  If a page could not be dequeued a new one is created.
 *
 *  @param index - index of the page
 *
 *  @return - the page dequeued
 */
- (MTImageScrollView *)_dequeueImageScrollViewForPageAtIndex:(NSInteger)index {
    // dequeue image scroll view
    MTImageScrollView *scrollView = [self.reusablePages anyObject];
    
    // create new one if none was found in the queue
    if (!scrollView) {
        scrollView = [[MTImageScrollView alloc] initWithFrame:self.view.bounds];
    }else{// remove the dequeued one from the reusable set
        [self.reusablePages removeObject:scrollView];
    }
    
    [self.pagingScrollView addSubview:scrollView];
    
    // update scroll view
    scrollView.frame = self.view.bounds;
    scrollView.image = [self _imageForPageAtIndex:index];
    scrollView.center = [self _centerForPageAtIndex:index];
    scrollView.pageIndex = index;
    
    // add to visible pages set
    [self.visiblePages addObject:scrollView];
    
    return scrollView;
}

/**
 *  Calculates the center for the page at the passed index
 *
 *  @param index - page index
 *
 *  @return - the calculated center
 */
- (CGPoint)_centerForPageAtIndex:(NSInteger)index {
    return CGPointMake(self.spaceBetweenPages/2.0 + self.view.width/2.0 + index * self.pagingScrollView.width, self.view.height/2.0);
}

/**
 *  Returns the number of pages displayed in the vc
 */
- (NSInteger)_numberOfPages {
    if (self.delegate) {
        return [self.delegate numberOfPagesForMTImageViewController:self];
    }else{
        return self.images.count;
    }
}

/**
 *  Returns the image for the page at the passed index
 *
 *  @param index - page index
 *
 *  @return - image
 */
- (UIImage *)_imageForPageAtIndex:(NSInteger)index {
    if (self.delegate) {
        UIImage *image = [self.delegate mtImageViewController:self imageForPageAtIndex:index];
        MTAssertParam(image);
        
        return image;
    }else{
        return self.images[index];
    }
}

/**
 *  Dismisses the image view controller.
 *  If the 'closeButtonAction' is set it will override the default behaviour.
 */
- (void)_dismissImageViewController {
    if (self.closeButtonAction) {
        self.closeButtonAction();
        return;
    }
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

/**
 *  Draws the close button icon and returns the image
 */
- (UIImage*)_closeButtonIcon{
    // begin context
    CGSize iconSize = CGSizeMake(18, 18);
    CGSize crosSize = CGSizeMake(16, 16);
    
    UIGraphicsBeginImageContextWithOptions(iconSize, NO, 0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
        
    // draw the cross
    CGContextSetLineWidth(ctx, 2);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor whiteColor] CGColor]);
    CGContextSetShadowWithColor(ctx, CGSizeMake(0, 1), 2, [[[UIColor blackColor] colorWithAlphaComponent:0.5] CGColor]);
    
    CGContextMoveToPoint(ctx, iconSize.width - crosSize.width, iconSize.height - crosSize.height);// upper left corner
    CGContextAddLineToPoint(ctx, iconSize.width - (iconSize.width - crosSize.width), iconSize.height - (iconSize.height - crosSize.height));// lower right corner
    
    CGContextMoveToPoint(ctx, iconSize.width - crosSize.width, iconSize.height - (iconSize.height - crosSize.height));
    CGContextAddLineToPoint(ctx, iconSize.width - (iconSize.width - crosSize.width), iconSize.height - crosSize.height);
    
    CGContextDrawPath(ctx, kCGPathStroke);
        
    // get image
    UIImage *closeButtonIcon = UIGraphicsGetImageFromCurrentImageContext();
        
    // end context
    UIGraphicsEndImageContext();
    
    return closeButtonIcon;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    self.currentPage = (targetContentOffset->x + self.pagingScrollView.width/2.0) / self.pagingScrollView.width;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self _setupTiles];
    [self _updatePageIndicator];
}

#pragma mark - Overrides

- (BOOL)prefersStatusBarHidden {
    return self.hidesStatusBar;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.statusBarStyle;
}

@end



#pragma mark - MTImageScrollView

@interface MTImageScrollView () <UIScrollViewDelegate>

@property (nonatomic, strong) UIImageView                          *imageView;

@end


@implementation MTImageScrollView

#pragma mark - API

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // setup scrollview to be zoomable
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.bouncesZoom = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;
        self.maximumZoomScale = 4;
        
        [self _setupViews];
    }
    return self;
}

#pragma mark - Lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // center the zoom view as it becomes smaller than the size of the screen
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = self.imageView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }else{
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }else{
        frameToCenter.origin.y = 0;
    }
    
    self.imageView.frame = frameToCenter;
}

#pragma mark - CA

- (void)setFrame:(CGRect)frame {
    BOOL sizeChanging = !CGSizeEqualToSize(frame.size, self.frame.size);
    
    [super setFrame:frame];
    
    if (sizeChanging) {
        [self _layoutViews];
    }
}

- (void)setImage:(UIImage *)image {
    _image = image;
    
    self.imageView.image = image;
    [self _layoutViews];
}

#pragma mark - Private

/**
 *  Sets up the image view and lays out the UI
 */
- (void)_setupViews {
    self.imageView = [UIImageView new];
    [self addSubview:self.imageView];
    [self _layoutViews];
}

/**
 *  Lays out the UI.
 *  Keeps the image always in the center and scales it to fit the screen.
 */
- (void)_layoutViews {
    if (!self.image) {
        return;
    }
    
    // reset the zoom
    [self setZoomScale:1];
    
    // layout image view
    CGSize imageSize = self.image.size;
    CGSize screenSize = self.frame.size;
    CGRect targetFrame;
    if (imageSize.width / screenSize.width > imageSize.height / screenSize.height) {// image is in landscape
        CGSize targetSize = CGSizeMake(screenSize.width, screenSize.width * (imageSize.height / imageSize.width));
        targetFrame = CGRectMake(0, screenSize.height/2.0 - targetSize.height/2.0, targetSize.width, targetSize.height);
    }else{// image is in portrait or square
        CGSize targetSize = CGSizeMake(screenSize.height * (imageSize.width / imageSize.height), screenSize.height);
        targetFrame = CGRectMake(screenSize.width/2.0 - targetSize.width/2.0, 0, targetSize.width, targetSize.height);
    }
    
    self.imageView.frame = targetFrame;
    
    // set content size
    self.contentSize = self.imageView.frame.size;
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    if (scrollView == self) {
        return self.imageView;
    }
    return nil;
}

@end
