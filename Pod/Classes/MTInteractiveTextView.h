//
//  MTInteractiveTextView.h
//  Pods
//
//  Created by Milutin Tomic on 25/01/16.
//

//
#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MTInteractiveTextView : UITextView

// a range/substring will be highlighted in this color when selected
@property (nonatomic, strong) IBInspectable UIColor *highlightedColor;

#pragma mark - Adding Actions

/**
 *  Adds the passed action for the first occurrence of the passed substring
 *
 *  @param action    - action to be executed when the substring is tapped
 *  @param substring - substring to attach the action to
 */
- (void)addAction:(void (^)(void))action forSubstring:(NSString*)substring;


/**
 *  Adds the passed action for the passed occurrence of the passed substring
 *
 *  @param action     - action to be executed when the substring is tapped
 *  @param substring  - substring to attach the action to
 *  @param occurrence - desired occurrence of the substring
 */
- (void)addAction:(void (^)(void))action forSubstring:(NSString*)substring occurrence:(NSInteger)occurrence;


/**
 *  Adds the passed action for all occurrences of the passed substring
 *
 *  @param action     - action to be executed when the substring is tapped
 *  @param substring  - substring to attach the action to
 */
- (void)addAction:(void (^)(void))action forAllOccurrencesOfSubstring:(NSString*)substring;


/**
 *  Adds the passed action for the passed range
 *
 *  @param action - action to be executed when the substring is tapped
 *  @param range  - range to attach the action to
 */
- (void)addAction:(void (^)(void))action forRange:(NSRange)range;

#pragma mark - Debug

/**
 *  Draws overlays for all actions
 */
- (void)highlightActions;

@end
