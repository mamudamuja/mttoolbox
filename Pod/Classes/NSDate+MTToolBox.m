//
//  NSDate+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 25/01/16.
//
//

#import "NSDate+MTToolBox.h"

@implementation NSDate (MTToolBox)

- (NSCalendar*)calender{
    return [NSCalendar currentCalendar];
}

+ (NSCalendar*)calender{
    return [NSCalendar currentCalendar];
}

#pragma mark - String Representation

/**
 *  Returns the string representation of the date using the passed date format
 *
 *  @param dateFormat - date format to use
 *
 *  @return - string representation
 */
- (NSString *)stringUsingDateFormat:(NSString *)dateFormat {
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = dateFormat;
    return [formatter stringFromDate:self];
}

#pragma mark - Comparing Dates

/**
 *  Checks if the date is the todays day
 */
- (BOOL)isToday {
    // today
    NSDateComponents *todayComponents = [[self calender] components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [[self calender] dateFromComponents:todayComponents];
    
    // date to check
    NSDateComponents *dateComponents = [[self calender] components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    NSDate *date = [[self calender] dateFromComponents:dateComponents];
    
    return [today isEqualToDate:date];
}

/**
 *  Returns the difference to the passed date in the specified unit
 *
 *  @param anotherDate - date to compare to
 *  @param unit        - unit
 */
- (NSDateComponents *)differenceToDate:(NSDate *)anotherDate unit:(NSCalendarUnit)unit {
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar] components:unit fromDate:self toDate:anotherDate options:0];
    return ageComponents;
}

#pragma mark - Date Components

/**
 *  Returns dates hour component
 *
 *  @return
 */
- (NSInteger)hour{
    NSDateComponents *components = [[self calender] components:NSCalendarUnitHour fromDate:self];
    return components.hour;
}


/**
 *  Returns dates hour component
 *
 *  @return
 */
- (NSInteger)minute{
    NSDateComponents *components = [[self calender] components:NSCalendarUnitMinute fromDate:self];
    return components.minute;
}


/**
 *  Returns dates hour component
 *
 *  @return
 */
- (NSInteger)second{
    NSDateComponents *components = [[self calender] components:NSCalendarUnitSecond fromDate:self];
    return components.second;
}


/**
 *  Returns dates year component
 *
 *  @return
 */
- (NSInteger)year{
    NSDateComponents *components = [[self calender] components:NSCalendarUnitYear fromDate:self];
    return components.year;
}


/**
 *  Returns dates month component
 *
 *  @return
 */
- (NSInteger)month{
    NSDateComponents *components = [[self calender] components:NSCalendarUnitMonth fromDate:self];
    return components.month;
}


/**
 *  Returns dates day component
 *
 *  @return
 */
- (NSInteger)day{
    NSDateComponents *components = [[self calender] components:NSCalendarUnitDay fromDate:self];
    return components.day;
}

#pragma mark - Creating Dates

/**
 *  Creates new date from components
 *
 *  @param day
 *  @param month
 *  @param year
 *  @param hour
 *  @param minut
 *  @param second
 *
 *  @return
 */
+ (NSDate*)dateWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second {
    NSDateComponents *components = [[NSDateComponents alloc]init];
    components.day = day;
    components.month = month;
    components.year = year;
    components.hour = hour;
    components.minute = minute;
    components.second = second;
    
    components.timeZone = [NSTimeZone localTimeZone];
    
    return [[self calender] dateFromComponents:components];
}

/**
 *  Creates new date from components
 *
 *  @param day
 *  @param month
 *  @param year
 *
 *  @return
 */
+ (NSDate*)dateWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year{
    return [self dateWithDay:day month:month year:year hour:0 minute:0 second:0];
}

@end
