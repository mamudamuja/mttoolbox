//
//  MTHUDView.h
//  Pods
//
//  Created by Milutin Tomic on 15/03/16.
//
//

#import <UIKit/UIKit.h>

@interface MTHUDView : UIView

/* === CUSTOMIZING APPEARANCE (GLOBAL) === */

// assing new image to overwrite the default success icon
// will be rendered as original image if no custom successIconColor specified
@property (nonatomic, strong) UIImage *successMessageIcon;

// assing new image to overwrite the default error icon
// will be rendered as original image if no custom errorIconColor specified
@property (nonatomic, strong) UIImage *errorMessageIcon;

// assign new font to overwrite the default message font
@property (nonatomic, strong) UIFont *messageFont;

// overwrites the default tint color
// this color will affect all hud elements
// to apply a color to a specific element only use the associated property
@property (nonatomic, strong) UIColor *hudTintColor;

// overwrites the default color for the error icon
@property (nonatomic, strong) UIColor *successMessageIconColor;

// overwrites the default color for the success icon
@property (nonatomic, strong) UIColor *errorMessageIconColor;

// overwrites the default color for a custom icon
@property (nonatomic, strong) UIColor *iconColor;

// overwrites the default message color
@property (nonatomic, strong) UIColor *messageColor;

// overwrites the default message box color
@property (nonatomic, strong) UIColor *messageBoxColor;

// overwrites the default background color
@property (nonatomic, strong) UIColor *messageBackgroundColor;

// overwrites the default message box corner radius
@property (nonatomic) CGFloat messageBoxCornerRadius;

// applies offset to the default messageBoxOffset from screen center
@property (nonatomic) CGPoint messageBoxOffset;

// overwrites the default icon view size
@property (nonatomic) CGFloat iconViewSize;

// overwrites the default stroke width for the error/success icon and progressIndicator
@property (nonatomic) CGFloat accessoryViewStrokeWidth;


/* === CUSTOMIZING BEHAVIOUR (GLOBAL) === */

// overwrites the default message duration for showAndDismiss actions
@property (nonatomic) NSTimeInterval messageDuration;

// if set to YES HUD will not pass touches to the window
// defaults to NO
@property (nonatomic) BOOL swallowsTouchEvents;


/**
 *  Returns shared instance of the MTHUDView.
 *  Use this instance to customize appearance and behaviour.
 *
 *  @return
 */
+ (instancetype)sharedView;

#pragma mark - Dismiss HUD

/**
 *  Dismisses the MTHUDView
 */
+ (void)dismiss;


/**
 *  Dismisses the HUDView after the specified delay
 *
 *  @param delay
 */
+ (void)dismissAfterDelay:(NSTimeInterval)delay;
    
#pragma mark - Message Modifiers
    
/**
 *  Enables the user to tap to dismiss the message.
 *  Should be called after the message is displayed.
 *  Will only affect the currently visivle message.
 *  Auto dismiss messages will still dismiss.
 */
//+ (void)enableTapToDismiss;


/**
 *  HUD will swallow touches for the duration of the current message
 */
+ (void)temporarilySwallowTouches;

#pragma mark Show HUD

/**
 *  Will present HUD with an message.
 *
 *  @param message - message to show
 */
+ (void)showWithMessage:(NSString*)message;


/**
 *  Will present HUD with an message and an icon.
 *
 *  @param message - message to show
 *  @param icon    - icon to show
 */
+ (void)showWithMessage:(NSString *)message andIcon:(UIImage*)icon;


/**
 *  Will present HUD with an error message and the error icon.
 *
 *  @param errorMessage - error message to show
 */
+ (void)showWithErrorMessage:(NSString*)errorMessage;


/**
 *  Will present HUD with an success message and the success icon.
 *
 *  @param successMessage - success message to show
 */
+ (void)showWithSuccessMessage:(NSString*)successMessage;

#pragma mark Show HUD with progress

/**
 *  Will present HUD with an message and a progress indicator.
 *  To update the progress call the method again with the same message and the updated progress.
 *
 *  @param message   - message to show
 *  @param progress  - progress (where 0 = 0% and 1 = 100%)
 *  @param clockWise - specifies if the indicator should go from empty to full or the opposite
 */
+ (void)showWithMessage:(NSString *)message andProgress:(CGFloat)progress clockWise:(BOOL)clockWise;


/**
 *  Will update the progress indicator.
 *  Has no effect if the progress indicator is not presented.
 *
 *  @param progress - new progress
 */
+ (void)setProgress:(CGFloat)progress;

#pragma mark Show and Autodismiss HUD

/**
 *  Will present HUD with an message.
 *  Will autodismiss after the messageDuration.
 *
 *  @param message - message to show
 */
+ (void)showAndAutoDismissWithMessage:(NSString*)message;


/**
 *  Will present HUD with an message and an icon.
 *  Will autodismiss after the messageDuration.
 *
 *  @param message - message to show
 *  @param icon    - icon to show
 */
+ (void)showAndAutoDismissWithMessage:(NSString *)message andIcon:(UIImage*)icon;


/**
 *  Will present HUD with an error message and the error icon.
 *  Will autodismiss after the messageDuration.
 *
 *  @param errorMessage - error message to show
 */
+ (void)showAndAutoDismissWithErrorMessage:(NSString*)errorMessage;


/**
 *  Will present HUD with an success message and the success icon.
 *  Will autodismiss after the messageDuration.
 *
 *  @param successMessage - success message to show
 */
+ (void)showAndAutoDismissWithSuccessMessage:(NSString*)successMessage;

#pragma mark Show with Activity Indicator

/**
 *  Will present HUD with an message and an activity indicator
 *
 *  @param message - message to show
 */
+ (void)showActivityIndicatorWithMessage:(NSString*)message;

@end
