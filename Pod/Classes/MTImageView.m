//
//  MTImageView.m
//  Pods
//
//  Created by Milutin Tomic on 14/01/16.
//
//

#import "MTImageView.h"

@interface MTImageView ()

@property (nonatomic, strong) NSOperationQueue *imageLoadingQueue;

@end


@implementation MTImageView

#pragma mark - Asynchronous Loading

- (void)loadImageFromDisk:(NSURL *)imageURL placeHolder:(UIImage *)placeHolder {
    // set placeholder image
    self.image = placeHolder;
    
    // load image in background
    __weak MTImageView *weakSelf = self;
    
    // create load operation
    NSBlockOperation *loadOperation = [NSBlockOperation new];
    loadOperation.queuePriority = NSOperationQueuePriorityNormal;
    loadOperation.qualityOfService = NSQualityOfServiceBackground;
    [loadOperation addExecutionBlock:^{
        // load image
        UIImage *image = [UIImage imageWithContentsOfFile:imageURL.relativePath];
        
        // update image view
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            weakSelf.image = image;
        }];
    }];
    
    // run load operation
    [self.imageLoadingQueue addOperation:loadOperation];
}

- (void)loadImageFromDisk:(NSURL *)imageURL {
    [self loadImageFromDisk:imageURL placeHolder:nil];
}

- (void)cancelImageLoading {
    [self.imageLoadingQueue cancelAllOperations];
}

#pragma mark - CA

- (NSOperationQueue *)imageLoadingQueue {
    if (!_imageLoadingQueue) {
        _imageLoadingQueue = [NSOperationQueue new];
        _imageLoadingQueue.maxConcurrentOperationCount = 1;
    }
    
    return _imageLoadingQueue;
}

/**
 *  Sets the templateImage as new image
 *  rendered in template mode
 *
 *  @param templateImage
 */
- (void)setTemplateImage:(UIImage *)templateImage{
    _templateImage = templateImage;
    
    self.image = [templateImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

@end
