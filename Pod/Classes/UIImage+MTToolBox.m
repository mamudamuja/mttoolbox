//
//  UIImage+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 12/09/16.
//
//

#import "UIImage+MTToolBox.h"
#import "MTToolBox.h"

@implementation UIImage (MTToolBox)

#pragma mark - Filters

/**
 *  Returns copy of image with applied filter to it
 *
 *  @param filterName - name of the filter 
 *                      ( https://developer.apple.com/library/content/documentation/GraphicsImaging/Reference/CoreImageFilterReference/ )
 *  @param params     - additional filter input parameters (inputImage will be added automatically) 
 */
- (UIImage *)imageByApplyingFilter:(NSString *)filterName parameters:(NSDictionary *)params {
    // create CIImage
    CIImage *beginImage = [[CIImage alloc] initWithCGImage:self.CGImage];
    
    // create filter
    CIFilter *filter = [CIFilter filterWithName:filterName];
    
    // set all filter default values
    [filter setDefaults];
    
    // set image as input
    [filter setValue:beginImage forKey:kCIInputImageKey];
    
    // apply additional passed params
    for (NSString *key in params) {
        [filter setValue:[params valueForKey:key] forKey:key];
    }
    
    // apply filter
    CIImage *outputImage = [filter outputImage];
    UIImage *resultImage = [UIImage imageWithCIImage:outputImage scale:self.scale orientation:self.imageOrientation];
    
    return resultImage;
}

#pragma mark - Base 64 Encoding

/**
 *  Returns Image's jpeg representation as base 64 encoded string
 *
 *  @param compressionQuality - compression quality (0 beeing the most compression and 1 the least compression)
 *
 *  @return
 */
- (NSString *)base64EncodedString:(CGFloat)compressionQuality {
    return [UIImageJPEGRepresentation(self, compressionQuality) base64EncodedStringWithOptions:0];
}

/**
 *  Returns Image's png representation as base 64 encoded string
 *
 *  @return
 */
- (NSString *)base64EncodedString {
    return [UIImagePNGRepresentation(self) base64EncodedStringWithOptions:0];
}

/**
 *  Creates UIImage from passed base 64 encoded string
 *
 *  @param encodedString - encoded string
 *
 *  @return
 */
+ (UIImage *)imageFromBase64EncodedString:(NSString *)encodedString {
    return [UIImage imageWithData:[[NSData alloc] initWithBase64EncodedString:encodedString options:NSDataBase64DecodingIgnoreUnknownCharacters]];
}

#pragma mark - Resizing


+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize inRect:(CGRect)rect {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    
    // draw image in provided rect
    [image drawInRect:rect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


+ (UIImage *)imageWithImage:(UIImage *)image scaledToFitToSize:(CGSize)newSize {
    // only scale images down
    if (image.size.width < newSize.width && image.size.height < newSize.height) return [image copy];
    
    // determine the scale factors
    CGFloat widthScale = newSize.width/image.size.width;
    CGFloat heightScale = newSize.height/image.size.height;
    
    CGFloat scaleFactor;
    
    // the smaller scale factor will scale more (0 < scaleFactor < 1) leaving the other dimension inside the newSize rect
    widthScale < heightScale ? (scaleFactor = widthScale) : (scaleFactor = heightScale);
    CGSize scaledSize = CGSizeMake(image.size.width * scaleFactor, image.size.height * scaleFactor);
    
    // scale image
    return [self imageWithImage:image scaledToSize:scaledSize inRect:CGRectMake(0.0, 0.0, scaledSize.width, scaledSize.height)];
}


+ (UIImage *)imageWithImage:(UIImage *)image scaledToFillToSize:(CGSize)newSize {
    // only scale images down
    if (image.size.width < newSize.width && image.size.height < newSize.height) return [image copy];
    
    // determine the scale factors
    CGFloat widthScale = newSize.width/image.size.width;
    CGFloat heightScale = newSize.height/image.size.height;
    
    CGFloat scaleFactor;
    
    // the larger scale factor will scale less (0 < scaleFactor < 1) leaving the other dimension hanging outside the newSize rect
    widthScale > heightScale ? (scaleFactor = widthScale) : (scaleFactor = heightScale);
    CGSize scaledSize = CGSizeMake(image.size.width * scaleFactor, image.size.height * scaleFactor);
    
    // create origin point so that the center of the image falls into the drawing context rect (the origin will have negative component).
    CGPoint imageDrawOrigin = CGPointMake(0, 0);
    widthScale > heightScale ?  (imageDrawOrigin.y = (newSize.height - scaledSize.height) * 0.5) :
    (imageDrawOrigin.x = (newSize.width - scaledSize.width) * 0.5);
    
    // create rect where the image will draw
    CGRect imageDrawRect = CGRectMake(imageDrawOrigin.x, imageDrawOrigin.y, scaledSize.width, scaledSize.height);
    
    // the imageDrawRect is larger than the newSize rect, where the imageDraw origin is located defines what part of
    // the image will fall into the newSize rect.
    return [UIImage imageWithImage:image scaledToSize:newSize inRect:imageDrawRect];
}

@end
