//
//  UICollectionView+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 02/09/16.
//
//

#import "UICollectionView+MTToolBox.h"

@implementation UICollectionView (MTToolBox)

/**
 *  Registers Nib with name for table view
 *
 *  @param cellName - Class/Nib name
 */
- (void)registerCellWithName:(NSString*)cellName{
    [self registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellWithReuseIdentifier:[NSString stringWithFormat:@"%@_ID", cellName]];
}

@end
