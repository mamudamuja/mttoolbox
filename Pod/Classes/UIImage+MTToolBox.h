//
//  UIImage+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 12/09/16.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (MTToolBox)

#pragma mark - Filters

/**
 *  Returns copy of image with applied filter to it
 *
 *  @param filterName - name of the filter
 *                      ( https://developer.apple.com/library/content/documentation/GraphicsImaging/Reference/CoreImageFilterReference/ )
 *  @param params     - additional filter input parameters (inputImage will be added automatically)
 */
- (UIImage *)imageByApplyingFilter:(NSString *)filterName parameters:(NSDictionary *)params;

#pragma mark - Base 64 Encoding

/**
 *  Returns Image's jpeg representation as base 64 encoded string
 *
 *  @param compressionQuality - compression quality (0 beeing the most compression and 1 the least compression)
 *
 *  @return
 */
- (NSString *)base64EncodedString:(CGFloat)compressionQuality;

/**
 *  Returns Image's png representation as base 64 encoded string
 *
 *  @return
 */
- (NSString *)base64EncodedString;

/**
 *  Creates UIImage from passed base 64 encoded string
 *
 *  @param encodedString - encoded string
 *
 *  @return
 */
+ (UIImage *)imageFromBase64EncodedString:(NSString *)encodedString;

#pragma mark - Resizing

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize inRect:(CGRect)rect;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToFitToSize:(CGSize)newSize;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToFillToSize:(CGSize)newSize;

@end
