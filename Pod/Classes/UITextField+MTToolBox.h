//
//  UITextField+MTToolBox.h
//  Pods
//
//  Created by Milutin Tomic on 23/01/16.
//
//

#import <UIKit/UIKit.h>

/* === APPEARANCE ATTRIBUTE KEYS === */
FOUNDATION_EXPORT NSString * const MTTitleColorKey; // expected value -> UIColor
FOUNDATION_EXPORT NSString * const MTTitleFontKey; // expected value -> UIFont
FOUNDATION_EXPORT NSString * const MTToolBarStyleKey; // expected value -> @(UIBarStyle)


@interface UITextField (MTToolBox)

/**
 *  Adds a tool bar with a done button which dismisses the caller (UITextField)
 *
 *  @param buttonTitle - title of the done button
 */
- (void)addDoneButtonWithTitle:(NSString*)buttonTitle;


/**
 *  Adds a tool bar with a done button which dismisses the caller (UITextField)
 *
 *  @param buttonTitle          - title of the done button
 *  @param appearanceAttributes - appearance attributes (doesn't need to contains all keys)
 */
- (void)addDoneButtonWithTitle:(NSString*)buttonTitle andAppearanceAttributes:(NSDictionary*)appearanceAttributes;


/**
 *  Creates tool bar with a done button which dismisses the caller (UITextField)
 *
 *  @param buttonTitle - done button title
 *
 *  @return
 */
- (UIToolbar*)accessoryViewWithDoneButtonTitle:(NSString*)buttonTitle;


/**
 *  Creates tool bar with a done button which dismisses the caller (UITextField)
 *
 *  @param buttonTitle          - done button title
 *  @param appearanceAttributes - appearance attributes
 *
 *  @return
 */
- (UIToolbar*)accessoryViewWithDoneButtonTitle:(NSString*)buttonTitle andAppearanceAttributes:(NSDictionary*)appearanceAttributes;

@end
