//
//  NSURL+MTToolBox.m
//  Pods
//
//  Created by Milutin Tomic on 15/03/16.
//
//

#import "NSURL+MTToolBox.h"

@implementation NSURL (MTToolBox)

#pragma mark - File URLs

/**
 *  Returns fileURL for Resource with passed name
 *
 *  @param fileName - name should be of format xxxx.yyy (e.g. test.pdf)
 *
 *  @return
 */
+ (NSURL*)mainBundleFileURLWithFileName:(NSString*)fileName{
    NSArray *components = [fileName componentsSeparatedByString:@"."];
    return [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:[components firstObject] ofType:[components lastObject]]];
}


/**
 *  Returns fileURL for subpath in documents folder
 *
 *  @param path - should contain file name (e.g. test.pdf), can also contain subdirectories (e.g. folder/subfolder/test.pdf)
 *
 *  @return
 */
+ (NSURL*)documentsFolderFileURLWithPath:(NSString*)path{
    NSString *documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *finalPath = [documentsDirectoryPath stringByAppendingPathComponent:path];
    return [NSURL fileURLWithPath:finalPath];
}

@end
