//
//  MTToolBoxTestTests.m
//  MTToolBoxTestTests
//
//  Created by Milutin Tomic on 19/02/2017.
//  Copyright © 2017 Milutin Tomic. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <MTToolBox/MTToolBox.h>

@interface MTToolBoxTestTests : XCTestCase

@end

@implementation MTToolBoxTestTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - NSArray+MTToolBox

/**
 *  tests MTToolBox Array mapping functions
 */
- (void)testArrayMapping {
    // test mapping
    NSArray *numbers = @[@0, @1, @2, @3, @4, @5];
    
    NSArray *numbersMapped = [numbers map:^id(NSNumber *object, NSInteger index) {
        return @([object integerValue] + 1);
    }];
    
    for (int index = 0; index < numbers.count; index++) {
        XCTAssertTrue([numbers[index] integerValue] + 1 == [numbersMapped[index] integerValue]);
    }
    
    // test mapping -> ignore nil NO
    numbersMapped = [numbers map:^id(NSNumber *object, NSInteger index) {
        return @([object integerValue] + 1);
    } ingnoreNil:NO];
    
    for (int index = 0; index < numbers.count; index++) {
        XCTAssertTrue([numbers[index] integerValue] + 1 == [numbersMapped[index] integerValue]);
    }
    
    // test mapping -> ignore nil YES, remove first object
    numbersMapped = [numbers map:^id(NSNumber *object, NSInteger index) {
        if (index == 0) {
            return nil;
        }
        return @([object integerValue] + 1);
    } ingnoreNil:YES];
    
    XCTAssertTrue(numbers.count == numbersMapped.count + 1);
    for (int index = 0; index < numbersMapped.count; index++) {
        XCTAssertTrue([numbers[index+1] integerValue] + 1 == [numbersMapped[index] integerValue]);
    }
}

#pragma mark - MTCache

- (void)testMTCache {
    // test storing and retrieving
    XCTAssertNil([MTCache objectForKey:@"hello"]);
    
    [MTCache setObject:@"world" forKey:@"hello"];
    
    XCTAssertEqual([MTCache objectForKey:@"hello"], @"world");
    XCTAssertNotEqual([MTCache objectForKey:@"hello" nameSpace:@"nm"], @"world");
    XCTAssertNil([MTCache objectForKey:@"hello" nameSpace:@"nm"]);
    XCTAssertEqual([MTCache objectForKey:@"hello" nameSpace:nil], @"world");
    
    [MTCache setObject:@"MTCache" forKey:@"hello" nameSpace:@"nm"];
    
    XCTAssertEqual([MTCache objectForKey:@"hello" nameSpace:@"nm"], @"MTCache");
    XCTAssert([MTCache setObject:@"TEST" forKey:@"persisted" nameSpace:nil persist:YES]);
    XCTAssert([MTCache setObject:@"TEST2" forKey:@"persisted" nameSpace:@"nm" persist:YES]);
    XCTAssertEqual([MTCache objectForKey:@"persisted"], @"TEST");
    
    NSCache *cache = [MTCache cacheWithNameSpace:nil];
    [cache removeAllObjects];
    
    XCTAssert([[MTCache objectForKey:@"persisted"] isEqualToString:@"TEST"]);
    
    [MTCache setObject:@"HAHAHA" forKey:@"persisted"];
    
    XCTAssert([[MTCache objectForKey:@"persisted"] isEqualToString:@"HAHAHA"]);
    
    [cache removeAllObjects];
    
    XCTAssert([[MTCache objectForKey:@"persisted"] isEqualToString:@"TEST"]);
    XCTAssert([[MTCache objectForKey:@"persisted" nameSpace:nil ignoreDisk:YES] isEqualToString:@"TEST"]);
    
    // test removing
    [MTCache removeAllObjects];
    [MTCache removeAllObjectsInNameSpace:@"nm"];
    [MTCache setObject:@"default" forKey:@"1" nameSpace:nil persist:YES];
    [MTCache setObject:@"namespace" forKey:@"1" nameSpace:@"nm" persist:YES];
    
    XCTAssert([[MTCache objectForKey:@"1" nameSpace:nil] isEqualToString:@"default"]);
    XCTAssert([[MTCache objectForKey:@"1" nameSpace:@"nm"] isEqualToString:@"namespace"]);
    
    [MTCache removeObjectForKey:@"1" nameSpace:nil ignoreDisk:YES];
    
    XCTAssert([[MTCache objectForKey:@"1" nameSpace:nil] isEqualToString:@"default"]);
    
    [MTCache removeObjectForKey:@"1"];
    
    XCTAssertNil([MTCache objectForKey:@"1" nameSpace:nil]);
    
    [MTCache removeObjectForKey:@"1" nameSpace:@"nm" ignoreDisk:YES];
    
    XCTAssert([[MTCache objectForKey:@"1" nameSpace:@"nm"] isEqualToString:@"namespace"]);
    
    [MTCache removeObjectForKey:@"1" nameSpace:@"nm"];
    
    XCTAssertNil([MTCache objectForKey:@"1" nameSpace:@"nm"]);
}

@end
