//
//  MTTestImageViewController.m
//  MTToolBoxTest
//
//  Created by Milutin Tomic on 28/02/2017.
//  Copyright © 2017 Milutin Tomic. All rights reserved.
//

#import "MTTestImageViewController.h"
#import <MTToolBox/MTToolBox.h>

@interface MTTestImageViewController () <MTImageViewControllerDelegate>

@property (nonatomic, strong) NSArray               *images;
@property (weak, nonatomic) IBOutlet UITableView    *tableView;
@property (weak, nonatomic) IBOutlet UISwitch       *arraySwitch;
@property (weak, nonatomic) IBOutlet UISwitch       *statusBarSwitch;
@property (weak, nonatomic) IBOutlet UISwitch       *spacingSwitch;
@property (weak, nonatomic) IBOutlet UISwitch       *delegateSwitch;
@property (weak, nonatomic) IBOutlet UISwitch       *pageIndicatorSwitch;

@end


@implementation MTTestImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    self.images = @[[UIImage imageNamed:@"landscape3"], [UIImage imageNamed:@"landscape2"], [UIImage imageNamed:@"landscape3"], [UIImage imageNamed:@"landscape3"], [UIImage imageNamed:@"landscape2"], [UIImage imageNamed:@"landscape3"], [UIImage imageNamed:@"landscape3"], [UIImage imageNamed:@"landscape2"], [UIImage imageNamed:@"landscape3"]];
}

#pragma mark - MTImageViewControllerDelegate

- (NSInteger)numberOfPagesForMTImageViewController:(MTImageViewController *)imageVC {
    if (_arraySwitch.isOn) {
        return self.images.count;
    }else{
        return 1;
    }
}

- (UIImage *)mtImageViewController:(MTImageViewController *)imageVC imageForPageAtIndex:(NSInteger)pageIndex {
    if (_arraySwitch.isOn) {
        return self.images[pageIndex];
    }else{
        return self.images[[self.tableView indexPathForSelectedRow].row];
    }
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.images.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = [NSString stringWithFormat:@"Image %i", (int)indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *imgs = nil;
    
    if (!_delegateSwitch.isOn) {
        imgs = _arraySwitch.isOn ? self.images : @[self.images[indexPath.row]];
    }
    
    MTImageViewController *viewController = [[MTImageViewController alloc] initWithImages:imgs];
    viewController.hidesStatusBar = _statusBarSwitch.isOn;
    viewController.statusBarStyle = UIStatusBarStyleLightContent;
    viewController.spaceBetweenPages = _spacingSwitch.isOn ? 10 : 0;
    viewController.showPageIndicator = _pageIndicatorSwitch.isOn;
    
    if (_delegateSwitch.isOn) {
        viewController.delegate = self;
    }
    
    if (_arraySwitch.isOn) {
        [viewController showPage:indexPath.row];
    }
    
    [self presentViewController:viewController animated:YES completion:nil];
}

@end
