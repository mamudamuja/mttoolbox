//
//  OtherTestViewController.h
//  MTToolBoxTest
//
//  Created by Milutin Tomic on 14/09/16.
//  Copyright © 2016 Milutin Tomic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherTestViewController : UIViewController

@property (nonatomic, copy, nullable) void (^dismissActionBlock)();

@end
