//
//  TransitionAnimatorTestViewController.m
//  MTToolBoxTest
//
//  Created by Milutin Tomic on 14/09/16.
//  Copyright © 2016 Milutin Tomic. All rights reserved.
//

#import "TransitionAnimatorTestViewController.h"
#import "OtherTestViewController.h"
#import <MTToolBox/MTToolBox.h>

static const NSTimeInterval kTransitionDuration = 0.4;

@interface TransitionAnimatorTestViewController () <MTTransitionAnimatorDelegate>

@property (nonatomic, strong) MTTransitionAnimator *modalAnimator;
@property (nonatomic, strong) MTTransitionAnimator *pushAnimator;
@property (nonatomic, strong) MTTransitionAnimator *overrideAnimator;

@property (weak, nonatomic) IBOutlet UIButton *presentModallyButton;
@property (nonatomic) BOOL interactiveModalTransition;

@end


@implementation TransitionAnimatorTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.modalAnimator = [[MTTransitionAnimator alloc] initWithDelegate:self];
    self.pushAnimator = [[MTTransitionAnimator alloc] initWithDelegate:self];
    
    // setup gesture recognizers for interactive transitions
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleModalTransitionRecognizer:)];
    [self.view addGestureRecognizer:panRecognizer];
}

#pragma mark - Gesture Recognizers Actions

- (void)handleModalTransitionRecognizer:(UIPanGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint touchLocation = [gestureRecognizer locationOfTouch:0 inView:self.view];
        if (CGRectContainsPoint(self.presentModallyButton.frame, touchLocation)) {
            self.interactiveModalTransition = YES;
            [self presentModally:nil];
        }
    }else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGFloat progress = fabs([gestureRecognizer translationInView:self.view].y) / self.view.height;
        progress = MIN(1, MAX(0, progress));
        [self.modalAnimator updateInteractiveTransition:progress];
    }else if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        CGFloat progress = fabs([gestureRecognizer translationInView:self.view].y) / self.view.height;
        progress = MIN(1, MAX(0, progress));
        if (progress > 0.3) {
            [self.modalAnimator finishInteractiveTransition];
        }else{
            [self.modalAnimator cancelInteractiveTransition];
        }
    }
}

#pragma mark - MTTransitionAnimatorDelegate

- (NSTimeInterval)transitionAnimator:(MTTransitionAnimator *)animator transitionDurationForOperation:(MTTransitionAnimatorOperation)operation {
    return kTransitionDuration;
}

- (void (^)(void))transitionAnimator:(MTTransitionAnimator *)animator animationBlockForTransitionFromViewController:(UIViewController<MTTransitionAnimatorViewController> *)fromViewController toViewController:(UIViewController<MTTransitionAnimatorViewController> *)toViewController animationContainer:(UIView *)animationContainer transitionContext:(id<UIViewControllerContextTransitioning>)transitionContext withOperation:(MTTransitionAnimatorOperation)operation {
    
    void (^animBlock)();
    
    if (animator == self.modalAnimator) {
        if (operation == MTTransitionAnimatorOperationPresent) {
            animBlock = ^void() {
                [animationContainer addSubview:toViewController.view];
                toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
                [toViewController.view centerUnderView:nil withPadding:animationContainer.height - 51];
                
                [UIView animateWithDuration:kTransitionDuration animations:^{
                    toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
                } completion:^(BOOL finished) {
                    if (finished && ![transitionContext transitionWasCancelled]) {
                        [fromViewController.view removeFromSuperview];
                    }
                    
                    self.interactiveModalTransition = NO;
                }];
            };
        }
    }else if (animator == self.pushAnimator || animator == self.overrideAnimator) {
        if (operation == MTTransitionAnimatorOperationPush) {
            animBlock = ^void() {
                [animationContainer addSubview:toViewController.view];
                toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
                toViewController.view.center = CGPointMake(toViewController.view.center.x + toViewController.view.width, toViewController.view.center.y);
                
                [UIView animateWithDuration:kTransitionDuration animations:^{
                    toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
                    fromViewController.view.center = CGPointMake(fromViewController.view.center.x - fromViewController.view.width, fromViewController.view.center.y);
                } completion:^(BOOL finished) {
                    if (finished && ![transitionContext transitionWasCancelled]) {
                        fromViewController.view.frame = [transitionContext finalFrameForViewController:fromViewController];
                    }
                    
                    self.interactiveModalTransition = NO;
                }];
            };
        }else{
            animBlock = ^void() {
                [animationContainer addSubview:toViewController.view];
                toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
                toViewController.view.center = CGPointMake(toViewController.view.center.x - toViewController.view.width, toViewController.view.center.y);
                
                [UIView animateWithDuration:kTransitionDuration animations:^{
                    toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
                    fromViewController.view.center = CGPointMake(fromViewController.view.center.x + fromViewController.view.width, fromViewController.view.center.y);
                } completion:^(BOOL finished) {
                    if (finished && ![transitionContext transitionWasCancelled]) {
                        [fromViewController.view removeFromSuperview];
                    }
                    
                    self.interactiveModalTransition = NO;
                }];
            };
        }
    }
    
    return animBlock;
}

- (BOOL)transitionAnimator:(MTTransitionAnimator *)animator shouldStartInteractiveTransitionFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController withOperation:(MTTransitionAnimatorOperation)operation {
    if (animator == self.modalAnimator && self.interactiveModalTransition) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Button Actions

- (IBAction)toggleValuChanged:(MTToggleButton *)sender {
    if (sender.isOn) {
        self.overrideAnimator = [[MTTransitionAnimator alloc] initWithDelegate:self navigationController:self.navigationController];
    }else{
        [self.overrideAnimator decoupleFromNavigationController];
        self.overrideAnimator = nil;
    }
}

- (IBAction)pushNormal:(id)sender {
    [self.navigationController pushViewController:[TransitionAnimatorTestViewController new] animated:YES];
}

- (IBAction)pushCustom:(id)sender {
    [self.pushAnimator pushViewController:[TransitionAnimatorTestViewController new] ontoNavigationController:self.navigationController];
}

- (IBAction)presentModally:(id)sender {
    [self.modalAnimator presentViewController:[OtherTestViewController new] fromViewController:self completion:^{
        MTLog(@"Done presenting");
    }];
}

@end
