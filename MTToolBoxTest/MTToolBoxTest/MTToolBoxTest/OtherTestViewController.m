//
//  OtherTestViewController.m
//  MTToolBoxTest
//
//  Created by Milutin Tomic on 14/09/16.
//  Copyright © 2016 Milutin Tomic. All rights reserved.
//

#import "OtherTestViewController.h"
#import <MTToolBox/MTToolBox.h>

static const NSTimeInterval kTransitionDuration = 0.4;

@interface OtherTestViewController () <MTTransitionAnimatorDelegate>

@property (nonatomic, strong) MTTransitionAnimator *animator;
@property (nonatomic) BOOL interactiveModalTransition;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;

@end


@implementation OtherTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.animator = [[MTTransitionAnimator alloc] initWithDelegate:self];
    
    // setup gesture recognizers for interactive transitions
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleModalTransitionRecognizer:)];
    [self.view addGestureRecognizer:panRecognizer];
}

#pragma mark - Gesture Recognizers Actions

- (void)handleModalTransitionRecognizer:(UIPanGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint touchLocation = [gestureRecognizer locationOfTouch:0 inView:self.view];
        if (CGRectContainsPoint(self.dismissButton.frame, touchLocation)) {
            self.interactiveModalTransition = YES;
            [self dismiss:nil];
        }
    }else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGFloat progress = fabs([gestureRecognizer translationInView:self.view].y) / self.view.height;
        progress = MIN(1, MAX(0, progress));
        [self.animator updateInteractiveTransition:progress];
    }else if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        CGFloat progress = fabs([gestureRecognizer translationInView:self.view].y) / self.view.height;
        progress = MIN(1, MAX(0, progress));
        if (progress > 0.3) {
            [self.animator finishInteractiveTransition];
        }else{
            [self.animator cancelInteractiveTransition];
        }
    }
}

#pragma mark - MTTransitionAnimatorDelegate

- (NSTimeInterval)transitionAnimator:(MTTransitionAnimator *)animator transitionDurationForOperation:(MTTransitionAnimatorOperation)operation {
    return kTransitionDuration;
}

- (void (^)(void))transitionAnimator:(MTTransitionAnimator *)animator animationBlockForTransitionFromViewController:(UIViewController<MTTransitionAnimatorViewController> *)fromViewController toViewController:(UIViewController<MTTransitionAnimatorViewController> *)toViewController animationContainer:(UIView *)animationContainer transitionContext:(id<UIViewControllerContextTransitioning>)transitionContext withOperation:(MTTransitionAnimatorOperation)operation {
    
    void (^animBlock)();
    
    if (animator == self.animator && operation == MTTransitionAnimatorOperationDismiss) {
        animBlock = ^void() {
            [animationContainer addSubview:toViewController.view];
            toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
            [animationContainer sendSubviewToBack:toViewController.view];
            
            [UIView animateWithDuration:kTransitionDuration animations:^{
                [fromViewController.view centerUnderView:nil withPadding:animationContainer.height - 51];
            } completion:^(BOOL finished) {
                if (![transitionContext transitionWasCancelled]) {
                    [fromViewController.view removeFromSuperview];
                }
            }];
        };
    }
    
    return animBlock;
}

- (BOOL)transitionAnimator:(MTTransitionAnimator *)animator shouldStartInteractiveTransitionFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController withOperation:(MTTransitionAnimatorOperation)operation {
    return self.interactiveModalTransition;
}

#pragma mark - Button Actions

- (IBAction)dismiss:(id)sender {
    [self.animator dismissViewController:self completion:^{
        MTLog(@"Done dismissing");
    }];
}

@end
