//
//  ViewController.m
//  MTToolBoxTest
//
//  Created by Milutin Tomic on 26/08/16.
//  Copyright © 2016 Milutin Tomic. All rights reserved.
//

#import "ViewController.h"
#import <MTToolBox/MTToolBox.h>
#import "TransitionAnimatorTestViewController.h"
#import "CustomImagePickerViewController.h"
#import "MTTestImageViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSArray *viewControllers;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


@implementation ViewController

#pragma mark - Lifestyle

- (void)viewDidLoad {
    TransitionAnimatorTestViewController *transitionTest = [TransitionAnimatorTestViewController new];
    transitionTest.title = @"MTTransitionAnimator";
    
    CustomImagePickerViewController *customImagePickerTest = [CustomImagePickerViewController new];
    customImagePickerTest.title = @"Custom Image Picker";
    
    MTTestImageViewController *testImageVC = [MTTestImageViewController new];
    testImageVC.title = @"MTImageViewController";
    
    self.viewControllers = @[transitionTest, customImagePickerTest, testImageVC];
    
    self.navigationController.navigationBar.translucent = NO;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:[UITableViewCell reuseIdentifier]];
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewControllers.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[UITableViewCell reuseIdentifier]];
    
    cell.textLabel.text = [self.viewControllers[indexPath.row] title];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.navigationController pushViewController:self.viewControllers[indexPath.row] animated:YES];
}

@end
