#
# Be sure to run `pod lib lint MTToolBox.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MTToolBox'
  s.version          = '2.2.1'
  s.summary          = 'iOS development tool box.'
#  s.description      = 'iOS development tool box'
  s.homepage         = 'https://bitbucket.org/mamudamuja/mttoolbox.git'
  s.license          = 'MIT'
  s.author           = { 'Milutin Tomic' => 'milutintomic91@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/mamudamuja/mttoolbox.git', :tag => s.version.to_s }
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.public_header_files = 'Pod/Classes/**/*.h'
  s.resource_bundles = {
    'MTImageViewController' => ['Pod/Assets/*']
  }
  s.frameworks = 'UIKit', 'CoreData'
end
